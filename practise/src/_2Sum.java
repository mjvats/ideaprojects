import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class _2Sum {

    public static void main(String[] args) {
        /*List<Integer> A = Arrays
            .asList(4, 7, -4, 2, 2, 2, 3, -5, -3, 9, -4, 9, -7, 7, -1, 9, 9, 4, 1, -4, -2, 3, -3,
                -5, 4, -7, 7, 9, -4, 4, -8);*/
        List<Integer> A = Arrays.asList(-10, -10, -10);
        int B = -5;
        ArrayList<Integer> result = new _2Sum().twoSum(A, B);
        for (Integer val : result) {
            System.out.print(val + " ");
        }
    }

    public ArrayList<Integer> twoSum(final List<Integer> A, int B) {

        HashMap<Integer, List<Integer>> tempMap = new HashMap<>();
        int[] resultArr = null;
        ArrayList<Integer> resultList = new ArrayList<>();

        for (int i = 0; i < A.size(); i++) {
            int temp = B - A.get(i);
            if (tempMap.containsKey(temp)) {
                if (resultArr == null)
                {
                    resultArr = new int[A.size() + 1];
                }
                List<Integer> value = tempMap.get(temp);
                for (Integer x : value) {
                    resultArr[x+1] = resultArr[x+1] == 0 ? i + 1 : Integer.min(resultArr[x+1], i+1);
                }
            }
            if (tempMap.containsKey(A.get(i))) {
                List<Integer> tempList = new ArrayList<>(tempMap.get(A.get(i)));
                tempList.add(i);
                tempMap.replace(A.get(i), tempList);
            } else {
                tempMap.put(A.get(i), Collections.singletonList(i));
            }
        }

        int i = Integer.MAX_VALUE;
        int j = Integer.MAX_VALUE;
        if (resultArr != null) {
            for (int k = 0; k < resultArr.length; k++) {
                if (resultArr[k] != 0 && (resultArr[k] < j || (resultArr[k] == j && k < i))) {
                    j = resultArr[k];
                    i = k;
                }
            }
            resultList.add(i);
            resultList.add(j);
        }
        return resultList;
    }
}
