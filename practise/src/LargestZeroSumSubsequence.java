import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LargestZeroSumSubsequence {

    public static void main(String[] args) {
        List<Integer> A = Arrays.asList(-1, 20, 7, -22, 1, 21, 5, 24, -26, -16, -4, -9, 19, 8, -27, 28, 9, 8, -29, 29, 8, 9, 17, -28, 13, 20, -1, -8, -16);
        ArrayList<Integer> result = lszero(A);
        for(int val : result){
            System.out.print(val+" ");
        }
    }

    public static ArrayList<Integer> lszero(List<Integer> A) {

        ArrayList<Integer> resultList = new ArrayList<>();

        HashMap<Integer, Integer> map = new HashMap<>();

        int sum = 0;
        int maxLen = 0;
        int j = 0, i = -1,k = -1;
        for(;j < A.size() ; j++){
            sum += A.get(j);
            if(sum == 0)
            {
                maxLen = j+1;
                k = j;
                i = -1;
            }
            if(map.containsKey(sum)){
                int temp = map.get(sum);
                int len = j-temp;
                if(len > maxLen)
                {
                    maxLen = len;
                    i = temp;
                    k = j;
                }
                else if(len == maxLen)
                {
                    if(temp < i)
                    {
                        i = temp;
                    }
                }
            }
            else
                map.put(sum, j);
        }
        while(++i <= k){
            resultList.add(A.get(i));
        }

        return resultList;
    }

}
