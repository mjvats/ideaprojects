public class BeautifulNumber {

    public static void main(String[] args) {
        System.out.println(solve(1, 1000000));
    }

    static long solve(int l, int r){
        // Your code goes here
        long sum = 0;
        for(int x = l; x <= r ; x++){
            String[] strArr = String.valueOf(x).split("");
            int[] intArr = new int[strArr.length];
            for(int i = 0 ; i < strArr.length ; i++){
                intArr[i] = Integer.valueOf(strArr[i]);
            }

            if(isBeautifulNum(intArr)){
                sum += x;
            }
        }
        return sum;
    }

    static boolean isBeautifulNum(int[] arr){

        int sum = 0;
        for(int val : arr){
            sum += val*val;
        }
        if(sum == 1){
            return true;
        }
        else{
            int tempSum = sum;
            while(tempSum >= 1){
                tempSum = 0;
                for(int i = 0 ; i < arr.length ; i++){
                    arr[i] = arr[i]*arr[i];
                    tempSum += arr[i];
                }
                if(tempSum == 1){
                    return true;
                }
                else{
                    sum = tempSum;
                    String[] strArr = String.valueOf(sum).split("");
                    arr = new int[strArr.length];
                    for(int i = 0 ; i < strArr.length ; i++){
                        arr[i] = Integer.valueOf(strArr[i]);
                    }
                }
            }
            return false;
        }
    }
}
