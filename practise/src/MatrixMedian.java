import java.util.Arrays;
import java.util.List;

public class MatrixMedian {

    public static void main(String[] args) {
        Integer[] list1 = {1, 3, 5};
        Integer[] list2 = {2, 6, 9};
        Integer[] list3 = {3, 6, 9};

        List<List<Integer>> A = Arrays
           .asList(Arrays.asList(list1), Arrays.asList(list2), Arrays.asList(list3));
        //List<List<Integer>> A = Arrays.asList(Arrays.asList(2), Arrays.asList(1), Arrays.asList(4), Arrays.asList(1), Arrays.asList(2), Arrays.asList(2), Arrays.asList(5));
        MatrixMedian median = new MatrixMedian();
        int result = median.findMedian(A);
        System.out.println(result);
    }

    public int findMedian(List<List<Integer>> A) {

        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int N = A.size();
        int M = A.get(0).size();

        for (List<Integer> integers : A) {
            min = Integer.min(integers.get(0), min);
            max = Integer.max(max, integers.get(M - 1));
        }

        int start = min, end = max, mid = 0;
        int ans = 0;
        while (start <= end) {
            mid = (start + end) / 2;
            long smallerThanMid = smallerThanMid(A, N, M, mid);
            if (smallerThanMid < (((long) N *M)/2)+1) {
                start = mid+1;
            }
            else {
                ans = mid;
                end = mid - 1;
            }
        }
        return ans;
    }

    private long smallerThanMid(List<List<Integer>> a, int n, int m, int x)
    {
        int count = 0;
        for(List<Integer> tempList : a)
        {
            int start = 0, end = tempList.size()-1;
            while(start <= end)
            {
                int mid = (start + end)/2;
                if(tempList.get(mid) <= x)
                {
                    if(mid == tempList.size()-1 || tempList.get(mid+1) > x)
                    {
                        count += (mid+1);
                        break;
                    }
                    else
                    {
                        start = mid + 1;
                    }
                }
                else
                {
                    end = mid - 1;
                }
            }
        }

        return count;
    }


}
