public class PopulateArray {

    public static void main(String[] args) {

        int[] B = {3, 5, 1};

        for(int i = 0 ; i < 20 ; i++){
            int[] A = new int[20];
            func(A, B, 3, 3, 0, 0);
            for(int val : A){
                System.out.print(val+" ");
            }
            System.out.println();
        }

    }

    public static void func(int[] A, int[] B, int N, int k, int bidx, int aidx){
        if(bidx == N){
            return;
        }

        int temp = 0;
        while (temp < B[bidx]){
            A[aidx++] = bidx;
            temp++;
        }

        func(A, B, N, k, bidx+1, aidx+k);
    }
}
