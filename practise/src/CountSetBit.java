public class CountSetBit {

    public static void main(String[] args) {
        System.out.println(getSetBitNum(6));
    }

    private static int getSetBitNum(int n){
        int count = 0;
        while(n != 0){
            n = n & (n-1);
            count++;
        }

        return count;
    }
}
