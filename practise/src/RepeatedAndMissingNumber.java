import java.util.ArrayList;
import java.util.List;

public class RepeatedAndMissingNumber {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(1);
        list.add(2);
        list.add(5);
        list.add(3);

        ArrayList<Integer> result = sol(list);
        for (int val : result){
            System.out.println(val);
        }
    }

    private static ArrayList<Integer> func(List<Integer> A) {
        ArrayList<Integer> result = new ArrayList<>();
        int repeatedNum = 0;
        for (int i = 0; i < A.size(); i++) {
            if (A.get(Math.abs(A.get(i)) - 1) < 0) {
                repeatedNum = Math.abs(A.get(i));
                A.set(i, 0);
                break;
            } else {
                A.set(Math.abs(A.get(i)) - 1, -A.get(Math.abs(A.get(i)) - 1));
            }
        }
        result.add(repeatedNum);

        long N = A.size();
        long sumOfN = (N * (N + 1)) / 2;

        long sum = 0;
        for (int i = 0; i < N; i++) {
            sum = (sum + Math.abs(A.get(i)));
        }
        result.add((int) (sumOfN - sum));
        return result;
    }

    private static ArrayList<Integer> sol(List<Integer> A) {
        int x = 0;
        int R = 0, M = 0;
        for (int val : A) {
            x = x ^ val;
        }
        for (int i = 1; i <= A.size(); i++) {
            x = x ^ i;
        }

        int setBitPosition = 0;
        for (int i = 0; i < Integer.toBinaryString(A.size()).length(); i++) {
            if (((x >> i) & 1) == 1) {
                setBitPosition = i;
                break;
            }
        }

        for (int val : A) {
            if (((val >> setBitPosition) & 1) == 1) {
                R = R^val;
            } else {
                M = M^val;
            }
        }
        for (int i = 1; i <= A.size(); i++) {
            if (((i >> setBitPosition) & 1) == 1) {
                R = R^i;
            } else {
                M = M^i;
            }
        }
        ArrayList<Integer> result = new ArrayList<>();
        int count = 0;
        for(int val : A){
            if(val == R){
                count++;
            }
        }
        if(count > 1){
            result.add(R);
            result.add(R^x);
        }
        else{
            count = 0;
            for(int val : A){
                if(val == M){
                    count++;
                }
            }
            if(count > 1){
                result.add(M);
                result.add(M^x);
            }
        }

        return result;
    }

}
