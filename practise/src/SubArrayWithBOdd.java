import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SubArrayWithBOdd {

    public static void main(String[] args) {

        List<Integer> A = Arrays.asList(4, 3, 2, 3, 4 );
        int B = 2;

        int result = solve(A, B);
        System.out.println(result);
    }

    public static int solve(List<Integer> A, int B) {

        HashMap<Integer, Integer> map = new HashMap<>();
        int sum = 0, count = 0;
        for (Integer integer : A) {
            if (integer % 2 != 0) {
                sum += 1;
            }
            if (sum == B) {
                count++;
            }

            if (map.containsKey(sum - B)) {
                count += map.get(sum - B);
            }

            if (map.containsKey(sum)) {
                map.put(sum, map.get(sum) + 1);
            } else {
                map.put(sum, 1);
            }
        }

        return count;
    }

}
