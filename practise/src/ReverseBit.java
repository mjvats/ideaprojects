public class ReverseBit {

    public static void main(String[] args) {
        System.out.println(reverseBit(5));
    }

    static private long reverseBit(long A){

        long result = 0;
        for(int i = 0, j = 31 ; i <= 31 & j >= 0 ; i++, j--){
            if(checkBits(A, i)){
               result =  (1L << j) | result;
            }
        }
        return result;
    }

    static private boolean checkBits(long A, int i){
        return ((A >> i) & 1L) == 1;
    }

}
