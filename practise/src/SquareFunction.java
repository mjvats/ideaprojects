import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class SquareFunction {

    public static void main(String[] args) throws Exception
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());

        while(T > 0){

            int size = Integer.parseInt(br.readLine());
            String[] input = br.readLine().trim().split(" ");
            int[] A = new int[size];
            for(int i = 0 ; i < size ; i++)
            {
                A[i] = Integer.parseInt(input[i]);
            }
            int result = countGoodPairs(A, size);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;

        }

        br.close();
        bw.close();

    }

    private static int countGoodPairs(int[] A, int N)
    {
        int count = 0;
/*        for(int i = 0 ; i < N-1 ; i++)
        {
            for(int j = i+1 ; j < N ; j++)
            {
                long M = (long)(A[i]*A[j]);
                if(valid(M))
                {
                    count++;
                }
            }
        }*/
        for(int i = 0, j = A.length-1; i < j;)
        {
            long M = (long)(A[i]*A[j]);
            if(valid(M))
            {
                count++;
                j--;
            }
            else
            {
                i++;
            }
        }
        return count;
    }

    private static boolean valid(long m) {
        long largestPerfectSquare = largestPerfectSquare(m);
        return m / largestPerfectSquare > 1;
    }

    private static long largestPerfectSquare(long m)
    {
        long max = 0;
        for(int i = 1 ; i <= (long)Math.sqrt(m) ; i++)
        {
            if((m%((long) i *i) == 0))
            {
                max = i*i;
            }
        }
        return max;
    }


}
