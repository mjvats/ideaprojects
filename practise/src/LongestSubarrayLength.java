import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LongestSubarrayLength {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 0, 0, 1, 0);
        int result = solve(list);
        System.out.println(result);
    }

    public static int solve(List<Integer> A) {

        int n = A.size();
        for(int i = 0 ; i < n ; i++) if(A.get(i) == 0) A.set(i, -1);

        HashMap<Integer, Integer> map = new HashMap<>();
        int ans = Integer.MIN_VALUE;
        for(int i = 0 ; i < n ; i++)
        {
            if(i > 0) A.set(i, A.get(i-1)+A.get(i));

            if(A.get(i) == 1) ans = Integer.max(ans, i+1);

            int temp = A.get(i)-1;
            if(map.containsKey(temp))
            {
                ans = Integer.max(ans, i - map.get(temp));
            }
            if(map.containsKey(A.get(i)))
            {
                map.put(A.get(i), Integer.min(map.get(A.get(i)), i));
            }
            else
            {
                map.put(A.get(i), i);
            }
        }
        return ans;
    }

}
