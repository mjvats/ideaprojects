import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ExpressAsAPowerB {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){

            int N = Integer.parseInt(br.readLine());
            if(func(N)) {
                bw.write("Yes");
            }
            else {
                bw.write("No");
            }
            bw.write("\n");
            T--;

        }

        br.close();
        bw.close();
    }

    public static boolean func(int n)
    {
        for(int i = 2; i <= (int)Math.sqrt(n) ; i++)
        {
            if(n % i == 0)
            {
                double b = Math.log(n)/Math.log(i);
                if(Math.floor(b) == Math.ceil(b))
                    return true;
            }
        }
        return false;
    }

}
