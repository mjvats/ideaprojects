import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;

public class PrintUnique {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int N = Integer.valueOf(br.readLine());
        int[] A = new int[N];
        String[] input = br.readLine().trim().split(" ");
        for (int i = 0; i < N; i++) {
            A[i] = Integer.valueOf(input[i]);
        }

        printUnique(A, bw);
        bw.write("\n");
        br.close();
        bw.close();
    }

    private static void printUnique(int[] arr, BufferedWriter bw) throws IOException {

        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            if (map.containsKey(arr[i])) {
                map.put(arr[i], map.get(arr[i]) + 1);
            } else {
                map.put(arr[i], 1);
            }
        }

        for (int i = 0; i < arr.length; i++) {
            if (map.containsKey(arr[i]) && map.get(arr[i]) == 1) {
                bw.write(arr[i] + " ");
            }
        }

    }

}
