import java.util.ArrayList;
import java.util.HashSet;

public class ColorfulNumber {

    public static void main(String[] args) {
        int result = isColorful(263);
        System.out.println(result);
    }

    public static int isColorful(int N) {
        ArrayList<Integer> digiList = new ArrayList<>();
        while (N / 10 > 0) {
            digiList.add(N % 10);
            N = N/10;
        }
        digiList.add(N % 10);
        HashSet<Long> set = new HashSet<>();
        int M = digiList.size();
        for (int i = 0; i < M; i++) {
            long prod = digiList.get(i);
            for (int j = i; j < M; j++) {
                if (i != j) {
                    prod = prod * digiList.get(j);
                }
                if (set.contains(prod)) {
                    return 0;
                } else {
                    set.add(prod);
                }
            }
        }
        return 1;
    }
}
