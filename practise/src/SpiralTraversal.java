public class SpiralTraversal {

    public static void main(String[] args) {
        int[][] input = {{1},{2},{3}};
        spiral(input);
    }

    private static void spiral(int[][] M){
        int i = 0, j = 0;

        int top = 0, bottom = M.length-1, left = 0, right = M[0].length-1;
        while(top <= bottom && left <= right){
            while(j <= right){
                System.out.print(M[i][j]+" ");
                j++;
            }
            top++;
            j--;
            i++;

            while(i <= bottom){
                System.out.print(M[i][j]+" ");
                i++;
            }
            right--;
            i--;
            j--;

            if(top <= bottom){
                while(j >= left){
                    System.out.print(M[i][j]+" ");
                    j--;
                }
            }
            bottom--;
            j++;
            i--;

            if(left <= right){
                while(i >= top){
                    System.out.print(M[i][j]+" ");
                    i--;
                }
            }
            left++;
            i++;
            j++;
        }
    }

}
