public class MethodReference {

    public static void main(String args[]){
        Test test = MethodReference::method;
        test.abstractMethod(1);
    }

    private static int method(int a){
        System.out.println("Static method by reference");
        return a;
    }

}

interface Test{
    void abstractMethod(int a);
}
