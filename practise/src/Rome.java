import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Rome {
    public static int solution(int[] A, int[] B) {
        // write your code in Java SE 8

        Integer[] arr = new Integer[A.length + B.length];
        int k = 0;
        for(int i = 0 ; i < A.length ; i++){
            arr[k] = A[i];
            k++;
        }

        for(int i = 0 ; i < B.length ; i++){
            arr[k] = B[i];
            k++;
        }

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(arr));
        int max = Collections.max(list);

        int[][] M = new int[max+1][max+1];
        for(int i = 0 ; i < A.length ; i++){
            if(A[i] != B[i]){
                M[A[i]][B[i]] = 1;
            }
        }

        boolean isReachableFromAll = false;
        loop1:
        for(int x = 0 ; x < max ; x++){
            loop2:
            for(int i = 0 ; i < M.length ; i++){
                loop3:
                for (int j = 0 ; j < M[0].length ; j++){
                    if(i == j || M[i][j] == 0){
                        continue;
                    }
                    else if (M[i][j] == 1){
                        if(j == x){
                            isReachableFromAll = true;
                            break loop3;
                        }
                        else{
                            if(isReachable(M, j, x)){
                                isReachableFromAll = true;
                                break loop3;
                            }
                        }
                    }
                }
            }
            if(isReachableFromAll){
                return x;
            }
        }

        return -1;
    }

    public static boolean isReachable(int[][] M, int source, int target){
        return M[source][target] == 1;
    }

    public static void main(String args[]){
        int[] A = {0, 1, 2, 4, 5};
        int[] B = {2, 3, 3, 3, 2};
        System.out.println(solution(A,B));
    }
}
