package dp;

public class PalindromePartitioning {

    public static void main(String[] args) {
        String s = "abcbm";
        System.out.println(m(s.split("")));
    }

    public static int m(String[] str){
        int n = str.length;
        byte[][] dp = new byte[n][n];

        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n-j; i++) {
                int k = i + j;
                if (isPalindrome(str, dp, i , k)) {
                    dp[i][k] = 1;
                } else {
                    dp[i][k] = 0;
                }
            }
        }

        int[] p = new int[n];
        for(int i = 0 ; i < n ; i++){
            int ans = Integer.MAX_VALUE;
            for(int j = i ; j >= 0 ; j--){
                if(dp[j][i] == 1){
                    if(j == 0){
                        ans = Math.min(ans, 0);
                    } else {
                        ans = Math.min(ans, 1 + p[j-1]);
                    }
                }
            }
            p[i] = ans;
        }
        return p[n-1];
    }

    public static boolean isPalindrome(String[] str, byte[][] dp, int i , int j){
        if (str[i].equals(str[j])) {
            if(i == j || j-i == 1){
                return true;
            }
            int p = i+1;
            int q = j-1;
            if(p <= q && dp[p][q] == 1) {
                return true;
            }
        }
        return false;
    }


}
