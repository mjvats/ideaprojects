package dp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class LargestSubMatrixSum {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0){
            String[] line1 = br.readLine().trim().split(" ");
            int m = Integer.parseInt(line1[0]);
            int n = Integer.parseInt(line1[1]);

            int[][] dp = new int[m][n];
            for(int i = 0 ; i < m ; i++){
                String[] input = br.readLine().trim().split(" ");
                for(int j = 0 ; j < n ; j++){
                    dp[i][j] = Integer.parseInt(input[j]);
                }
            }
            bw.write(func(dp)+"\n");
            T--;
        }
        br.close();
        bw.close();
    }


    public static int sol(int[][] dp){
        int N = dp.length;
        int M = dp[0].length;

        int ans = Integer.MIN_VALUE;
        for(int i = 0 ; i < N ; i++){
            for(int j = 1 ; j < M ; j++){
                ans = Math.max(ans, dp[i][j]);
                dp[i][j] = dp[i][j] + dp[i][j-1];
            }
        }

        for(int i = 0 ; i < N ; i++){

           for(int j = 0 ; j < M ; j++){

               int p = i;
               for(; p < N ; p++){
                   ans = Math.max(ans, dp[p][j]);
               }

               int q = j;
               for(; q < M ; q++){
                   ans = Math.max(ans, dp[i][q]);
               }

               p = i+1;
               q = j+1;
               int x = 0;
               while (p < N && q < M){

                   if(j == 0){
                       x += dp[i][q] + dp[p][q];
                   } else {
                       x += (dp[i][q] - dp[i][j-1]) + (dp[p][q] - dp[p][j-1]);
                   }
                   p++;
                   q++;
               }
               ans = Math.max(ans, x);
           }
        }

        return ans;
    }

    public static int func(int[][] dp){
        int N = dp.length;
        int M = dp[0].length;

        int ans = Integer.MIN_VALUE;
        for(int i = 0 ; i < N ; i++){
            for(int j = 1 ; j < M ; j++){
                ans = Math.max(ans, dp[i][j]);
                dp[i][j] = dp[i][j] + dp[i][j-1];
            }
        }

        for(int i = 0 ; i < N ; i++){
            for(int j = 0 ; j < M ; j++){
                for(int y = j ; y < M ; y++){
                    int currentSum = 0;
                    for(int x = i ; x < N ; x++){
                        if(j == 0){
                            currentSum += dp[x][y];
                        } else {
                            currentSum += dp[x][y] - dp[x][j-1];
                        }
                        ans = Math.max(ans, currentSum);
                    }

                }
            }
        }
        return ans;
    }

}
