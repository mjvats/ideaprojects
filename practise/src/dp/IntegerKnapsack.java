package dp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class IntegerKnapsack {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0){

            String[] line1 = br.readLine().trim().split(" ");
            int knapsackSize = Integer.parseInt(line1[0]);
            int noOfItems = Integer.parseInt(line1[1]);
            int[] w = new int[noOfItems]; // array representing weights.
            int[] v = new int[noOfItems]; // array representing values.
            int n = 0;
            while (n < noOfItems){
                String[] input = br.readLine().split(" ");
                w[n] = Integer.parseInt(input[0]);
                v[n] = Integer.parseInt(input[1]);
                n++;
            }

            long ans = sol(w, v, knapsackSize, noOfItems);
            bw.write(ans+"");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.flush();
    }

    public static long sol(int[] w, int[] v, int s, int n){

        long[][] t = new long[n+1][s+1];

        for(int j = 0 ; j <= s ; j++){
            t[0][j] = 0;
        }

        for(int i = 0 ; i <= n ; i++){
            t[i][0] = 0;
        }

        for(int i = 1; i <= n ; i++){
            for(int j = 1 ; j <= s ; j++){
                //t[i][j] = Math.max(t[i-1][j], t[i-1][j-w[i-1]] + v[i-1]);
                if(w[i-1] > j){
                    t[i][j] = t[i-1][j];
                } else {
                    t[i][j] = Math.max(t[i-1][j], t[i-1][j-w[i-1]] + v[i-1]);
                }
            }
        }
        return t[n][s];
    }

}
