package dp;

public class BitonicSequence {

    public static void main(String[] args) {
        int[] arr = {15, -5, 7, 7, 6, 12, -3};
        System.out.println(bitonicSequence(arr));
    }

    public static int bitonicSequence(int[] arr){

        int n = arr.length;
        int[] l2r = new int[n];

        for(int i = 0 ; i < n ; i++){
            int ans = 0;
            for(int j = 0 ; j < i ; j++){
                if(arr[i] >= arr[j]){
                    ans = Math.max(ans, l2r[j]);
                }
            }
            l2r[i] = ans+1;
        }

        int[] r2l = new int[n];
        for(int i = n-1 ; i >= 0 ; i--){
            int ans = 0;
            for(int j = n-1 ; j > i ; j--){
                if(arr[i] >= arr[j]){
                    ans = Math.max(ans, r2l[j]);
                }
            }
            r2l[i] = ans+1;
        }

        int max = Integer.MIN_VALUE;
        for(int i = 0; i < n ; i++){
            max = Math.max(l2r[i]+r2l[i]-1, max);
        }
        return max;
    }

}
