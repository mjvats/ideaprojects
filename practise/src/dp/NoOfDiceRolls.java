package dp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NoOfDiceRolls {

    static InputStreamReader isr = new InputStreamReader(System.in);
    static OutputStreamWriter osw = new OutputStreamWriter(System.out);

    static BufferedReader br = new BufferedReader(isr);
    static BufferedWriter bw = new BufferedWriter(osw);

    public static void main(String[] args) throws IOException {
        int T = Integer.parseInt(br.readLine());
        List<Integer> inp = new ArrayList<>();
        while (T > 0){
            inp.add(Integer.parseInt(br.readLine()));
            T--;
        }
        int max = Collections.max(inp);
        sol(inp, max);
        bw.write("\n");
        br.close();
        bw.close();
    }

    static void sol(List<Integer> input, int n) throws IOException{
        long[] dp = new long[n+1];
        dp[0] = 1;
        for(int i = 1 ; i <= n ; i++){
            if(i <= 5){
                for(int j = 1; j <= i ; j++){
                    dp[i] = (dp[i]+dp[i-j])%1000000007;
                }

            } else {
                for(int j = 1; j <= 6 ; j++){
                    dp[i] = (dp[i]+dp[i-j])%1000000007;
                }
            }
        }

        for(int val : input){
            bw.write(dp[val]+"\n");
        }
    }

}
