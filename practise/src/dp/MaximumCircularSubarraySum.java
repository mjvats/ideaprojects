package dp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MaximumCircularSubarraySum {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0){
            int n = Integer.parseInt(br.readLine());
            String[] input = br.readLine().trim().split(" ");
            long[] arr = new long[n];
            for(int i = 0 ; i < n ; i++){
                arr[i] = Long.parseLong(input[i]);
            }
            bw.write(sol(arr)+"\n");
            T--;
        }
        br.close();
        bw.flush();
    }

    public static long sol(long[] arr){
        int n = arr.length;
        long[] dp = new long[n];
        dp[0] = arr[0];
        int lastIndex = -1;
        long ans = dp[0];
        for(int i = 1; i < n ; i++){
            dp[i] = Math.max(arr[i]+dp[i-1], arr[i]);
            if(dp[i] < 0){
                lastIndex = i;
            }
            ans = Long.max(ans, dp[i]);
        }

        for(int i = 0; i <= lastIndex ; i++){
            if(i == 0){
                dp[0] = Math.max(arr[0]+dp[n-1], arr[0]);
            }
            else {
                dp[i] = Math.max(arr[i]+dp[i-1], arr[i]);
            }
            ans = Long.max(ans, dp[i]);
        }
        return ans;
    }

}
