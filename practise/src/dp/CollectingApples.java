package dp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class CollectingApples {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0){
            String[] line1 = br.readLine().trim().split(" ");
            int m = Integer.parseInt(line1[0]);
            int n = Integer.parseInt(line1[1]);

            int[][] dp = new int[m][n];
            for(int i = 0 ; i < m ; i++){
                String[] input = br.readLine().trim().split(" ");
                for(int j = 0 ; j < n ; j++){
                    dp[i][j] = Integer.parseInt(input[j]);
                }
            }
            bw.write(sol(dp, m, n)+"\n");
            T--;
        }
        br.close();
        bw.close();
    }

    public static int sol(int[][] dp, int m, int n){
        for(int j = 1 ; j < n ; j++){
            dp[0][j] = dp[0][j-1] + dp[0][j];
        }

        for(int i = 1 ; i < m ; i++){
            dp[i][0] = dp[i-1][0] + dp[i][0];
        }

        for(int i = 1 ; i < m ; i++){
            for(int j = 1 ; j < n ; j++){
                dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]) + dp[i][j];
            }
        }

        return dp[m-1][n-1];
    }

}
