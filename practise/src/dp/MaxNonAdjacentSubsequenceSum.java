package dp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MaxNonAdjacentSubsequenceSum {

    static InputStreamReader isr = new InputStreamReader(System.in);
    static OutputStreamWriter osw = new OutputStreamWriter(System.out);

    static BufferedReader br = new BufferedReader(isr);
    static BufferedWriter bw = new BufferedWriter(osw);

    public static void main(String[] args) throws IOException {

        int T = Integer.parseUnsignedInt(br.readLine());
        while( T > 0){
            int n = Integer.parseUnsignedInt(br.readLine());
            int[] arr = new int[n];
            String[] input = br.readLine().trim().split(" ");
            for(int i = 0 ; i < n ; i++){
                arr[i] = Integer.parseInt(input[i]);
            }

            int result = func(arr, n);
            bw.write(result+"\n");
            T--;
        }

        br.close();
        bw.flush();
    }

    public static int func(int[] arr, int n){
        int[] dp = new int[n];
        dp[0] = arr[0];
        dp[1] = Math.max(dp[0], arr[1]);

        for(int i = 2 ; i < n ; i++){
            dp[i] = Math.max(dp[i-1], Math.max(arr[i], arr[i]+dp[i-2]));
        }

        return dp[n-1];
    }

}
