package dp;

public class LongestPalindromicSubstring {

    public static void main(String[] args) {
        String[] str = "ababbbabbababa".split("");
        System.out.println(m(str));
    }

    public static int sol(String[] str) {
        int n = str.length;
        int[][] dp = new int[n][n];
        int ans = 0;

        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n-j; i++) {
                int k = i + j;
                if (str[i].equals(str[k])) {
                    int p = i+1;
                    int q = k-1;
                    if(p <= q && dp[p][q] > 0)
                        dp[i][k] = dp[p][q] + 2;
                }
                ans = Math.max(ans, dp[i][k]);
            }
        }

        return ans;
    }

    public static int func(String[] str){
        StringBuilder sb = new StringBuilder();
        sb.append("#");
        for(String val : str){
            sb.append(val);
            sb.append("#");
        }
        String[] s = sb.toString().split("");
        int[] p = new int[s.length];

        int n = s.length;
        int l = 0;
        int r = -1;
        int ans = 0;
        for(int i = 0 ; i < n ; i++){
            int k ;
            if(i > r){
                k = 0;
            } else {
                int j = l+r-i;
                k = Math.min(p[j], r-i);
            }
            while ((i-k) > 0 && (i+k) < n && s[i - k].equals(s[i + k])){
                ++k;
            }

            int a = (i-k);
            if(a < 0){
                ++a;
            }
            int b = i+k;
            if(b >= n){
                --b;
            }
            if(!s[a].equals(s[b])){
                --k;
            }
            p[i] = k;
            ans = Math.max(ans, p[i]);
            if(i+k > r){
                l = i-k;
                r = i+k;
            }
        }

        return ans;
    }

    public static int m(String[] str){
        int n = str.length;
        int[][] dp = new int[n][n];
        int ans = 0;

        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n-j; i++) {
                int k = i + j;
                if (isPalindrome(str, dp, i , k)) {
                    dp[i][k] = 0;
                } else {
                    ans = Integer.MAX_VALUE;
                    for(int m = i ; m < k ; m++){
                        ans = Math.min(ans, 1+dp[i][m]+dp[m+1][k]);
                    }
                    dp[i][k] = ans;
                }
            }
        }

        return dp[0][n-1];
    }

    public static boolean isPalindrome(String[] str, int[][] dp, int i , int j){
        if (str[i].equals(str[j])) {
            if(i == j || j-i == 1){
                return true;
            }
            int p = i+1;
            int q = j-1;
            if(p <= q && dp[p][q] == 0) {
                return true;
            }
        }
        return false;
    }

}
