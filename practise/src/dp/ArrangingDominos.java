package dp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ArrangingDominos {

    static InputStreamReader isr = new InputStreamReader(System.in);
    static OutputStreamWriter osw = new OutputStreamWriter(System.out);

    static BufferedReader br = new BufferedReader(isr);
    static BufferedWriter bw = new BufferedWriter(osw);

    public static void main(String[] args) throws IOException {

        int T = Integer.parseInt(br.readLine());
        List<Integer> input = new ArrayList<>();
        for (int i = 0; i < T; i++) {
            input.add(Integer.valueOf(br.readLine()));
        }

        int max = Collections.max(input);
        long[] result = new long[max + 1];

        fact(input, result);
        bw.close();
        br.close();
    }

    private static void fact(List<Integer> input, long[] result) throws IOException {
        result[0] = 1;
        result[1] = 1;
        result[2] = 2;
        result[3] = 3;
        result[4] = 5;
        for (int i = 5; i < result.length; i++) {
            result[i] = ((result[i - 1]) % 1000000007 + (result[i - 2]) % 1000000007
                + (8 * (result[i - 5]) % 1000000007) % 1000000007) % 1000000007;
        }

        for (int val : input) {
            bw.write(result[val] + "\n");
        }
    }
}
