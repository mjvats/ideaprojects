package dp;

import java.math.BigInteger;

public class FibonacciModified {

    public static BigInteger fibonacciModified(int t1, int t2, int n) {
        // Write your code here
        BigInteger a = new BigInteger(t1+"");
        BigInteger b = new BigInteger(t2+"");
        BigInteger[] dp = new BigInteger[n+1];
        dp[0] = BigInteger.ZERO;
        dp[1] = a;
        dp[2] = b;
        for(int i = 3 ; i <= n ; i++){
            dp[i] = (dp[i-1].multiply(dp[i-1])).add(dp[i-2]);
        }

        return dp[n];
    }

}
