package dp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Factorial {

    public static void main(String[] args) throws IOException{
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.valueOf(br.readLine());
        int[] input = new int[T];
        for(int i = 0 ; i < input.length ; i++){
            input[i] = Integer.valueOf(br.readLine());
        }

        int max = getMax(input);
        long[] result = new long[max+1];

        fact(input, result, bw);
        bw.close();
        br.close();
    }

    private static void fact(int[] input, long[] result, BufferedWriter bw) throws IOException {
        result[0] = 1;
        result[1] = 1;
        for(int i = 2; i < result.length ; i++){
            result[i] = (i*result[i-1])%1000000007;
        }

        for(int i = 0 ; i < input.length ; i++){
            bw.write(result[input[i]] +"\n");
        }
    }

    private static int getMax(int[] arr){
        int max = arr[0];
        for(int val : arr){
            if(val > max)
                max = val;
        }
        return max;
    }
}
