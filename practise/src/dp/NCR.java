package dp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class NCR {

    static InputStreamReader isr = new InputStreamReader(System.in);
    static OutputStreamWriter osw = new OutputStreamWriter(System.out);

    static BufferedReader br = new BufferedReader(isr);
    static BufferedWriter bw = new BufferedWriter(osw);

    public static void main(String[] args) throws IOException {
        int T = Integer.parseInt(br.readLine());
        int[] N = new int[T];
        int[] R = new int[T];
        int count = 0;
        while (count < T){
            String[] input = br.readLine().trim().split(" ");
            int n = Integer.parseInt(input[0]);
            int r = Integer.parseInt(input[1]);
            N[count] = n;
            R[count] = r;
            count++;
        }
        int maxN = getMax(N);
        int maxR = getMax(R);
        long[][] t = sol(maxN, maxR);

        for(int i = 0 ; i < T ; i++){
            int n = N[i];
            int r = R[i];
            bw.write(t[n][r]+"\n");
        }
        br.close();
        bw.close();
    }

    public static long[][] sol(int n, int r){
        long[][] t = new long[n+1][r+1];
        for(int j = 0 ; j <= r ; j++){
            t[0][j] = 0L;
        }

        for(int i = 0 ; i <= n ; i++){
            t[i][0] = 1;
        }

        for(int i = 1 ; i <= n ; i++){
            for(int j = 1 ; j <= r ; j++){
                t[i][j] = (t[i-1][j] + t[i-1][j-1])%1000000007;
            }
        }

        return t;
    }

    public static int getMax(int[] arr){
        int max = Integer.MIN_VALUE;
        for(int val : arr) max = Math.max(max, val);
        return max;
    }

}
