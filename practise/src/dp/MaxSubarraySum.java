package dp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class MaxSubarraySum {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0){
            int n = Integer.parseInt(br.readLine());
            String[] input = br.readLine().trim().split(" ");
            int[] arr = new int[n];

            for(int i = 0 ; i < input.length ; i++){
                arr[i] = Integer.parseInt(input[i]);
            }
            sol(arr).stream().forEach(x -> {
                try {
                    bw.write(x+" ");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            bw.write("\n");
            T--;
        }

        bw.close();
        br.close();
    }

    static ArrayList<Long> func(int[] inp) {
        int n = inp.length;
        long[] arr = new long[n];
        long p = 0L;
        long q = 0L;
        arr[0] = inp[0];
        long max = arr[0];
        for(int i = 1 ; i < n ; i++){
            if((arr[i-1]+inp[i]) < inp[i]){
                arr[i] = inp[i];
                if(arr[i] > max){
                    p = i;
                    q = i;
                    max = arr[i];
                }
            } else {
                arr[i] = arr[i-1] + inp[i];
                if(arr[i] >= max){
                    q = i;
                }
                max = Math.max(max, arr[i]);
            }
        }
        ArrayList<Long> result = new ArrayList<>();
        result.add(max);
        result.add(p);
        result.add(q);
        return result;
    }

    static ArrayList<Long> sol(int[] inp){
        ArrayList<ArrayList<Long>> result = new ArrayList<>();
        int n = inp.length;
        long[] arr = new long[n];
        arr[0] = inp[0];
        long p = 0L;
        long q = 0L;
        long max = arr[0];
        for(int i = 1 ; i < n ; i++) {
            arr[i] = Math.max(inp[i], inp[i] + arr[i-1]);
            if(arr[i] > max){
                max = arr[i];
                if(arr[i-1] < 0){
                    p = i;
                }
                q = i;
            } else if ( arr[i] < max) {
                ArrayList<Long> temp = new ArrayList<>();
                temp.add(max);
                temp.add(p);
                temp.add(q);
                result.add(temp);
                max = arr[i];
                if(arr[i-1] < 0){
                    p = i;
                }
                q = i;
            }
        }

        ArrayList<Long> temp = new ArrayList<>();
        temp.add(max);
        temp.add(p);
        temp.add(q);
        result.add(temp);

        ArrayList<Long> maxList = new ArrayList<>();
        max = Integer.MIN_VALUE;
        for(ArrayList<Long> list : result){
            if(list.get(0) > max) {
                max = list.get(0);
                maxList = list;
            }
        }
        return maxList;
    }

}
