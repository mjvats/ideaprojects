package dp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class PathInAMatrix {

    static int ans;

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            ans = 0;
            String[] line1 = br.readLine().trim().split(" ");
            int N = Integer.parseInt(line1[0]);
            int M = Integer.parseInt(line1[1]);
            int K = Integer.parseInt(line1[2]);
            int[][] dp = new int[N][M];
            while (K > 0) {
                String[] nextLine = br.readLine().trim().split(" ");
                int i = Integer.parseInt(nextLine[0]);
                int j = Integer.parseInt(nextLine[1]);
                dp[i][j] = -1; //-1 represents that the cell is blocked
                K--;
            }

            /*//result obtained from recursive approach
            if (dp[0][0] == -1 || dp[N - 1][M - 1] == -1) {
                bw.write(String.valueOf(0));
            } else {
                sol(dp, N - 1, M - 1);
                bw.write(String.valueOf(ans));
            }*/

            //result obtained from iterative approach
            bw.write(String.valueOf(func(dp, N, M)));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static void sol(int[][] dp, int i, int j) {
        if (dp[i][j] == -1) {
            return;
        }
        if (i == 0 && j == 0) {
            ans++;
            return;
        }
        if (i > 0) {
            sol(dp, i - 1, j);
        }
        if (j > 0) {
            sol(dp, i, j - 1);
        }
        if (i > 0 && j > 0) {
            sol(dp, i - 1, j - 1);
        }
    }

    public static int func(int[][] dp, int n, int m){
        if(dp[0][0] == -1 || dp[n-1][m-1] == -1){
            return 0;
        }

        for(int i = 0 ; i < n ; i++){
            for(int j = 0; j < m ; j++){
                if(dp[i][j] != -1){
                    if(i == 0 && j == 0){
                        dp[i][j] = 1;
                    } else if (i == 0){
                        dp[i][j] = dp[i][j-1];
                    } else if (j == 0){
                        dp[i][j] = dp[i-1][j];
                    } else {
                        dp[i][j] = dp[i-1][j]+dp[i][j-1]+dp[i-1][j-1];
                    }
                } else {
                    dp[i][j] = 0;
                }
            }
        }

        return dp[n-1][m-1];
    }

}
