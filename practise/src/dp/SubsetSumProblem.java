package dp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class SubsetSumProblem {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0){
            String[] line1 = br.readLine().trim().split(" ");
            int n = Integer.parseInt(line1[0]);
            int s = Integer.parseInt(line1[1]);

            int[] arr = new int[n];
            String[] nextLine = br.readLine().trim().split(" ");
            int i = -1;
            for(String val : nextLine){
                arr[++i] = Integer.parseInt(val);
            }
            bw.write(sol(arr, n, s) ? "YES" : "NO");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static boolean sol(int[] arr, int n, int s) {
        if(s == 0) return true;
        boolean[][] dp = new boolean[n + 1][s + 1];
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j <= s; j++) {
                if (arr[i - 1] > j) {
                    dp[i][j] = dp[i - 1][j];
                } else if (arr[i - 1] == j) {
                    dp[i][j] = true;
                } else {
                    dp[i][j] = dp[i - 1][j - arr[i - 1]] || dp[i-1][j];
                }
            }
        }
        return dp[n][s];
    }

}
