public class SameSign {

    public static void main(String[] args) {
        int a = -2;
        int b = 3;
        System.out.println(sameSignCheck(a,b));
    }

    public static boolean sameSign(int a, int b){
        a = (a>>31)&1;
        b = (b>>31)&1;
        return a == b;
    }

    public static boolean sameSignCheck(int a, int b){
        return (a^b) >= 0;
    }

}
