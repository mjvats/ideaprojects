package linkedlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class MyLinkedList {

    ListNode head;
    ListNode last;
    int length;

    MyLinkedList() {
        this.head = null;
        this.last = null;
        this.length = 0;
    }

    public void add(int val) {
        ListNode node = new ListNode(val);
        if (last != null) {
            last.next = node;
            last = node;
        } else {
            head = node;
            last = head;
        }
        length++;
    }

    public ListNode oddEvenList() {
        return this.oddEvenList(head);
    }

    public ListNode oddEvenList(ListNode head) {
        if (head == null) {
            return null;
        }

        int nodeNum = 1;
        ListNode t1 = head, t2 = head.next, temp = t2;
        boolean flag = true;

        while (t1.next != null && t2.next != null) {
            nodeNum++;
            System.out.println("flag:" + flag);
            if (flag) {
                t1.next = t2.next;
                t1 = t2.next;
            } else {
                t2.next = t1.next;
                t2 = t1.next;
            }
            flag = !flag;
        }
        t2.next = t1.next;
        t1.next = temp;
        return nodeNum > 10000 ? null : head;
    }

    public ListNode getMid() {
        return getMid(head);
    }

    public ListNode getMid(ListNode head) {
        if (head == null) {
            return null;
        }

        ListNode fNode = head;
        ListNode sNode = head;
        while (sNode.next != null && sNode.next.next != null) {
            sNode = sNode.next.next;
            fNode = fNode.next;
        }

        if (sNode.next != null && sNode.next.next == null) {
            System.out.println("List has even nodes");
        } else {
            System.out.println("List has odd nodes");
        }

        return fNode.next;
    }

    public boolean isPalindrome() {
        return isPalindrome(head);
    }

    public boolean isPalindrome(ListNode head) {

        ListNode tempHead = getMid(head);
        tempHead = this.reverse(tempHead);

        boolean isPalindrome = true;

        ListNode curr = head;
        while (tempHead != null) {
            if (curr.val != tempHead.val) {
                isPalindrome = false;
                break;
            } else {
                curr = curr.next;
                tempHead = tempHead.next;
            }
        }

        return isPalindrome;
    }

    public ListNode reverse() {
        return reverse(head);
    }

    public ListNode reverse(ListNode head) {
        if (head == null) {
            return null;
        }

        ListNode curr = head.next;
        ListNode temp = head;

        while (head.next != null) {
            head.next = curr.next;
            curr.next = temp;
            temp = curr;
            curr = head.next;
        }

        return temp;
    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null) {
            return null;
        } else if (l1 == null) {
            return l2;
        } else if (l2 == null) {
            return l1;
        } else {
            ListNode node1 = l1;
            ListNode node2 = l2;
            ListNode temp;
            boolean flag = true;

            while (node1 != null && node2 != null) {
                if (flag) {
                    temp = node1.next;
                    node1.next = node2;
                    node1 = temp;
                    flag = false;
                } else {
                    temp = node2.next;
                    node2.next = node1;
                    node2 = temp;
                    flag = true;
                }

            }
            return l1;
        }
    }

    public ListNode removeDuplicates() {
        return removeDuplicates(head);
    }

    /**
     * This method removes all elements which occurs more than once.
     */
    public ListNode removeDuplicates(ListNode A) {
        ListNode curr = A;
        ListNode prev = null;
        ListNode head = A;
        ListNode temp = null;

        while (curr != null) {
            boolean isUnique = false;
            if ((prev == null && curr.next == null)) {
                isUnique = true;
            } else if (prev == null) {
                if (curr.next.val != curr.val) {
                    isUnique = true;
                }
            } else if (curr.next == null) {
                if (prev.val != curr.val) {
                    isUnique = true;
                }
            } else {
                if (prev.val != curr.val && curr.next.val != curr.val) {
                    isUnique = true;
                }
            }

            if (isUnique) {
                if (temp != null) {
                    temp.next = curr;
                } else {
                    head = curr;
                }
                temp = curr;
            }
            prev = curr;
            curr = curr.next;
        }
        if (temp != null) {
            temp.next = curr;
            return head;
        }
        return null;
    }

    public ListNode sortBinary() {
        return sortBinary(head);
    }

    public ListNode sortBinary(ListNode A) {
        if (A == null) {
            return null;
        }

        ListNode curr = A;
        while (curr != null) {
            if (curr.val == 1) {
                break;
            }
            curr = curr.next;
        }

        ListNode temp = curr;

        while (curr != null) {
            if (curr.val == 0) {
                temp.val = 0;
                curr.val = 1;
                while (temp != curr && temp.val != 1) {
                    temp = temp.next;
                }
            }
            curr = curr.next;
        }
        return A;
    }

    ListNode partitionList(int k) {
        return partitionList(head, k);
    }

    ListNode partitionList(ListNode A, int k) {
        if (A == null) {
            return null;
        }

        ListNode dummy = A;
        ListNode curr = A;
        ListNode prev = curr;
        while (curr != null) {
            if (curr.val >= k) {
                ListNode temp = curr.next;
                ListNode tempPrev = curr;
                while (temp != null && temp.val >= k) {
                    tempPrev = temp;
                    temp = temp.next;
                }
                if (temp != null) {
                    if (prev.next != temp && prev != curr) {
                        prev.next = temp;
                    }
                    tempPrev.next = temp.next;
                    temp.next = curr;
                    prev = temp;
                    curr = temp;
                } else {
                    return dummy;
                }
            } else {
                prev = curr;
                curr = curr.next;
            }
            if (dummy.val >= k && curr.val < k) {
                dummy = curr;
            }
        }
        return dummy;
    }

    public ListNode insertionSort() {
        return insertionSort(head);
    }

    public ListNode insertionSort(ListNode A) {
        if (A == null) {
            return null;
        }

        ListNode curr = head.next;
        ListNode prev = head;

        while (curr != null) {
            if (curr.val < prev.val) {
                ListNode temp = head;
                while (temp.next != curr && temp.next.val <= curr.val) {
                    temp = temp.next;
                }
                prev.next = curr.next;
                if (curr.val < temp.val) {
                    if (temp == head) {
                        head = curr;
                    }
                    curr.next = temp;
                } else {
                    curr.next = temp.next;
                    temp.next = curr;
                }
                curr = prev;
            } else {
                prev = curr;
                curr = curr.next;
            }
        }
        return head;
    }

    ListNode mergeSort() {
        return mergeSort(head, 0, length - 1);
    }

    ListNode mergeSort(ListNode head, int start, int end) {
        if (start < end) {
            int mid = (start + end) / 2;
            ListNode head1 = head;
            ListNode head2 = head;
            int i = start - 1;
            while (head2 != null) {
                i++;
                if (i == mid + 1) {
                    break;
                } else {
                    head2 = head2.next;
                }
            }

            head1 = mergeSort(head1, start, mid);
            head2 = mergeSort(head2, mid + 1, end);
            return merge(head1, head2, start, end);
        }
        Stack<Integer> s = new Stack<>();
        return head;
    }

    ListNode merge(ListNode head1, ListNode head2, int start, int end) {

        int mid = (start + end) / 2;
        ListNode newHead = null;
        ListNode dummy = new ListNode(Integer.MIN_VALUE);
        int i = start, j = mid + 1;
        while ((head1 != null && i <= mid) && (head2 != null && j <= end)) {
            if (head1.val < head2.val) {
                dummy.next = head1;
                dummy = head1;
                head1 = head1.next;
                i++;
            } else {
                dummy.next = head2;
                dummy = head2;
                head2 = head2.next;
                j++;
            }
            if (newHead == null) {
                newHead = dummy;
            }
        }

        while (head1 != null && i <= mid) {
            dummy.next = head1;
            dummy = head1;
            head1 = head1.next;
            i++;
        }

        while (head2 != null && j <= end) {
            dummy.next = head2;
            dummy = head2;
            head2 = head2.next;
            j++;
        }

        dummy.next = null;
        return newHead;
    }

    ListNode reverseKGroup(int k) {
        return reverseKGroup(head, k);
    }

    ListNode reverseKGroup(ListNode head, int k) {

        int i = 0;
        ListNode curr = head;
        ListNode last = head;
        ListNode tempHead = null;
        while (curr != null) {
            int diff = length - i;
            if (diff < k) {
                break;
            }
            head = curr;
            int count = k;
            while (count > 0) {
                curr = curr.next;
                count--;
                i++;
            }
            ListNode newHead = reverseK(head, k).get(0);
            if (newHead == null) {
                break;
            }
            if (tempHead == null) {
                tempHead = newHead;
            }
            last.next = newHead;
            last = head;

        }
        last.next = curr;
        return tempHead;
    }

    List<ListNode> reverseK(ListNode head, int k) {
        if (head == null) {
            return null;
        }
        ListNode curr = head;
        ListNode dummy = null;
        for (int i = 0; curr != null && i < k; i++) {
            ListNode prev = curr;
            curr = curr.next;
            prev.next = dummy;
            dummy = prev;
        }
        List<ListNode> list = new ArrayList<>();
        list.add(dummy);
        list.add(head);
        return list;
    }

    ListNode reverseInRange(int left, int right) {
        return reverseInRange(head, left, right);
    }

    ListNode reverseInRange(ListNode head, int left, int right) {
        if (head == null) {
            return null;
        }

        int k = (right - left + 1);
        int position = 0;

        ListNode dummy = new ListNode(Integer.MIN_VALUE, head);
        ListNode curr = dummy;
        ListNode first = null;
        ListNode last = null;
        while (curr != null) {
            if (position == left - 1) {
                first = curr;
            }
            if (position == right) {
                last = curr.next;
            }
            position++;
            curr = curr.next;
        }

        ListNode newHead = reverseK(first.next, k).get(0);
        first.next = newHead;
        curr = newHead;
        while (curr.next != null) {
            curr = curr.next;
        }
        curr.next = last;
        return dummy.next;
    }

    ListNode evenReverse() {
        return evenReverse(head);
    }

    ListNode evenReverse(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode temp1 = head;
        ListNode head2 = head.next;
        ListNode temp2 = head2;

        while (temp1 != null && temp2 != null) {
            temp1.next = temp2.next;
            temp1 = temp1.next;

            if (temp1 != null) {
                temp2.next = temp1.next;
                temp2 = temp2.next;
            }
        }

        ListNode reversedHead = reverse(head2);
        ListNode origHead = head;
        ListNode dummy = new ListNode(Integer.MIN_VALUE);
        boolean flag = true;
        while (origHead != null && reversedHead != null) {
            if (flag) {
                dummy.next = origHead;
                dummy = origHead;
                origHead = origHead.next;
                flag = false;
            } else {
                dummy.next = reversedHead;
                dummy = reversedHead;
                reversedHead = reversedHead.next;
                flag = true;
            }
        }

        if (origHead == null) {
            dummy.next = reversedHead;
        }
        if (reversedHead == null) {
            dummy.next = origHead;
        }

        return head;
    }

    ListNode rotateList(int k) {
        return rotateList(head, k);
    }

    ListNode rotateList(ListNode head, int k) {
        if (head == null) {
            return null;
        }
        int d = 0;
        if (k == length) {
            return reverse(head);
        } else if (k < length) {
            d = length - k;
        } else {
            d = length - (k % length);
        }

        ListNode first = head;
        int count = 0;
        while (head != null && count < d) {
            count++;
            head = head.next;
        }

        ListNode reversedH1 = reverseK(first, d).get(0);
        first.next = reverse(head);
        return reverse(reversedH1);
    }

    int kthNodeFromMiddle(int k) {
        return kthNodeFromMiddle(head, k);
    }

    int kthNodeFromMiddle(ListNode head, int k) {
        if (head == null) {
            return -1;
        }
        int mid = (length / 2) + 1;
        int diff = mid - k;
        if (diff == 0) {
            return head.val;
        } else if (diff < 0) {
            return -1;
        }

        ListNode curr = head;
        int i = 0;
        while (curr != null) {
            i++;
            if (i == diff) {
                break;
            } else {
                curr = curr.next;
                Stack s = new Stack();
                s.push(2);
            }
        }
        return curr.val;
    }

    ListNode reverseAlternateKNodes(int k) {
        return reverseAlternateKNodes(head, k);
    }

    ListNode reverseAlternateKNodes(ListNode head, int k) {
        if (head == null) {
            return null;
        }
        ListNode curr = head;
        ListNode last = head;
        ListNode prev = null;
        ListNode tempHead = null;
        ListNode newHead = null;
        boolean flag = true;
        int i = 0;
        while (curr != null) {
            int diff = length - i;
            if (diff < k) {
                last.next = curr;
                break;
            }
            int count = k;
            while (count > 0) {
                i++;
                prev = curr;
                curr = curr.next;
                count--;
            }
            if (!flag) {
                last.next = head;
                last = prev;
                flag = true;
            } else {
                List<ListNode> result = reverseK(head, k);
                newHead = result.get(0);
                last.next = newHead;
                last = result.get(1);
                flag = false;
            }
            if (tempHead == null) {
                tempHead = newHead;
            }
            head = curr;

            Map<Integer, Integer> map = new HashMap<>();

        }
        last.next = null;
        return tempHead;
    }

    ListNode reorder() {
        return reorder(head);
    }

    ListNode reorder(ListNode head) {
        if (head == null) {
            return null;
        }

        ListNode mid = getMid(head);
        ListNode curr = head;
        while (curr.next != mid) {
            curr = curr.next;
        }
        curr.next = null;
        ListNode reversedHead = reverse(mid);

        return mergeTwoLists(head, reversedHead);
    }

    ListNode cycle(){
        return cycle(head);
    }

    ListNode cycle(ListNode head){
        if (head == null){
            return null;
        }
        ListNode slow = head;
        ListNode fast = head;

        while(slow != null && fast!= null && fast.next != null){
            slow = slow.next;
            fast = fast.next.next;
            if(slow == fast){
                return slow;
            }
        }
        return null;
    }
}
