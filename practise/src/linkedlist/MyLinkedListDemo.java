package linkedlist;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MyLinkedListDemo {

    public static void main(String[] args) throws IOException {
        MyLinkedList list = new MyLinkedList();

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        String[] input = br.readLine().trim().split(" ");
        for(String val : input){
            list.add(Integer.parseInt(val));
        }

        ListNode head = list.head;
        ListNode last = list.last;
        ListNode curr = head;
        while(curr != null){
            if(curr.val == Integer.parseInt(input[191])){
                last.next = curr;
                break;
            }
            curr = curr.next;
        }

/*        ListNode head = list.reverse();
        while (head != null){
            System.out.println(head.val);
            head = head.next;
        }*/
        head = list.cycle();
        while (head != null){
            System.out.print(head.val+" ");
            head = head.next;
        }

        br.close();
        bw.close();
    }
}
