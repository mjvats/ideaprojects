import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class TheTwoDishes {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            String[] input = br.readLine().trim().split(" ");
            int N = Integer.parseInt(input[0]);
            int S = Integer.parseInt(input[1]);
            bw.write(sol(N, S)+"");
            bw.write("\n");
            T--;
        }
        br.close();
        bw.close();
    }

    public static int sol(int N, int S){
        int a;
        int b;
        int diff;
        if(N >= S){
            diff = S;
        }else{
            a = N;
            b = S-N;
            diff = Math.abs(a-b);
        }
        return diff;
    }

}
