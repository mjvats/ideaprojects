public class AddBinary {

    public static void main(String args[]) {
        String a = "0";
        String b = "0";

        String result = addBinary(a, b);
        System.out.println(result);
    }

    private static String addBinary(String a, String b) {

        if (!validateInput(a, b)) {
            return "";
        }

        char[] char_a = a.toCharArray();
        char[] char_b = b.toCharArray();

        if (char_b.length > char_a.length) {
            char[] c = char_b;
            char_b = char_a;
            char_a = c;
        }

        char[] out = new char[char_a.length+1];
        int outIndex = out.length-1;
        int carry = 0;

        int i = char_a.length - 1, j = char_b.length - 1;
        for (; i >= 0 && j >= 0; i--, j--) {
            if (char_a[i] == '1' && char_b[j] == '1') {
                if(carry == 1) {
                    //out = '1' + out;
                    out[outIndex] = '1';
                    outIndex--;
                }
                else{
                    //out = '0' + out;
                    out[outIndex] = '0';
                    outIndex--;
                }
                carry = 1;
            } else if ((char_a[i] == '1' && char_b[j] == '0') || (char_a[i] == '0'
                && char_b[j] == '1')) {
                if (carry == 1) {
                    //out = '0' + out;
                    out[outIndex] = '0';
                    outIndex--;
                    carry = 1;
                } else {
                    //out = '1' + out;
                    out[outIndex] = '1';
                    outIndex--;
                }
            } else {
                if (carry == 1) {
                    //out = '1' + out;
                    out[outIndex] = '1';
                    outIndex --;
                    carry = 0;
                } else {
                    //out = '0' + out;
                    out[outIndex] = '0';
                    outIndex--;
                }
            }
        }

        if (j < 0 && i >= 0) {
            while (i >= 0) {
                if (carry == 1) {
                    if (char_a[i] == '1') {
                        //out = '0' + out;
                        out[outIndex] = '0';
                        outIndex--;
                        carry = 1;
                    } else {
                        out[outIndex] = '1';
                        outIndex--;
                        carry = 0;
                    }
                }
                else{
                    out[outIndex] = char_a[i];
                    outIndex--;
                }
                i--;
            }
        }

        if(carry == 1){
            out[outIndex] = '1';
            return String.valueOf(out);
        }
        else {
            char[] temp = new char[out.length-1];
            for(int k = 1; k < out.length ; k++){
                temp[k-1] = out[k];
            }
            return String.valueOf(temp);
        }
    }

    private static boolean validateInput(String a, String b) {
        if (a.isEmpty() || b.isEmpty() ||
            a.length() < 1 || a.length() > 10000 ||
            b.length() < 1 || b.length() > 10000) {
            return false;
        }

        for (int i = 0; i < a.length(); i++) {
            if (i == 0) {
                if (a.length() > 1 && a.charAt(i) == '0') {
                    return false;
                }
            } else if (a.charAt(i) != '0' && a.charAt(i) != '1') {
                return false;
            }
        }

        for (int i = 0; i < b.length(); i++) {
            if (i == 0 && b.length() > 1 && b.charAt(i) == '0') {
                return false;
            } else if (b.charAt(i) != '0' && b.charAt(i) != '1') {
                return false;
            }
        }
        return true;
    }
}
