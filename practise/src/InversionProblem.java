public class InversionProblem {

    static int count = 0;
    public static void main(String[] args) {
        int[] arr = {15, 5, 20, 3, 18, 4, -7, 12, 4, -2};
        System.out.println(func(arr));
        mergeSort(arr, 0, 9);
        System.out.println(count);
    }

    static void mergeSort(int[] A, int start, int end){
        if(start == end){
            return;
        }
        int mid = (start + end)/2;
        mergeSort(A, start, mid);
        mergeSort(A, mid+1, end);
        merge(A, start, end);
    }

    static void merge(int[] A, int start, int end){
        int mid = (start + end)/2;
        int[] L = new int[mid - start +1];
        int[] R = new int[end - mid];
        int k = 0;
        for(int i = start ; i <= mid ; i++){
            L[k++] = A[i];
        }
        k=0;
        for(int i = mid+1 ; i <= end ; i++){
            R[k++] = A[i];
        }

        int i = 0; int j = 0; k = start;
        while(i < L.length && j < R.length){
            if(L[i] <= R[j]){
                A[k++] = L[i];
                i++;
            }
            else{
                count += L.length-i;
                A[k++] = R[j];
                j++;
            }
        }

        while(i < L.length){
            A[k++] = L[i];
            i++;
        }

        while(j < R.length){
            A[k++] = R[j];
            j++;
        }
    }

    static int func(int[] arr){
        int count = 0;
        int n = arr.length;

        for(int i = 0 ; i < n ; i++){
            for(int j = i+1 ; j < n ; j++){
                if(arr[i] > arr[j]){
                    count++;
                }
            }
        }

        return count;
    }
}
