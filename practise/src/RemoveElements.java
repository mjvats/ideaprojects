import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RemoveElements {

    public static void main(String[] args) {
        Integer[] A = {4,1,1,2,1,3};
        List<Integer> list = Arrays.asList(A);
        removeElement(list, 1);
    }

    public static int removeElement(List<Integer> list, int b) {
        ArrayList<Integer> a = new ArrayList<>(list);
        int i = -1;
        int j = -1;
        while(i < a.size()-1){
            if(a.get(i+1).equals(b)){
                j = i+1;
                while(j < a.size()-1){
                    if(!a.get(j+1).equals(b)){
                        swap(a, i+1, j+1);
                        break;
                    }
                    j++;
                }
                if(j == a.size()-1)
                    break;
            }
            i++;
        }
        return i+1;
    }

    public static void swap(ArrayList<Integer> a, int i , int j){
        a.set(i, a.get(i) ^ a.get(j));
        a.set(j, a.get(i) ^ a.get(j));
        a.set(i, a.get(i) ^ a.get(j));
    }

}
