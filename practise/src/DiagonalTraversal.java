import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class DiagonalTraversal {

    public static void main(String[] args) throws IOException {

        /*BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        bw.write("Mrityunjay"+" ");
        bw.close();*/
        int[][] input = {{1,2,3}, {4,5,6},{7,8,9}};
       diagonalTraversal(input, new BufferedWriter(new OutputStreamWriter(System.out)));
    }

    static private void diagonalTraversal(int[][] M, BufferedWriter bw) throws IOException{

        for(int x = 0, y = M[0].length-1; y >= 0 ; y--){
            int sum = 0;
            int i = x, j = y;
            while(j < M[0].length){
                sum += M[i][j];
                i++;
                j++;
            }
            bw.write(sum+" ");
        }

        for(int x = 1, y = 0 ; x <= M.length ; x++){
            int sum = 0;
            int i = x, j = y;
            while(i < M.length){
                sum += M[i][j];
                i++;
                j++;
            }
            bw.write(sum+" ");
        }
        bw.close();
    }
}
