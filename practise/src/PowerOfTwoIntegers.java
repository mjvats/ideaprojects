public class PowerOfTwoIntegers {

    public static void main(String[] args) {
        System.out.println(sol(536870912));
    }

    public static boolean sol(int n){
        for(int i = 2 ; i <= Math.sqrt(n) ; i++){
            if(n % i == 0){
                double k = Math.log(n)/Math.log(i);
                if((k - (int)k) < 0.00000001){
                    return true;
                }
            }
        }
        return false;
    }

}
