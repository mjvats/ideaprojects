public class PathCount {

    int ans = 0;

    public static void main(String[] args) {
        PathCount p = new PathCount();
        int n = 3;
        int m = 3;
        int x = 1;
        int y = 1;

        System.out.println(p.countPathsThroughCell(n, m, x, y));
    }

    int countPathsThroughCell(int n, int m, int x, int y) {
        int[][] matrix = new int[n][m];
        func(matrix, n-1, m-1, x, y, 0);
        return ans;
    }

    void func(int[][] m, int i, int j, int x, int y, int count){
        if(i < 0 || j < 0){
            return;
        }

        if(i == 0 && j == 0 && count > 0){
            ans++;
        }

        if(i == x && j == y){
            count++;
        }

        if(j == 0){
            func(m, i-1, j, x, y, count);
        }

        if(i == 0){
            func(m, i, j-1, x, y, count);
        }

        if(i > 0 && j > 0){
            func(m, i-1, j, x, y, count);
            func(m, i, j-1, x, y, count);
        }

    }

}
