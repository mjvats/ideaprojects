import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class SmallerElements {

    static long count;
    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0)
        {
            count = 0;
            int n = Integer.parseInt(br.readLine());
            int[] a = new int[n];
            String[] line1 = br.readLine().trim().split(" ");
            for(int i = 0 ; i < n ; i++) a[i] = Integer.parseInt(line1[i]);
            sol(a);
            bw.write(count+"");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static void sol(int[] arr) {
        mergeSort(arr, 0, arr.length - 1);
    }

    public static void mergeSort(int[] arr, int start, int end) {
        if (start < end) {
            int mid = (start + end) / 2;
            mergeSort(arr, start, mid);
            mergeSort(arr, mid + 1, end);
            merge(arr, start, end);
        }
    }

    public static void merge(int[] arr, int start, int end) {
        int mid = (start + end) / 2;
        int[] L = new int[mid - start + 1];
        int[] R = new int[end - mid];

        int k = start;
        for (int i = 0; i < L.length; ) {
            L[i++] = arr[k++];
        }
        for (int i = 0; i < R.length; ) {
            R[i++] = arr[k++];
        }

        int i = 0, j = 0;
        k = start;
        while (i < L.length && j < R.length) {
            if (R[j] < L[i]) {
                count += L.length - i;
                arr[k++] = R[j++];
            } else {
                arr[k++] = L[i++];
            }
        }

        while (i < L.length) {
            arr[k++] = L[i++];
        }
        while (j < R.length) {
            arr[k++] = R[j++];
        }
    }

}
