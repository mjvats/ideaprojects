package graph;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class PathInGraph {

    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        int count = 0;
        while (count < T) {
            bw.write("Test Case #"+(count+1)+":\n");
            String[] line1 = br.readLine().trim().split(" ");
            int N = Integer.parseInt(line1[0]);
            int M = Integer.parseInt(line1[1]);
            AdjacencyList graph = new AdjacencyList(N + 1, false);
            while (M > 0) {
                String[] nextLine = br.readLine().trim().split(" ");
                int u = Integer.parseInt(nextLine[0]);
                int v = Integer.parseInt(nextLine[1]);
                graph.addUnweightedPath(u, v);
                graph.addUnweightedPath(v, u);
                M--;
            }

            int Q = Integer.parseInt(br.readLine());
            while (Q > 0) {
                String[] query = br.readLine().trim().split(" ");
                int u = Integer.parseInt(query[0]);
                int v = Integer.parseInt(query[1]);
                bw.write(graph.dfs(u, v) ? "Yes" : "No");
                bw.write("\n");
                Q--;
            }
            count++;
        }
        br.close();
        bw.close();
    }

}
