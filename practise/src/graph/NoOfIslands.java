package graph;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import javafx.util.Pair;

public class NoOfIslands {

    static char[][] m;

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            String[] line1 = br.readLine().trim().split(" ");
            int R = Integer.parseInt(line1[0]);
            int C = Integer.parseInt(line1[1]);

            m = new char[R][C];
            int row = 0;
            while (row < R) {
                char[] input = br.readLine().trim().toCharArray();
                for (int j = 0; j < C; j++) {
                    m[row][j] = input[j];
                }
                row++;
            }
            bw.write(noOfIslandIterative(m) + "");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int noOfIsland(char[][] m) {
        int R = m.length;
        int C = m[0].length;
        int islandCount = 0;
        for (int i = 0; i < R; i++) {
            for (int j = 0; j < C; j++) {
                if (m[i][j] == '1') {
                    islandCount++;
                    dfs(i, j, R, C);
                }
            }
        }
        return islandCount;
    }

    public static void dfs(int i, int j, int R, int C) {
        if (i < 0 || i >= R || j < 0 || j >= C || m[i][j] == '0') {
            return;
        }
        m[i][j] = '0';
        int[] di = {-1, 1, 0, 0, -1, -1, 1, 1};
        int[] dj = {0, 0, -1, 1, -1, 1, -1, 1};
        for (int k = 0; k < di.length; k++) {
            dfs(i + di[k], j + dj[k], R, C);
        }
    }

    public static int noOfIslandIterative(char[][] m) {
        int R = m.length;
        int C = m[0].length;
        int islandCount = 0;
        Queue<Pair<Integer, Integer>> q = new LinkedList<>();
        int[] di = {-1, 1, 0, 0, -1, -1, 1, 1};
        int[] dj = {0, 0, -1, 1, -1, 1, -1, 1};
        for (int i = 0; i < R; i++) {
            for (int j = 0; j < C; j++) {
                if (m[i][j] == '1') {
                    islandCount++;
                    m[i][j] = '0';
                    q.add(new Pair<>(i, j));
                    while (!q.isEmpty()){
                        Pair<Integer, Integer> p = q.poll();
                        //m[p.getKey()][p.getValue()] = '0';
                        for (int k = 0; k < di.length; k++) {
                            int x = p.getKey() + di[k];
                            int y = p.getValue() + dj[k];
                            if(x >= 0 && x < R && y >= 0 && y < C && m[x][y] == '1'){
                                m[x][y] = '0';
                                q.add(new Pair<>(x, y));
                            }
                        }
                    }
                }
            }
        }
        return islandCount;
    }
}
