package graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import javafx.util.Pair;

public class AdjacencyList {

    //This ArrayList represents a weighted graph. Pair is used to store both edge and weight.
    ArrayList<ArrayList<Pair<Integer, Integer>>> weightedGraph;

    //This ArrayList represents an unweighted graph. Storing weight is not required.
    ArrayList<ArrayList<Integer>> unweightedGraph;

    //Represents the size of the graph.
    int size;

    /**
     * This is the public constructor which instantiates both weighted and unweighted graph based
     * upon the parameter passed. Every index is initialized with an empty ArrayList.
     *
     * @param size size of the graph
     * @param weighted boolean flag representing if the graph to be instantiated should be weighted
     * or unweighted.
     */
    AdjacencyList(int size, boolean weighted) {
        if (weighted) {
            weightedGraph = new ArrayList<>();
            for (int i = 0; i <= size; i++) {
                weightedGraph.add(i, new ArrayList<>());
            }
        } else {
            unweightedGraph = new ArrayList<>();
            for (int i = 0; i <= size; i++) {
                unweightedGraph.add(i, new ArrayList<>());
            }
        }
        this.size = size;
    }

    /**
     * This method does the bfs on given graph. It can return two possible things, one is that
     * destination is found or not, second is the longest path in graph.
     *
     * @return Pair whose first value is : farthest node from source and second value is : longest
     * path in graph
     */
    public Pair<Integer, Integer> bfs(Integer source) {
        Queue<Integer> q = new LinkedList<>();
        boolean[] visited = new boolean[this.size];
        visited[source] = true;
        q.add(source);
        q.add(null);
        int path = 0;
        Integer prev = null;
        Integer curr;
        while (!q.isEmpty()) {
            curr = q.poll();
            if (curr == null) {
                if (!q.isEmpty()) {
                    path++;
                    q.add(null);
                } else {
                    break;
                }
            } else {
                prev = curr;
                for (int neighbour : unweightedGraph.get(curr)) {
                    if (!visited[neighbour]) {
                        visited[neighbour] = true;
                        q.add(neighbour);
                    }
                }
            }

        }
        Pair<Integer, Integer> p = new Pair(prev, path);
        return p;
    }

    /**
     * Method to add path between source and destination.
     *
     * @param source source.
     * @param destination destination.
     */
    public void addUnweightedPath(int source, int destination) {
        if (unweightedGraph.get(source) == null) {
            ArrayList<Integer> newPath = new ArrayList<>();
            newPath.add(destination);
            unweightedGraph.add(source, newPath);
        } else {
            unweightedGraph.get(source).add(destination);
        }
    }

    /**
     * Method to add path between source and destination and also store it's weight.
     *
     * @param source source.
     * @param destination destination.
     * @param weight weight of the edge.
     */
    public void addWeightedPath(int source, int destination, int weight) {
        Pair<Integer, Integer> pair = new Pair<>(destination, weight);
        if (weightedGraph.get(source) == null) {
            ArrayList<Pair<Integer, Integer>> newPath = new ArrayList<>();
            newPath.add(pair);
            weightedGraph.add(source, newPath);
        } else {
            weightedGraph.get(source).add(pair);
        }
    }

    /**
     * This method checks if a path exist between source and destination in an unweighted graph.
     *
     * @param source source
     * @param destination destination.
     * @return true/false representing if path exist between source and destination or not.
     */
    public boolean isPathInUnweightedGraph(int source, int destination) {
        if (source == destination) {
            return true;
        }
        boolean[] visited = new boolean[unweightedGraph.size()];
        Queue<Integer> queue = new LinkedList<>();
        queue.add(source);
        visited[source] = true;

        while (!queue.isEmpty()) {
            int nextNode = queue.poll();
            ArrayList<Integer> nodeList = unweightedGraph.get(nextNode);
            for (int val : nodeList) {
                if (val == destination) {
                    return true;
                } else {
                    if (!visited[val]) {
                        visited[val] = true;
                        queue.add(val);
                    }
                }
            }
        }
        return false;
    }

    public boolean dfs(int source, int destination) {
        boolean[] v = new boolean[size];
        return dfs(source, destination, v);
    }

    public boolean dfs(int source, int destination, boolean[] v) {
        if (source == destination) {
            return true;
        }
        v[source] = true;
        for (int neighbour : unweightedGraph.get(source)) {
            if (neighbour == destination) {
                return true;
            }
            if (!v[neighbour]) {
                v[neighbour] = true;
                if (dfs(neighbour, destination, v)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This method checks if a path exist between source and destination in a weighted graph.
     *
     * @param source source.
     * @param destination destination.
     * @return true/false representing if path exist or not.
     */
    public boolean isPathInWeightedGraph(int source, int destination) {
        if (source == destination) {
            return true;
        }
        boolean[] visited = new boolean[unweightedGraph.size()];
        Queue<Integer> queue = new LinkedList<>();
        queue.add(source);
        visited[source] = true;

        while (!queue.isEmpty()) {
            int nextNode = queue.poll();
            ArrayList<Integer> nodeList = unweightedGraph.get(nextNode);
            for (int val : nodeList) {
                if (val == destination) {
                    return true;
                } else {
                    if (!visited[val]) {
                        visited[val] = true;
                        queue.add(val);
                    }
                }
            }
        }
        return false;
    }

    public ArrayList<ArrayList<Integer>> connectedComponentsInUndirectedGraph() {
        boolean[] visited = new boolean[unweightedGraph.size()];
        ArrayList<ArrayList<Integer>> connectedComponentsList = new ArrayList<>();

        for (int i = 1; i < size; i++) {
            if (!visited[i]) {
                Stack<Integer> s = new Stack<>();
                s.push(i);

                ArrayList<Integer> connectedComponent = new ArrayList<>();
                while (!s.isEmpty()) {
                    int node = s.pop();
                    if (!visited[node]) {
                        visited[node] = true;
                        connectedComponent.add(node);
                        for (int neighbour : unweightedGraph.get(node)) {
                            s.push(neighbour);
                        }
                    }
                }
                connectedComponentsList.add(connectedComponent);
            }

        }

        return connectedComponentsList;
    }

    public int longestPathInGraphBFS(Integer source, Integer destination) {
        Pair<Integer, Integer> p = bfs(source);
        int farthestNode = p.getKey();
        p = bfs(farthestNode);
        return p.getValue();
    }

    public int longestPathInGraph() {

        return longestPathInGraphBFS(1, null);
        /*int longestPath = 0;
        for (int i = 1; i < size; i++) {
            longestPath = Math.max(longestPath, longestPathFromNode(i));
        }
        return longestPath;*/
    }

    public int longestPathFromNode(int node) {
        boolean[] visited = new boolean[size];
        int longestPath = 0;
        int path = 0;
        Stack<Integer> s = new Stack<>();
        s.push(node);
        visited[node] = true;
        while (!s.isEmpty()) {
            int next = s.peek();
            ArrayList<Integer> neighbours = unweightedGraph.get(next);
            boolean allNeighbourVisited = true;
            for (int neighbour : neighbours) {
                if (!visited[neighbour]) {
                    allNeighbourVisited = false;
                    visited[neighbour] = true;
                    s.push(neighbour);
                    path++;
                    break;
                }
            }
            if (allNeighbourVisited) {
                longestPath = Math.max(longestPath, path);
                s.pop();
                path--;
            }
        }
        return longestPath;
    }

}
