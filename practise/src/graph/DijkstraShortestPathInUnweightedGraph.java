package graph;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import javafx.util.Pair;

public class DijkstraShortestPathInUnweightedGraph {

    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int t = Integer.parseInt(br.readLine());
        while (t > 0) {
            String[] line1 = br.readLine().split(" ");
            int n = Integer.parseInt(line1[0]);
            int m = Integer.parseInt(line1[1]);

            List<List<Integer>> edges = new ArrayList<>();

            int count = 0;
            while (count < m) {
                String[] input = br.readLine().split(" ");
                List<Integer> list = new ArrayList<>();
                for (String val : input) {
                    list.add(Integer.parseInt(val));
                }
                edges.add(list);
                count++;
            }

            int s = Integer.parseInt(br.readLine());
            List<Integer> result = shortestReach(n, edges, s);
            for (int val : result) {
                bw.write(val + " ");
            }
            bw.newLine();
            t--;
        }

        br.close();
        bw.close();
    }

    public static List<Integer> shortestReach(int n, List<List<Integer>> edges, int s) {

        ArrayList<ArrayList<Pair<Integer, Integer>>> g = createGraph(n, edges);
        Queue<Integer> q = new LinkedList<>();
        int[] d = new int[n + 1];
        for (int i = 0; i <= n; i++) {
            d[i] = -1;
        }
        q.add(s);
        d[s] = 0;
        while (!q.isEmpty()) {
            int u = q.poll();
            ArrayList<Pair<Integer, Integer>> neighbours = g.get(u);
            for (Pair<Integer, Integer> p : neighbours) {
                int v = p.getKey();
                int w = p.getValue();

                if (d[v] == -1 || (d[u] + w) < d[v]) {
                    d[v] = d[u] + w;
                    q.add(v);
                }
            }
        }

        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            if (i != s) {
                result.add(d[i]);
            }
        }

        return result;
    }

    public static ArrayList<ArrayList<Pair<Integer, Integer>>> createGraph(int n,
        List<List<Integer>> edges) {
        ArrayList<ArrayList<Pair<Integer, Integer>>> graph = new ArrayList<>();
        for (int i = 0; i <= n; i++) {
            graph.add(new ArrayList<>());
        }
        for (List<Integer> edge : edges) {
            int u = edge.get(0);
            int v = edge.get(1);
            int w = edge.get(2);

            if (graph.get(u) == null) {
                ArrayList<Pair<Integer, Integer>> list = new ArrayList<>();
                list.add(new Pair<>(v, w));
                graph.add(u, list);
            } else {
                graph.get(u).add(new Pair<>(v, w));
            }

            if (graph.get(v) == null) {
                ArrayList<Pair<Integer, Integer>> list = new ArrayList<>();
                list.add(new Pair<>(u, w));
                graph.add(v, list);
            } else {
                graph.get(v).add(new Pair<>(u, w));
            }
        }

        return graph;
    }

}
