package graph;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import javafx.util.Pair;

public class WaterFLow {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0){
            String[] line1 = br.readLine().trim().split(" ");
            int n = Integer.parseInt(line1[0]);
            int m = Integer.parseInt(line1[1]);

            int[][] heights = new int[n][m];
            int i = 0;
            while (i < n){
                String[] line2 = br.readLine().trim().split(" ");
                for(int j = 0 ; j < m ; j++){
                    heights[i][j] = Integer.parseInt(line2[j]);
                }
                i++;
            }

            List<List<Integer>> result = pacificAtlantic(heights);
            for(List<Integer> list : result){
                for(int val : list){
                    bw.write(val+" ");
                }
                bw.newLine();
            }
            T--;
        }

        br.close();
        bw.close();
    }

    public static List<List<Integer>> pacificAtlantic(int[][] heights) {
        int n = heights.length;
        int m = heights[0].length;
        int[][] blueSea = new int[n][m];
        int[][] redSea = new int[n][m];

        for (int i = 0 ; i < n ; i++){
            for (int j = 0 ; j < m ; j++){
                blueSea[i][j] = heights[i][j];
                redSea[i][j] = heights[i][j];
            }
        }

        Queue<Pair<Integer, Integer>> q = new LinkedList<>();
        for(int j = 0 ; j < m ; j++){
            q.add(new Pair<>(0, j));
        }

        for(int i = 0 ; i < n ; i++){
            q.add(new Pair<>(i, 0));
        }

        bfs(blueSea,heights, q, n, m);

        for(int j = 0 ; j < m ; j++){
            q.add(new Pair<>(n-1, j));
        }

        for(int i = 0 ; i < n ; i++){
            q.add(new Pair<>(i, m-1));
        }

        bfs(redSea, heights, q, n, m);

        List<List<Integer>> result = new ArrayList<>();
        for(int i = 0 ; i < n ; i++){
            for(int j = 0 ; j < m ; j++){
                if(blueSea[i][j] == 0 && redSea[i][j] == 0){
                    List<Integer> list = new ArrayList<>();
                    list.add(i);
                    list.add(j);
                    result.add(list);
                }
            }
        }

        return result;
    }

    public static void bfs(int[][] water, int[][] heights, Queue<Pair<Integer, Integer>> q, int n, int m){

        while (!q.isEmpty()){
            Pair<Integer, Integer> p = q.poll();
            int i = p.getKey();
            int j = p.getValue();
            water[i][j] = 0;
            int[] di = new int[] {1, -1, 0, 0};
            int[] dj = new int[] {0, 0, 1, -1};
            for(int k = 0 ; k < 4 ; k++){
                int x = i + di[k];
                int y = j + dj[k];
                if(x < 0 || x >= n || y < 0 || y >= m || (water[x][y] == 0)){
                    continue;
                }

                if(water[x][y] >= heights[i][j]){
                    q.add(new Pair<>(x, y));
                }
            }
        }

    }

}
