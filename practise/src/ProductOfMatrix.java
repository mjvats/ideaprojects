import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ProductOfMatrix {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.valueOf(br.readLine());
        while(T > 0){
            String[] dimension = br.readLine().trim().split(" ");
            int[][] A = new int[Integer.valueOf(dimension[0])][Integer.valueOf(dimension[1])];
            int rowNumA = Integer.valueOf(dimension[0]);
            int rowCount = 0;
            while(rowCount < rowNumA){
                String[] row = br.readLine().trim().split(" ");
                for(int i = 0; i < row.length ; i++){
                    A[rowCount][i] = Integer.valueOf(row[i]);
                }
                rowCount++;
            }

            dimension = br.readLine().trim().split(" ");
            int[][] B = new int[Integer.valueOf(dimension[0])][Integer.valueOf(dimension[1])];
            int rowNumB = Integer.valueOf(dimension[0]);
            rowCount = 0;
            while(rowCount < rowNumB){
                String[] row = br.readLine().trim().split(" ");
                for(int i = 0; i < row.length ; i++){
                    B[rowCount][i] = Integer.valueOf(row[i]);
                }
                rowCount++;
            }

            int[][] C = product(A,B);
            for(int i = 0 ; i < C.length ; i++){
                for(int j = 0 ; j < C[0].length ; j++){
                    bw.write(C[i][j]+" ");
                }
                bw.write("\n");
            }
            T--;
        }

        br.close();
        bw.close();
    }

    private static int[][] product(int[][] A, int[][] B){
        int[][] C = new int[A.length][B[0].length];

        for(int x = 0 ; x < C.length ; x++){
            for(int y = 0 ; y < C[0].length ; y++){
                int i = x, j = 0, m = 0, n = y, sum = 0;
                while(j < A[0].length && m < B.length){
                    sum += A[i][j]*B[m][n];
                    j++;m++;
                }
                C[x][y] = sum;
            }
        }

        return C;
    }

}
