import java.util.ArrayList;

public class SmallerOrEqual {

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        System.out.println(solve(list, 13));
    }

    public static int solve(ArrayList<Integer> A, int B) {

        int count = 0;
        int start = 0;
        int end = A.size()-1;
        while(start <= end)
        {
            int mid = (start+end)/2;
            if(A.get(mid) == (B))
            {
                if(mid == A.size()-1 || A.get(mid+1) != (B))
                {
                    count = mid+1;
                    return count;
                }
                else
                {
                    start = mid+1;
                }
            }
            else if(A.get(mid) < B)
            {
                if(mid == A.size()-1 || A.get(mid+1) > B){
                    count = mid+1;
                    return count;
                }
                start = mid+1;
            }
            else{
                end = mid-1;
            }
        }
        return count;
    }

}
