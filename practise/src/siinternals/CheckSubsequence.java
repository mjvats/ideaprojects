package siinternals;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class CheckSubsequence {

    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            String[] input = br.readLine().trim().split(" ");
            String A = input[0];
            String B = input[1];
            String result = sol(A, B);
            bw.write(result);
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static String sol(String A, String B)
    {
        int n = A.length();
        int m = B.length();

        int matchCount = 0;
        for(int i = 0, j = 0 ; i < n && j < m ;)
        {
            if(A.charAt(i) == B.charAt(j))
            {
                i++;
                j++;
                matchCount++;
            }
            else j++;
        }

        if(matchCount == n) return "Yes";
        else return "No";
    }

}
