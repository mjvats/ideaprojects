package siinternals;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class RangeOfPrimes {

    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            String[] line1 = br.readLine().trim().split(" ");
            int A = Integer.parseInt(line1[0]);
            int B = Integer.parseInt(line1[1]);
            int result = sol(A, B);
            bw.write(result+"");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }


    public static int sol(int A, int B)
    {
        boolean[] temp = new boolean[B+1];
        int countA = 0;
        int countB = 0;
        boolean isPrime = false;
        for(int i = 2 ; i <= B ; i++)
        {
            if(!temp[i]) countB++;
            if(i == A)
            {
                countA = countB;
                if(!temp[i]) isPrime = true;
            }
            for(int j = i ; j <= B ; j = j+i) temp[j] = true;
        }
        if (countA == 0) return countB;
        else if (!isPrime) return countB-countA;
        else return (countB - countA) +1;
    }

}
