package siinternals;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class RangeSumSubarrays {

    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            String[] line1 = br.readLine().trim().split(" ");
            int n = Integer.parseInt(line1[0]);
            long A = Long.parseLong(line1[1]);
            long B = Long.parseLong(line1[2]);

            long arr[] = new long[n];
            String[] input = br.readLine().trim().split(" ");

            for(int i = 0 ; i < n ; i++) arr[i] = Long.parseLong(input[i]);
            int result = sol(arr, A, B);
            bw.write(result+"");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }


    public static int sol(long[] arr, long A, long B)
    {
        int n = arr.length;
        long[] temp = new long[n];
        temp[0] = arr[0];
        for(int i = 1; i < n ; i++) temp[i] = temp[i-1]+arr[i];

        int count = 0;
        for(int i = 0 ; i < n ; i++)
        {
            if(temp[i] >= A && temp[i] <= B) count++;
            for(int j = 0 ; j < i ; j++)
            {
                long val = temp[i]-temp[j];
                if(val >= A && val <= B) count++;
            }
        }
        return count;
    }

}
