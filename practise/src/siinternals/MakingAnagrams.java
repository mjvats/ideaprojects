package siinternals;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

public class MakingAnagrams {

    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            String[] input = br.readLine().trim().split(" ");
            String A = input[0];
            String B = input[1];
            int result = sol(A, B);
            bw.write(result+"");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int sol(String A, String B)
    {
        int m = A.length();
        int n = B.length();

        Map<Character, Integer> mapA = new HashMap<>();
        for(int i = 0 ; i < m ; i++)
        {
            char c = A.charAt(i);
            if(!mapA.containsKey(c)) mapA.put(c, 1);
            else mapA.put(c, mapA.get(c)+1);
        }

        Map<Character, Integer> mapB = new HashMap<>();
        for(int i = 0 ; i < n ; i++)
        {
            char c = B.charAt(i);
            if(!mapB.containsKey(c)) mapB.put(c, 1);
            else mapB.put(c, mapB.get(c)+1);
        }

        if(mapA.size() < mapB.size()) return getCount(mapA, mapB);
        else return getCount(mapB, mapA);
    }

    public static int getCount(Map<Character, Integer> mapA, Map<Character, Integer> mapB)
    {
        int count = 0;
        for(Map.Entry<Character, Integer> entry : mapA.entrySet())
        {
            char c = entry.getKey();
            if(!mapB.containsKey(c)) count += mapA.get(c);
            else mapB.replace(c, Math.abs(mapB.get(c)-mapA.get(c)));

        }
        for(Map.Entry<Character, Integer> entry : mapB.entrySet())  count += entry.getValue();
        return count;
    }

}
