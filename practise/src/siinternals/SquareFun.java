package siinternals;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class SquareFun {

    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            String[] line1 = br.readLine().trim().split(" ");
            long A = Long.parseLong(line1[0]);
            long B = Long.parseLong(line1[1]);
            long result = sol(A, B);
            bw.write(result+"");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static long count(long A, long B)
    {
        long countA = 0L;
        long countB = 0L;
        long diff = 0L;
        boolean APresent = false;
        boolean flag = false;
        long i = 1L;
        for(; i <= B ; i=i+diff+1)
        {
            countB = countB + 1L;
            if(i == A)
            {
                APresent = true;
                countA = countB;
            }
            else if(i > A && !APresent)
            {
                APresent = true;
                countA = countB-1L;
            }
            diff = diff + 2L;
        }

        if(!APresent && countA == 0L) return 0;
        else if(APresent) return (countB-countA+1);
        else return countB-countA;
    }

    public static long sol(long A, long B)
    {
        double a = Math.sqrt(A);
        double b = Math.sqrt(B);

        return (long) (Math.floor(b) - Math.ceil(a)+1L);
    }

}
