import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;

public class FindingCubeRoot {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            long k = Long.parseLong(br.readLine());
            long result = sol(k);
            bw.write(result + "");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static long sol(long k)
    {
        long a = Math.abs(k);
        int sqrt = (int) Math.sqrt(a);
        long start = 0L;
        long end = sqrt;

        while (start <= end){
            long mid = (start+end)/2;
            BigInteger x = new BigInteger(mid+"");
            BigInteger y = new BigInteger(a+"");
            BigInteger prod = x.multiply(x).multiply(x);
            int comp = prod.compareTo(y);
            if(comp == 0)
                return k >= 0 ? mid : -mid;
            else if( comp == -1)
                start = mid+1;
            else
                end = mid-1;
        }
        return -1;
    }

}
