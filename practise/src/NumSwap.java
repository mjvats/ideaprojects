public class NumSwap {

    public static void main(String args[]){
        int[] input = {1,1};
        new NumSwap().reverseUsingXor(input, 0,1);
        for(int i = 0 ; i < input.length ; i++){
            System.out.println(input[i]);
        }
    }

    private int[] reverseUsingXor(int[] nums, int i, int j){
        nums[i] = nums[i] ^ nums[j];
        nums[j] = nums[i] ^ nums[j];
        nums[i] = nums[i] ^ nums[j];
        return nums;
    }

    private int[] reverseUsingTemp(int[] nums, int i, int j){
        int temp = nums[j];
        nums[j] = nums[i];
        nums[i] = temp;
        return nums;
    }
}
