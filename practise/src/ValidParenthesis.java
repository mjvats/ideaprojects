public class ValidParenthesis {

    public static void main(String[] args) {
        int n = 4;
        char[] arr = new char[n];
        int i = 0;
        func(arr, i, n, 0, 0);
    }

    public static void func(char[] arr, int i, int n, int oc, int cc){
        if(i == arr.length){
            for(char ch : arr){
                System.out.print(ch+" ");
            }
            System.out.println();
            return;
        }

        if(oc < n/2){
            arr[i] = '(';
            func(arr, i+1, n, oc+1, cc);
        }

        if(cc < oc){
            arr[i] = ')';
            func(arr, i+1, n, oc, cc+1);
        }
    }
}
