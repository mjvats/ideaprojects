import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ReverseBits {

    public static void main(String args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.valueOf(br.readLine());

        while (T > 0) {

            int N = Integer.valueOf(br.readLine());
            bw.write(String.valueOf(reverseBits(N)));
            bw.write("\n");
            T--;

        }

        br.close();
        bw.close();
    }

    static String reverseBits(int N) {
        int result = 0;
        for (int i = 0; i < 32 ; i++) {
            if (((N >> i) & 1) == 1) {
                result = result | (1 << (31-i));
            }
        }
        return Integer.toUnsignedString(result);
    }

    public static void main(String[] args) {
        int N = 10;
        for(int i = 0 ; i < 32 ; i = i+2){
            if((((N >> i)&1)==1)^(((N >> (i+1))&1)==1)){
                N = N ^ (1 << i);
                N = N ^ (1 << i+1);
            }
        }
        System.out.println(Integer.toUnsignedString(N));
    }

}
