import java.util.Arrays;
import java.util.List;

public class PickingNumbers {

    public static void main(String[] args) {
        Integer[] A = {4, 2, 3, 4, 4, 9, 98, 98, 3, 3, 3, 4, 2, 98, 1, 98, 98, 1, 1, 4, 98, 2, 98,
            3, 9, 9, 3, 1, 4, 1, 98, 9, 9, 2, 9, 4, 2, 2, 9, 98, 4, 98, 1, 3, 4, 9, 1, 98, 98,
            4, 2, 3, 98, 98, 1, 99, 9, 98, 98, 3, 98, 98, 4, 98, 2, 98, 4, 2, 1, 1, 9, 2, 4,};
        List<Integer> list = Arrays.asList(A);
        System.out.println(pickingNumbers(list));
    }

    public static int pickingNumbers(List<Integer> a) {

        int[] countarr = new int[101];
        for(int i = 0 ; i < a.size() ; i++)
        {
            countarr[a.get(i)]++;
        }

        int max = 0;
        for(int i = 0 ; i < 100 ; i++)
        {
            if(countarr[i] + countarr[i+1] > max)
            {
                max = countarr[i]+countarr[i+1];
            }
        }
        return max;
    }

}
