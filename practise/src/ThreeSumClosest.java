import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ThreeSumClosest {

    public static void main(String[] args) {
        Integer[] A = {2147483647, -2147483648, -2147483648, 0, 1};
        List<Integer> inpList = Arrays.asList(A);
        int result = threeSumClosest(inpList, 0);
        System.out.println(result);
    }

    public static int threeSumClosest(List<Integer> A, int B) {

        Collections.sort(A);
        ArrayList<Long> tempList = new ArrayList<>();
        for(int i = 0 ; i < A.size()-2 ; i++)
        {
            long firstNum = (long)A.get(i);
            long diff = (long)B - firstNum;
            for(int j = i+1, k = A.size()-1 ; j < k ;)
            {
                long sum = (long)(A.get(j))+(long)(A.get(k));
                if(sum == diff)
                {
                    tempList.add(firstNum+sum);
                    return B;
                }
                else if(sum < diff)
                {
                    tempList.add(firstNum+sum);
                    j++;
                }
                else
                {
                    tempList.add(firstNum+sum);
                    k--;
                }
            }
        }

        long min = Math.abs(B-tempList.get(0));
        int minIndex = 0;
        for(int i = 1; i < tempList.size(); i++){
            long diff = Math.abs(B - tempList.get(i));
            if(diff < min){
                min = diff;
                minIndex = i;
            }
        }
        return Integer.valueOf(String.valueOf(tempList.get(minIndex)));
    }

}
