import java.util.Arrays;
import java.util.List;

public class SortedInsertPosition {

    public static void main(String[] args) {
        Integer[] A = {1};
        List<Integer> a = Arrays.asList(A);
        int b = 2;
        int result = searchInsert(a, b);
        System.out.println(result);

    }

    public static int searchInsert(List<Integer> a, int b) {

        int index = binarySearch(a, b);
        if (index > -1) {
            return index;
        } else {
            return binarySearchForRange(a, b);
        }
    }

    public static int binarySearch(List<Integer> a, int b) {
        int start = 0, end = a.size() - 1, mid = 0;

        while (start <= end) {
            mid = (start + end) / 2;
            if (a.get(mid) == b) {
                return mid;
            } else if (a.get(mid) < b) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return -1;
    }

    public static int binarySearchForRange(List<Integer> a, int b) {
        int start = 0, end = a.size() - 1, mid = 0;
        while (start <= end) {
            mid = (start + end) / 2;
            if (mid == a.size() - 1 && a.get(mid) < b) {
                return mid + 1;
            } else if (mid == 0 || (a.get(mid) > b && a.get(mid - 1) < b)) {
                return mid;
            } else if (a.get(mid) > b && a.get(mid - 1) > b) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        return -1;
    }
}
