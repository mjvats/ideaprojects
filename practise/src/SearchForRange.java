import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchForRange {

    public static void main(String[] args) {
        Integer[] A = {5, 7, 7, 8, 8, 10};
        List<Integer> list = Arrays.asList(A);
        ArrayList<Integer> result = searchRange(list, 6);
        for(int val : result)
        {
            System.out.println(val);
        }
    }

    public static ArrayList<Integer> searchRange(final List<Integer> A, int B) {
        ArrayList<Integer> result = new ArrayList<>();
        int start = 0, end = A.size()-1;
        int startIndex = -1, endIndex = -1;
        while(start <= end)
        {
            int mid = (start + end)/2;
            if(A.get(mid) == B)
            {
                if(mid == 0 || A.get(mid-1) != B)
                {
                    startIndex = mid;
                    break;
                }
                end = mid-1;
            }
            else if(A.get(mid) < B)
            {
                start = mid+1;
            }
            else
            {
                end = mid-1;
            }
        }

        start = 0;
        end = A.size()-1;
        while(start <= end)
        {
            int mid = (start + end)/2;
            if(A.get(mid) == B)
            {
                if(mid == A.size()-1 || A.get(mid+1) != B)
                {
                    endIndex = mid;
                    break;
                }
                start = mid+1;
            }
            else if(A.get(mid) < B)
            {
                start = mid+1;
            }
            else
            {
                end = mid-1;
            }
        }
        result.add(startIndex);
        result.add(endIndex);
        return result;
    }

}
