import java.util.Scanner;

public class ScannerDemo {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int size = Integer.valueOf(sc.nextLine());

        String[] inputLine = sc.nextLine().trim().split(" ");
        int[] arr = new int[size];

        for(int i = 0 ; i < size ; i++){
            arr[i] = Integer.valueOf(inputLine[i]);
            System.out.println(arr[i]);
        }
    }
}
