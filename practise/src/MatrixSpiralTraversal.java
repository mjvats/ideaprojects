public class MatrixSpiralTraversal {

    public static void main(String[] args) {
        int[][] a = {{1},{2},{3}};
        spiralMatrix(a);
    }

    private static void spiralMatrix(int[][] a){
        if(a.length == 0)
            return;

        int k = 0, l = a.length-1, m = 0, n = a[0].length-1;
        while(k <= l && m <= n){
            for(int i = m ; i <= n ; i++){
                System.out.print(a[k][i] + " ");
            }
            k++;

            for(int i = k ; i <= l ; i++){
                System.out.print(a[i][n] + " ");
            }
            n--;

            if(k <= l){
                for(int i = n ; i >= m ; i--){
                    System.out.print(a[l][i] + " ");
                }
                l--;
            }

            if(n >= m){
                for(int i = l ; i >= k ; i--){
                    System.out.print(a[i][m] + " ");
                }
                m++;
            }
        }
    }
}
