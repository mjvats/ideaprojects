package tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import javafx.util.Pair;

public class BinarySearchTree {

    Map<Integer, List<Node>> map;
    int idx = 0;
    int count = 0;
    int nodesAtKDist = 0;
    int depth;
    int height = -1;
    Map<Integer, Integer> lcamap = new HashMap<>();
    private Node root;
    private int diameter = 0;
    Stack<Node> s = new Stack<>();
    Queue<Node> q = new LinkedList<>();
    ArrayList<Integer> heights = new ArrayList<>();

    BinarySearchTree() {
        this.setRoot(null);
        map = new TreeMap<>();
    }

    private static long getPower(int a, int b) {
        int bitSize = Integer.toBinaryString(b).length();
        long ans = 1, x = 1;
        int k = 64735492;
        for (int i = 0; i < bitSize; i++) {
            x = i == 0 ? a : (x * x) % k;
            if (((b >> i) & 1) == 1) {
                ans = (ans * x) % k;
            }
        }
        return ans;
    }

    public static ArrayList<ArrayList<Integer>> levelOrder(Node root) {
        Stack<Node> s1 = new Stack<>();
        Stack<Node> s2 = new Stack<>();
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();

        s2.push(root);
        while (true) {
            ArrayList<Integer> temp = new ArrayList<Integer>();
            while (!s2.isEmpty()) {
                root = s2.pop();
                //System.out.print(root.data+" ");
                temp.add(root.data);
                if (root.left != null) {
                    s1.push(root.left);
                }
                if (root.right != null) {
                    s1.push(root.right);
                }
            }
            //System.out.println();
            result.add(temp);
            while (!s1.isEmpty()) {
                s2.push(s1.pop());
            }
            if (s1.isEmpty() && s2.isEmpty()) {
                return result;
            }
        }
    }

    public Node getRoot() {
        return this.root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    /*public Node add(int data) {
        if (this.root == null) {
            this.root = new Node(data);
            return this.root;
        }
        return add(this.root, data);
    }

    private Node add(Node root, int data) {
        if (root == null) {
            root = new Node(data);
        } else {
            if (data < root.data) {
                root.left = add(root.left, data);
            } else {
                root.right = add(root.right, data);
            }
        }
        return root;
    }*/

    public Node add(int data) {
        if (this.root == null) {
            height++;
            this.root = new Node(data, height);
            heights.add(height);
            return this.root;
        }
        return add(this.root, data, this.root.height);
    }

    private Node add(Node root, int data, int height) {
        if (root == null) {
            height++;
            root = new Node(data, height);
            heights.add(height);
        } else {
            if (data < root.data) {
                root.left = add(root.left, data, root.height);
            } else {
                root.right = add(root.right, data, root.height);
            }
        }
        return root;
    }

    public void preOrder(Node root) {
        if (root == null) {
            return;
        }
        System.out.print(root.data + " ");
        preOrder(root.left);
        preOrder(root.right);
    }

    public void postOrder(Node root) {
        if (root == null) {
            return;
        }
        postOrder(root.left);
        postOrder(root.right);
        System.out.print(root.data + " ");
    }

    public void inOrder(Node root) {
        if (root == null) {
            return;
        }
        inOrder(root.left);
        System.out.print(root.data + " ");
        inOrder(root.right);
    }

    public int depth(int val) {
        if (root == null) {
            return -1;
        }
        int depth = depth(root, val, 0);
        return depth;
    }

    public int depth(Node root, int val, int depth) {
        if (root == null) {
            return -1;
        }
        if (root.data == val) {
            return depth;
        }

        int d = depth(root.left, val, depth + 1);
        if (d != -1) {
            return d;
        }
        return depth(root.right, val, depth + 1);
    }

    public int depth() {
        if (root == null) {
            return -1;
        }
        depth(root, 0);
        return depth;
    }

    public void depth(Node root, int d) {
        if (root == null) {
            return;
        }
        depth = Math.max(depth, d);
        depth(root.left,d + 1);
        depth(root.right,d + 1);
    }

    public void zigzagTraversal() {
        if (this.root != null) {
            zigzagTraversal(this.root);
        }
    }

    private void zigzagTraversal(Node root) {
        if (root == null) {
            return;
        }
        Queue<Node> q = new LinkedList<>();
        Stack<Node> s = new Stack<>();
        q.add(root);
        boolean flag = false;
        while (true) {
            while (!q.isEmpty()) {
                Node node = q.poll();
                System.out.print(node.data + " ");
                s.push(node);
            }
            System.out.println();
            if (s.isEmpty()) {
                return;
            }

            while (!s.isEmpty()) {
                Node node = s.pop();
                if (flag) {
                    if (node.left != null) {
                        q.add(node.left);
                    }
                    if (node.right != null) {
                        q.add(node.right);
                    }
                } else {
                    if (node.right != null) {
                        q.add(node.right);
                    }
                    if (node.left != null) {
                        q.add(node.left);
                    }
                }
            }
            flag = !flag;
        }
    }

    public void zigzagBottomUp() {
        if (this.root != null) {
            zigzagBottomUp(this.root);
        }
    }

    public void zigzagBottomUp(Node root) {
        if (root == null) {
            return;
        }
        Queue<Node> q = new LinkedList<>();
        Stack<Node> s = new Stack<>();
        Stack<Node> result = new Stack<>();
        q.add(root);
        boolean flag = false;
        while (true) {
            while (!q.isEmpty()) {
                Node node = q.poll();
                result.push(node);
                s.push(node);
            }

            if (s.isEmpty()) {
                break;
            }

            while (!s.isEmpty()) {
                Node node = s.pop();
                if (flag) {
                    if (node.left != null) {
                        q.add(node.left);
                    }
                    if (node.right != null) {
                        q.add(node.right);
                    }
                } else {
                    if (node.right != null) {
                        q.add(node.right);
                    }
                    if (node.left != null) {
                        q.add(node.left);
                    }
                }
            }
            flag = !flag;
        }

        while (!result.isEmpty()) {
            System.out.print(result.pop().data + " ");
        }
    }

    public void levelOrderBottomUp() {
        if (this.root != null) {
            levelOrderBottomUp(this.root);
        }
    }

    public void levelOrderBottomUp(Node root) {
        if (root == null) {
            return;
        }
        Queue<Node> q = new LinkedList<>();
        Stack<Node> s = new Stack<>();
        q.add(root);
        q.add(null);
        while (!q.isEmpty()) {
            Node node = q.poll();
            if (node != null) {
                if (node.right != null) {
                    q.add(node.right);
                }
                if (node.left != null) {
                    q.add(node.left);
                }
            } else {
                if (q.isEmpty()) {
                    break;
                } else {
                    q.add(null);
                }

            }
            s.push(node);
        }

        while (!s.isEmpty()) {
            Node node = s.pop();
            if (node == null) {
                System.out.println();
            } else {
                System.out.print(node.data + " ");
            }
        }
    }

    public void verticalOrder() {
        if (root == null) {
            return;
        }
        verticalOrder(this.root, 1000);
        for (Map.Entry<Integer, List<Node>> entry : map.entrySet()) {
            List<Node> list = entry.getValue();
            list.sort(Comparator.comparingInt(o -> o.data));
            for (Node node : list) {
                System.out.print(node.data + " ");
            }
            System.out.println();
        }
    }

    public void verticalOrder(Node root, int v) {
        if (root == null) {
            return;
        }
        if (map.containsKey(v)) {
            List<Node> list = map.get(v);
            list.add(root);
        } else {
            List<Node> list = new ArrayList<>();
            list.add(root);
            map.put(v, list);
        }

        verticalOrder(root.left, v - 1);
        verticalOrder(root.right, v + 1);
    }

    public void diagonalOrder() {
        if (root == null) {
            return;
        }
        diagonalOrder(this.root, 0);

        ArrayList<Integer> result = new ArrayList<>();
        for (Map.Entry<Integer, List<Node>> entry : map.entrySet()) {
            List<Node> list = entry.getValue();
            list.forEach(node -> result.add(node.data));
            for (Node node : list) {
                System.out.print(node.data + " ");
            }
            System.out.println();
        }
    }

    public void diagonalOrder(Node root, int v) {
        if (root == null) {
            return;
        }
        if (map.containsKey(v)) {
            map.get(v).add(root);
        } else {
            List<Node> list = new ArrayList<>();
            list.add(root);
            map.put(v, list);
        }

        diagonalOrder(root.left, v + 1);
        diagonalOrder(root.right, v);
    }

    public void rightView() {
        if (this.root != null) {
            rightView(this.root);
        }
    }

    public void rightView(Node root) {
        ArrayList<ArrayList<Integer>> result = levelOrder(root);
        ArrayList<Integer> rightView = new ArrayList<>();
        for (ArrayList<Integer> list : result) {
            rightView.add(list.get(list.size() - 1));
        }
    }

    public boolean isLeaf(Node root) {
        return (root == null || (root.left == null && root.right == null));
    }

    public boolean isCompleteTreeNode(Node root) {
        return (root == null || (root.left != null && root.right != null));
    }

    public boolean isCompleteBinaryTreeNode(Node root) {
        return root == null || root.left != null || root.right == null;
    }

    public boolean isFBT() {
        return isFBT(this.root);
    }

    public boolean isFBT(Node root) {
        if (root == null) {
            return true;
        }
        if (isLeaf(root) || isCompleteTreeNode(root)) {
            return isFBT(root.left) && isFBT(root.right);
        }
        return false;
    }

    public boolean isCBT() {
        return CBT(this.root);
    }

    public boolean isCBT(Node root) {
        if (root == null) {
            return true;
        }
        if (isCompleteBinaryTreeNode(root)) {
            return isCBT(root.left) && isCBT(root.right);
        }
        return false;
    }

    public boolean CBT(Node root) {
        if (root == null) {
            return true;
        }
        Queue<Node> q = new LinkedList<>();
        q.add(root);
        while (!q.isEmpty()) {
            Node node = q.poll();
            if (node == null) {
                while (!q.isEmpty()) {
                    if (q.poll() != null) {
                        return false;
                    }
                }
                return true;
            }
            if (node.left == null) {
                q.add(null);
            } else {
                q.add(node.left);
            }

            if (node.right == null) {
                q.add(null);
            } else {
                q.add(node.right);
            }
        }
        return true;
    }

    public boolean isBalancedTree() {
        if (this.root != null) {
            int h_left = isBalancedTree(root.left);
            int h_right = isBalancedTree(root.right);
            if (h_left == -1 || h_right == -1 || Math.abs(h_left - h_right) > 1) {
                return false;
            } else {
                return Math.abs(h_left - h_right) <= 1;
            }
        }
        return false;
    }

    public int isBalancedTree(Node root) {
        if (root == null) {
            return 0;
        }
        int h_left = isBalancedTree(root.left);
        int h_right = isBalancedTree(root.right);

        if (h_left == -1 || h_right == -1 || Math.abs(h_left - h_right) > 1) {
            return -1;
        } else {
            return Math.max(h_left, h_right) + 1;
        }
    }

    public void leftView() {
        if (this.root != null) {
            leftView(this.root, 0);
            for (Map.Entry<Integer, List<Node>> entry : map.entrySet()) {
                List<Node> list = entry.getValue();
                System.out.print(list.get(0).data + " ");
            }
        }
    }

    public void leftView(Node root, int level) {
        if (root == null) {
            return;
        }
        if (map.containsKey(level)) {
            map.get(level).add(root);
        } else {
            List<Node> list = new ArrayList<>();
            list.add(root);
            map.put(level, list);
        }

        leftView(root.left, level + 1);
        leftView(root.right, level + 1);
    }

    public long sumFromRootToLeaf() {
        return sumFromRootToLeaf(this.root, 0L);
    }

    public long sumFromRootToLeaf(Node root, long sum) {
        if (root == null) {
            return 0;
        }
        int digits = digiCount(root.data);
        sum = ((sum * getPower(10, digits)) % 1000000007 + root.data) % 1000000007;
        long left = sumFromRootToLeaf(root.left, sum);
        long right = sumFromRootToLeaf(root.right, sum);
        long total = (left + right) % 1000000007;
        return total == 0 ? sum : total;
    }

    public int digiCount(int n) {
        int count = 0;
        while (n / 10 > 0) {
            count++;
            n = n / 10;
        }
        return ++count;
    }

    public void preInToPost(int[] pre, int[] in) {
        preInToPost(pre, in, 0, pre.length - 1);
    }

    public void preInToPost(int[] pre, int[] in, int low, int high) {
        if (low > high) {
            return;
        }
        int position = search(pre[idx], in);
        idx++;
        preInToPost(pre, in, low, position - 1);
        preInToPost(pre, in, position + 1, high);
        System.out.print(in[position]);
    }

    public int search(int x, int[] in) {
        int idx = -1;
        while (idx < in.length) {
            if (in[idx + 1] == x) {
                break;
            }
            idx++;
        }
        return ++idx;
    }

    public boolean isBST(int[] arr) {
        int parent = 0;
        int leftChild = 1;
        int rightChild = 2;
        if (leftChild >= arr.length) {
            return true;
        } else if (rightChild >= arr.length && arr[leftChild] < arr[parent]) {
            return true;
        }
        if ((arr[leftChild] < arr[parent]) && (arr[rightChild] > arr[parent])) {
            return isLeftBST(arr, arr[parent], 1) && isRightBST(arr, arr[parent], 2);
        }
        return false;
    }

    public boolean isLeftBST(int[] arr, int root, int index) {
        int leftChild = 2 * index + 1;
        int rightChild = 2 * index + 2;
        if (leftChild >= arr.length && rightChild >= arr.length) {
            return true;
        } else if (leftChild >= arr.length && arr[rightChild] > arr[index]
            && arr[rightChild] < root) {
            return isLeftBST(arr, root, rightChild);
        } else if (rightChild >= arr.length && arr[leftChild] < arr[index]
            && arr[leftChild] < root) {
            return isLeftBST(arr, root, leftChild);
        }
        if ((arr[leftChild] < arr[index]) && (arr[rightChild] > arr[index])
            && (arr[leftChild] < root) && (arr[rightChild] < root)) {
            boolean isLeftBSt = isLeftBST(arr, root, leftChild);
            boolean isRightBST = isLeftBST(arr, root, rightChild);
            if (isLeftBSt && isRightBST) {
                count++;
            }

            return isLeftBSt && isRightBST;
        }
        return false;
    }

    public boolean isRightBST(int[] arr, int root, int index) {
        int leftChild = 2 * index + 1;
        int rightChild = 2 * index + 2;
        if (leftChild >= arr.length && rightChild >= arr.length) {
            return true;
        } else if (leftChild >= arr.length && arr[rightChild] > arr[index]
            && arr[rightChild] > root) {
            return isRightBST(arr, root, rightChild);
        } else if (rightChild >= arr.length && arr[leftChild] < arr[index]
            && arr[leftChild] > root) {
            return isRightBST(arr, root, leftChild);
        }
        if ((arr[leftChild] < arr[index]) && (arr[rightChild] > arr[index])
            && (arr[leftChild] > root) && (arr[rightChild] > root)) {
            boolean isLeftBSt = isRightBST(arr, root, leftChild);
            boolean isRightBST = isRightBST(arr, root, rightChild);
            if (isLeftBSt && isRightBST) {
                count++;
            }

            return isLeftBSt && isRightBST;
        }
        return false;
    }

    /**
     * The idea behind this method is that in a BST, all nodes to the left should be in range
     * [-Infinity, root.data) and all nodes to the right should be in range (root.data, Infinity].
     *
     * @param arr Input array
     * @param parent parent index
     * @param a left limit of range
     * @param b right limit of range
     * @return true/false saying if tree is a BST
     */
    boolean isBST(int[] arr, int parent, int a, int b) {
        if (arr[parent] < a || arr[parent] > b) {
            return false;
        }

        int leftChild = 2 * parent + 1;
        int rightChild = 2 * parent + 2;

        if (leftChild >= arr.length && rightChild >= arr.length) {
            return true;
        } else if (leftChild >= arr.length && (arr[rightChild] > arr[parent]
            && arr[rightChild] <= b)) {
            return isBST(arr, rightChild, arr[parent], b);
        } else if (rightChild >= arr.length && (arr[leftChild] >= a
            && arr[leftChild] < arr[parent])) {
            return isBST(arr, leftChild, a, arr[parent]);
        }

        if ((arr[leftChild] >= a && arr[leftChild] < arr[parent]) && (arr[rightChild]) > arr[parent]
            && arr[rightChild] <= b) {
            return isBST(arr, leftChild, a, arr[parent]) && isBST(arr, rightChild, arr[parent], b);
        }
        return false;
    }

    public int numberOfBSTs(int[] arr) {
        int n = arr.length;
        for (int i = n - 1; i >= 0; i--) {
            boolean isBST = isBST(arr, i, Integer.MIN_VALUE, Integer.MAX_VALUE);
            if (isBST) {
                count++;
            }
        }
        return count;
    }

    public int lca(int u, int v) {
        Node p = new Node(u);
        Node q = new Node(v);
        return lowestCommonAncestorLeetCode(this.root, p, q).data;
    }

    public Node lowestCommonAncestor(Node root, Node p, Node q) {
        int u = p.data;
        int v = q.data;
        while (root != null) {
            int x = root.data;
            if (u >= x && v >= x) {
                if (u == x || v == x) {
                    break;
                } else {
                    root = root.right;
                }
            } else if (u <= x && v <= x) {
                if (u == x || v == x) {
                    break;
                } else {
                    root = root.left;
                }
            } else {
                break;
            }
        }
        return root;
    }

    public int diameter() {
        diameter(this.root);
        return this.diameter;
    }

    public int diameter(Node root) {
        if (root == null) {
            return 0;
        }
        int left = diameter(root.left);
        int right = diameter(root.right);

        int temp;
        temp = left + right + 1;
        diameter = Math.max(diameter, temp);
        return Math.max(left, right) + 1;
    }

    public int nodesAtKDistance(Node s, int k) {
        if (s == null) {
            return 0;
        }
        List<Node> list = path(this.root, s);
        int n = list.size();
        Node prev = null;
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                prev = list.get(n - i - 1);
                nodesAtKDistanceFromRoot(prev, k);
            } else {
                Node curr = list.get(n - i - 1);
                if ((k - i) <= 0) {
                    nodesAtKDist++;
                    break;
                } else {
                    if (prev == curr.left) {
                        nodesAtKDistanceFromRoot(curr.right, k - i - 1);
                    } else {
                        nodesAtKDistanceFromRoot(curr.left, k - i - 1);
                    }
                }
                prev = curr;
            }
        }
        return nodesAtKDist;
    }

    void nodesAtKDistanceFromRoot(Node root, int k) {
        if (root == null) {
            return;
        }
        if (k == 0) {
            ++nodesAtKDist;
        }
        nodesAtKDistanceFromRoot(root.left, k - 1);
        nodesAtKDistanceFromRoot(root.right, k - 1);
    }

    public List<Node> path(Node root, Node s) {
        if (root == null) {
            return null;
        }
        List<Node> list = new ArrayList<>();
        while (root != null) {
            list.add(root);
            if (s.data < root.data) {
                root = root.left;
            } else if (s.data > root.data) {
                root = root.right;
            } else {
                break;
            }
        }
        return list;
    }

    public Node lowestCommonAncestorLeetCode(Node root, Node p, Node q) {
        if (root == null) {
            return null;
        }
        findParent(root);
        Set<Integer> set_p = new HashSet<>();
        int u = p.data;
        while (lcamap.containsKey(u)) {
            int val = lcamap.get(u);
            set_p.add(val);
            if (val == q.data) {
                return q;
            } else {
                u = val;
            }
        }

        Set<Integer> set_q = new HashSet<>();
        int v = q.data;
        while (lcamap.containsKey(v)) {
            int val = lcamap.get(v);
            set_q.add(val);
            if (val == p.data) {
                return p;
            } else {
                v = val;
            }
        }

        for (Integer val : set_p) {
            if (set_q.contains(val)) {
                return new Node(val);
            }
        }
        return null;
    }

    public void findParent(Node root) {
        if (root == null) {
            return;
        }
        if (root.left != null) {
            lcamap.put(root.left.data, root.data);
        }
        if (root.right != null) {
            lcamap.put(root.right.data, root.data);
        }

        findParent(root.left);
        findParent(root.right);
    }

    /**
     * This method checks if the BST is valid or not.
     *
     * @param root of BST.
     * @return true or false representing if BST is valid or not.
     */
    public boolean isValidBST(Node root) {
        if (root == null) {
            return true;
        }
        boolean leftValid = true;
        boolean rightValid = true;
        if (root.left != null) {
            if (root.left.data < root.data) {
                leftValid = isValidBST(root.left);
            } else {
                leftValid = false;
            }
        }

        if (root.right != null) {
            if (root.right.data > root.data) {
                rightValid = isValidBST(root.right);
            } else {
                rightValid = false;
            }
        }
        return leftValid && rightValid;
    }

    /**
     * This method will take an input array which represents the preOrder traversal of a tree and
     * will extract the exact structure of the original BST.
     *
     * @param arr array which represents the preOrder traversal of a tree .
     * @return root node of the original BST.
     */
    public int preOrderToBST(int[] arr) {
        Node root = new Node(arr[0]);
        preOrderToBST(root, arr, 0, arr.length - 1);
        return isValidBST(root) ? 1 : 0;
    }

    /**
     * This is will extract the original structure of BST from input array with help of the root
     * node.
     *
     * @param root root node of the original BST.
     * @param arr Array representing the preorder traversal.
     * @param start start index of array
     * @param end end index of array
     */
    void preOrderToBST(Node root, int[] arr, int start, int end) {
        if (start >= end) {
            return;
        }

        int i = start + 1;
        int j = bst(arr, arr[start], i, end);
        if (arr[i] < arr[start]) {
            root.left = new Node(arr[i]);
            preOrderToBST(root.left, arr, i, j == -1 ? end : j - 1);
        }
        if (j > start) {
            root.right = new Node(arr[j]);
            preOrderToBST(root.right, arr, j, end);
        }
    }

    /**
     * This method will find the first element which is greater than val in arr between start and
     * end index
     *
     * @param arr array under consideration
     * @param val need to return the index of first element in arr >= val
     * @param start start index of the range
     * @param end end index of the range
     * @return index of the first element greater than equal to val.
     */
    int bst(int[] arr, int val, int start, int end) {
        int low = start;
        int high = end;

        while (low <= high) {
            int mid = (low + high) / 2;
            if (arr[mid] >= val) {
                if (mid > 0 && arr[mid - 1] <= val) {
                    return mid;
                } else {
                    high = mid - 1;
                }
            } else {
                low = mid + 1;
            }
        }
        return -1;
    }

    public boolean isValidPreOrder(int[] arr) {
        return isValidPreOrder(arr, 0, arr.length - 1);
    }

    public boolean isValidPreOrder(int[] arr, int start, int end) {
        if (start >= end) {
            return true;
        }
        int val = arr[start];
        int i = start + 1;
        int j = bst(arr, val, i, end);

        boolean smaller;
        boolean greater;
        boolean left = true;
        boolean right = true;

        if (arr[i] < arr[start]) {
            smaller = allElementSmall(arr, val, i, j == -1 ? end : j - 1);
            if (smaller) {
                left = isValidPreOrder(arr, i, j == -1 ? end : j - 1);
            } else {
                left = false;
            }
        }

        if (j > start) {
            greater = allElementGreater(arr, val, j, end);
            if (greater) {
                right = isValidPreOrder(arr, j, end);
            } else {
                right = false;
            }
        }

        return left && right;
    }

    public boolean allElementSmall(int[] arr, int val, int start, int end) {
        for (int i = start; i <= end; i++) {
            if (arr[i] > val) {
                return false;
            }
        }
        return true;
    }

    public boolean allElementGreater(int[] arr, int val, int start, int end) {
        for (int i = start; i <= end; i++) {
            if (arr[i] < val) {
                return false;
            }
        }
        return true;
    }

    public void inOrderIterative() {
        if (this.root != null) {
            inOrderIterative(this.root);
        }
    }

    public Stack<Node> inOrderIterative(Node root) {
        Queue<Node> q = new LinkedList<>();
        Stack<Node> s = new Stack<>();
        Stack<Node> p = new Stack<>();
        q.add(root);
        while (true) {
            while (!q.isEmpty()) {
                Node node = q.poll();
                s.push(node);
                if (node.left != null) {
                    q.add(node.left);
                }
            }
            if (s.isEmpty()) {
                break;
            }
            while (!s.isEmpty()) {
                Node node = s.pop();
                p.push(node);
                if (node.right != null) {
                    q.add(node.right);
                    break;
                }
            }
        }

        Stack<Node> reverse = new Stack<>();
        while (!p.isEmpty()) {
            reverse.push(p.pop());
        }
        return reverse;
    }

    ArrayList<ArrayList<Integer>> result = new ArrayList<>();
    public ArrayList<ArrayList<Integer>> rootToLeafPathWithSum(int k){
        ArrayList<Integer> list = new ArrayList<>();
        rootToLeafPathWithSum(this.root, k, 0, list);
        return  result;
    }

    public void rootToLeafPathWithSum(Node root, int k, int sum, ArrayList<Integer> list){
        if(root == null){
            return;
        }

        list.add(root.data);
        if(root.left == null && root.right == null){
            if(root.data+sum == k){
                result.add(list);
            }
        } else {
            if(root.left != null){
                ArrayList<Integer> newList = new ArrayList<>(list);
                rootToLeafPathWithSum(root.left, k, root.data+sum, newList);
            }
            if(root.right != null){
                ArrayList<Integer> newList = new ArrayList<>(list);
                rootToLeafPathWithSum(root.right, k, root.data+sum, newList);
            }
        }
    }

    public ArrayList<ArrayList<Integer>> pathToGivenNode(int k){
        ArrayList<Integer> list = new ArrayList<>();
        pathToGivenNode(this.root, k, list);
        return  result;
    }

    public void pathToGivenNode(Node root, int k, ArrayList<Integer> list){
        if(root == null){
            return;
        }
        list.add(root.data);
        if(root.data == k){
            result.add(list);
        } else {
            if(root.left != null){
                ArrayList<Integer> newList = new ArrayList<>(list);
                pathToGivenNode(root.left, k, newList);
            }
            if(root.right != null){
                ArrayList<Integer> newList = new ArrayList<>(list);
                pathToGivenNode(root.right, k, newList);
            }
        }
    }

    public boolean twoSumBinaryTree(int k){
        if(this.root == null){
            return false;
        }

        Stack<Node> s1 = new Stack<>(); // s1 will keep nodes till extreme left
        Stack<Node> s2 = new Stack<>(); // s2 will keep nodes till extreme right
        Node tempL = this.root; // tempL represents leftMost nodes in BST
        Node tempR = this.root; // tempR represents rightMost nodes in BST

        //take tempL to extreme left
        while(tempL != null){
            s1.push(tempL);
            tempL = tempL.left;
        }

        //take tempR to extreme right
        while (tempR != null){
            s2.push(tempR);
            tempR = tempR.right;
        }

        tempL = s1.peek();
        tempR = s2.peek();
        while (tempL != null && tempR != null && tempL.data < tempR.data){
            //calculating sum
            int sum = tempL.data + tempR.data;

            //if sum == k, return true;
            if(sum == k){
                return true;
            } else if (sum < k){ // if sum is less than k
                if(s1.isEmpty()){
                    return false;
                }
                s1.pop();
                tempL = tempL.right; // move tempL to the extreme left of right side of it's own parent node
                while(tempL != null){
                    s1.push(tempL);
                    tempL = tempL.left;
                }
                tempL = s1.peek();
            } else { // if sum is greater than k
                if(s2.isEmpty()){
                    return false;
                }
                s2.pop();
                tempR = tempR.left; // move tempR to the extreme right side of it's own parent node
                while(tempR != null){
                    s2.push(tempR);
                    tempR = tempR.right;
                }
                tempR = s2.peek();
            }
        }
        return false;
    }

    Node head = null;
    Node last = null;

    public void flatten(){
        flatten(this.root);
    }
    public void flatten(Node root)
    {
        preOrderLL(root);
        Node curr = head;
        while(curr != null){
            curr.right = curr.left;
            curr.left = null;
            curr = curr.right;
        }

        preOrderPrint(head);
    }

    public void preOrderLL(Node root)
    {
        if(root == null)
        {
            return;
        }

        if(last == null)
        {
            last = root;
            head = last;
        }
        else
        {
            last.left = root;
            last = root;
        }

        preOrderLL(root.left);
        preOrderLL(root.right);
    }

    public void preOrderPrint(Node root)
    {
        if(root == null)
        {
            System.out.print("null,");
            return;
        }

        System.out.print(root.data+",");
        preOrderPrint(root.left);
        preOrderPrint(root.right);
    }

    public int next(){
        if(hasNext()){
            Node node = s.pop();
            if(node.right != null){
                q.add(node.right);
                this.bstIterator();
            }
            return node.data;
        }
        return -1;
    }

    public boolean hasNext(){
        return !s.isEmpty() || !q.isEmpty();
    }

    public void bstIterator(){
        if(root == null){
            return;
        }
        while (!q.isEmpty()){
            Node node = q.poll();
            s.push(node);
            if(node.left != null) {
                q.add(node.left);
            }
        }
    }

}
