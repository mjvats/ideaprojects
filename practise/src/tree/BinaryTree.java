package tree;

public class BinaryTree {

    Node root;
    BinaryTree(){
        this.root = null;
    }

    public void add(int val){
        Node node = new Node(val);
        if(root == null){
            root = node;
        }else{
            add(root, val);
        }
    }

    public void add(Node root, int val){
        if(root.left == null){
            root.left = new Node(val);
        } else if(root.right == null){
            root.right = new Node(val);
        } else {
            add(root.left, val);
            add(root.right, val);
        }
    }

}
