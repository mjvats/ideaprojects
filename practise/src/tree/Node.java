package tree;

public class Node {

    int data;
    int height;
    Node left;
    Node right;

    Node(int data) {
        this.data = data;
        this.left = null;
        this.right = null;
    }

    Node(int data, int height) {
        this.data = data;
        this.height = height;
        this.left = null;
        this.right = null;
    }
}
