package tree;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class BinarySearchTreeTest {

    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            BinarySearchTree bst = new BinarySearchTree();
            int n = Integer.parseInt(br.readLine());
            int[] arr = new int[n];
            String[] input = br.readLine().trim().split(" ");
            //int[] arr = new int[input.length];
            for(int i = 0 ; i < n ; i++) {
                bst.add(Integer.parseInt(input[i]));
                //arr[i] = Integer.parseInt(input[i]);
            }
            ArrayList<Integer> heights = bst.heights;
            for(int height : heights)
                bw.write(height+" ");
            bw.newLine();
            T--;
        }
        br.close();
        bw.close();
    }

    public static void main(int[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            BinarySearchTree bst = new BinarySearchTree();
            String[] line1 = br.readLine().trim().split(" ");
            int N = Integer.parseInt(line1[0]);
            int Q = Integer.parseInt(line1[1]);

            int[] arr = new int[N];
            String[] line2 = br.readLine().trim().split(" ");
            for(int i = 0 ; i < N ; i++) {
                bst.add(Integer.parseInt(line2[i]));
            }

            while (Q > 0){
                String[] input = br.readLine().trim().split(" ");
                int u = Integer.parseInt(input[0]);
                int v = Integer.parseInt(input[1]);
                bw.write(bst.lca(u , v)+" ");
                Q--;
            }
            bw.write("\n");
            T--;
        }
        br.close();
        bw.close();
    }

}
