import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ThreeSumZero {

    public static void main(String[] args) {
        Integer[] arr = {2147483647, -2147483648, -2147483648, 0, 1};
        List<Integer> A = Arrays.asList(arr);
        ArrayList<ArrayList<Integer>> result = threeSumZero(A);
        for (ArrayList<Integer> list : result) {
            System.out.print("[");
            for (Integer val : list) {
                System.out.print(val + " ");
            }
            System.out.print("] ");
        }
    }

    public static ArrayList<ArrayList<Integer>> threeSum(List<Integer> A) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        ArrayList<Integer> templist;
        Collections.sort(A);
        HashMap<String, Integer> duplicateFinder = new HashMap<>();
        for (int i = 0; i < A.size() - 2; i++) {
            BigInteger firstNum = new BigInteger(String.valueOf(A.get(i)));
            BigInteger diff = BigInteger.ZERO.subtract(firstNum);
            templist = new ArrayList<>();
            for (int j = i + 1, k = A.size() - 1; j < k; ) {
                BigInteger numAtJ = new BigInteger(String.valueOf(A.get(j)));
                BigInteger numAtK = new BigInteger(String.valueOf(A.get(k)));
                BigInteger sum = numAtJ.add(numAtK);
                if (sum.compareTo(diff) == 0) {
                    templist.add(firstNum.intValue());
                    templist.add(A.get(j));
                    templist.add(A.get(k));
                    String temp = "" + firstNum.intValue() + A.get(i) + A.get(k);
                    if (!duplicateFinder.containsKey(temp)) {
                        result.add(templist);
                        duplicateFinder.put(temp,1);
                    }
                    k--;
                    templist = new ArrayList<>();
                } else if (sum.compareTo(diff) == -1) {
                    j++;
                } else {
                    k--;
                }
            }
        }
        return result;
    }

    public static ArrayList<ArrayList<Integer>> threeSumZero(List<Integer> A) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        ArrayList<Integer> templist;
        Collections.sort(A);
        HashMap<String, Integer> duplicateFinder = new HashMap<>();
        for (int i = 0; i < A.size() - 2; i++) {
            int firstNum = A.get(i);
            long diff = 0L-firstNum;
            templist = new ArrayList<>();
            for (int j = i + 1, k = A.size() - 1; j < k; ) {
                long sum = (long)A.get(j) + (long)A.get(k);
                if (sum == diff) {
                    templist.add(firstNum);
                    templist.add(A.get(j));
                    templist.add(A.get(k));
                    String temp = "" + firstNum + A.get(i) + A.get(k);
                    if (!duplicateFinder.containsKey(temp)) {
                        result.add(templist);
                        duplicateFinder.put(temp,1);
                    }
                    k--;
                    templist = new ArrayList<>();
                } else if (sum < diff) {
                    j++;
                } else {
                    k--;
                }
            }
        }
        return result;
    }


}
