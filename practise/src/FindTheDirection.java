import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class FindTheDirection {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            int X = Integer.parseInt(br.readLine());
            bw.write(sol(X));
            bw.write("\n");
            T--;
        }
        br.close();
        bw.close();
    }

    public static String sol(int X){
        int val = X%4;
        if(val == 1) return "East";
        else if(val == 2) return "South";
        else if(val == 3) return "West";
        else return "North";
    }

}
