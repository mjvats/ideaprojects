import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ApowerB {

    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){

            String[] input = br.readLine().trim().split(" ");
            long startTime = System.currentTimeMillis();
            int a = Integer.parseInt(input[0]);
            int b = Integer.parseInt(input[1]);

            long result = getPower(a, b);
            bw.write(String.valueOf(result));
            bw.write("\n");
            long endTime = System.currentTimeMillis();
            bw.write(endTime-startTime+"");
            T--;
        }

        br.close();
        bw.close();
    }

    private static long getPower(int a, int b){
        int bitSize = Integer.toBinaryString(b).length();
        long ans = 1, x = 1;
        int k = 64735492;
        for(int i = 0 ; i < bitSize ; i++){
            x = i == 0 ? a : (x*x)%k;
            if(((b >> i) & 1) == 1){
                ans = (ans * x)%k;
            }
        }
        return ans;
    }
}
