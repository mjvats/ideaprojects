import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Interleavings {

    public static void main(String[] args) {
        String str1 = "nkb";
        String str2 = "gl";

        int N = str1.length()+str2.length();
        char[] result = new char[N];
        List<String> list = new ArrayList<>();
        func(str1.toCharArray(), str2.toCharArray(), result, list, N, 0, 0, 0);
        Collections.sort(list);
        for(String val : list){
            System.out.println(val);
        }
    }

    public static void func(char[] str1, char[] str2, char[] result, List<String> list, int N, int idx1, int idx2, int idxr){
        if(idxr == N){
            list.add(String.valueOf(result));
            return;
        }

        if(idx1 < str1.length){
            result[idxr] = str1[idx1];
            func(str1, str2, result, list, N, idx1+1, idx2, idxr+1);
        }

        if(idx2 < str2.length){
            result[idxr] = str2[idx2];
            func(str1, str2, result, list, N, idx1, idx2+1, idxr+1);
        }

    }

}
