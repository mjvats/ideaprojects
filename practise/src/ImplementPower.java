public class ImplementPower {

    public static void main(String[] args) {
        int result = pow(71045970, 41535484, 64735492);
        System.out.println(result);
    }

    public static int pow(int x, int n, int d) {
        if(x == 0)
        {
            return 0;
        }
        else if (n == 0)
        {
            return 1;
        }
        else
        {
            long result = pow((long)x, (long) n, (long)d);
            result = result % d;
            if(result < 0)
            {
                result = d + result;
            }
            return (int) result;
        }

    }

    public static long pow(long x, long n, long d){
        long temp = 1;
        if(n == 1)
        {
            return x;
        }
         temp = pow(x, n/2, d);
        long result;
        if(n % 2 == 0)
        {
            result = (temp * temp) % d;
        }
        else
        {
            result = (x*((temp * temp)%d)) % d;
        }
        return result;
    }

}
