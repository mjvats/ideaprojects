package sorting;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class FrequencySort {

    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0)
        {
            int n = Integer.parseInt(br.readLine());
            int[] a = new int[n];
            String[] line1 = br.readLine().trim().split(" ");
            for(int i = 0 ; i < n ; i++) a[i] = Integer.parseInt(line1[i]);

            int[] result = sort(a);
            for(int i = 0 ; i < n ; i++) bw.write(result[i]+" ");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    private static int[] sort(int[] arr)
    {
        int n = arr.length;
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int val : arr) {
            min = Integer.min(min, val);
            max = Integer.max(max, val);
        }

        int size = (max-min)+1;
        int[] count = new int[size];
        for (int val : arr) count[Math.abs(min) + val]++;

        int minCount = n;
        int maxCount = 1;
        for (int val : count) {
            if(val > 0 && val < minCount) minCount = val;
            maxCount = Integer.max(maxCount, val);
        }

        int[] result = new int[n];
        int index = 0;
        int k = minCount > 0 ? minCount : maxCount;
        while (k <= maxCount)
        {
            for(int i = 0 ; i < size ; i++)
            {
                if(count[i] == k)
                {
                    for(int j = 0 ; j < count[i] ; j++)
                    {
                        result[index++] = min+i;
                    }
                }
            }
            k++;
        }
        return result;
    }

}
