package sorting;

public class FillTwoSortedArrays {

    public static void main(String[] args) {
        int N = 7;
        int M = 5;
        int[] A = {-3, 5, 10, 12, 18, 25, 34, 0, 0, 0, 0, 0};
        int[] B = {2, 7, 18, 45, 52};
        sort(A, B, N, M);
        for(int val : A){
            System.out.print(val+" ");
        }
    }

    public static void sort(int[] A, int[] B, int N, int M){
        int k = N+M-1;
        int i = N-1;
        int j = M-1;

        while (i >= 0 && j >= 0){
            if(A[i] >= B[j]){
                A[k--] = A[i--];
            } else {
                A[k--] = B[j--];
            }
        }
    }

}
