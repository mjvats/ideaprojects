package sorting;

public class SelectionSort {

    public static void main(String[] args) {
        int[] arr = {5, 1, 4, 2, 8};
        sort(arr);
        for(int val : arr){
            System.out.println(val+" ");
        }
    }

    public static void sort(int[] arr){
        int n = arr.length;
        for(int i = 0 ; i < n ; i++){
            int min = arr[i];
            int min_index = i;
            for(int j = i+1; j < n ; j++){
                if(arr[j] < min){
                    min = arr[j];
                    min_index = j;
                }
            }

            int temp = arr[min_index];
            arr[min_index] = arr[i];
            arr[i] = temp;
        }
    }

}
