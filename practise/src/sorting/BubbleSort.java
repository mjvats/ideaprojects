package sorting;

public class BubbleSort {

    public static void main(String[] args) {
        int[] arr = {5};
        sort(arr);
        for(int val : arr){
            System.out.print(val+" ");
        }
    }

    public static void sort(int[] arr){
        int n = arr.length;
        boolean swapped = true;
        while (swapped){
            swapped = false;
            for(int i = 1 ; i < n ; i++){
                if(arr[i] < arr[i-1]){
                    swapped = true;
                    arr[i] = arr[i]^arr[i-1];
                    arr[i-1] = arr[i]^arr[i-1];
                    arr[i] = arr[i]^arr[i-1];
                }
            }
        }
    }

}
