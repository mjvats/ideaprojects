package sorting;

public class InsertionSort {

    public static void main(String[] args) {
        int[] A = {-30, 15, 20, 10, -10 };;
        //int[] A = {3, 7, 0, 9, 2, 8, 4};
        insertionSort(A);
        for(int i = 0 ; i < A.length ; i++){
            System.out.print(A[i] + " ");
        }
    }

    private static void insertionSort(int[] A){
        for(int i = 1 ; i < A.length ; i++){
            int key = A[i];
            int j = i-1;
            while(j >= 0 && A[j] > key){
                A[j+1] = A[j];
                j--;
            }
            A[j+1] = key;
        }
    }

}
