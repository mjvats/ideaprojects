package sorting;

public class CountSort {

    public static void main(String[] args) {
        int[] arr = {3, 5, 10, 3, 2, 5, 5, 8, 10, 5, 8, 5};
        sort(arr, 12, 10);
        for(int val : arr){
            System.out.print(val+" ");
        }
    }

    public static void sort(int[] arr, int N, int M){
        int[] count = new int[M+1];
        for(int val : arr){
            count[val]++;
        }

        int x = 0;
        for(int i = 0 ; i <= M ; i++){
            if(count[i] > 0){
                while (count[i] > 0){
                    arr[x++] = i;
                    count[i]--;
                }
            }
        }
    }

}
