package sorting;

public class MergeSort {

    public static void main(String[] args) {
        int[] A = {-4, 0, 10, -7} ;
        mergeSort(A, 0, A.length-1);
        for(int i = 0 ; i < A.length ; i++){
            System.out.print(A[i]+" ");
        }
    }

    static void mergeSort(int[] A, int start, int end){
        if(start == end){
            return;
        }
        int mid = (start + end)/2;
        mergeSort(A, start, mid);
        mergeSort(A, mid+1, end);
        merge(A, start, end);
    }

    static void merge(int[] A, int start, int end){
        int mid = (start + end)/2;
        int[] L = new int[mid - start +1];
        int[] R = new int[end - mid];
        int k = 0;
        for(int i = start ; i <= mid ; i++){
            L[k++] = A[i];
        }
        k=0;
        for(int i = mid+1 ; i <= end ; i++){
            R[k++] = A[i];
        }

        int i = 0; int j = 0; k = start;
        while(i < L.length && j < R.length){
            if(L[i] <= R[j]){
                A[k++] = L[i];
                i++;
            }
            else{
                A[k++] = R[j];
                j++;
            }
        }

        while(i < L.length){
            A[k++] = L[i];
            i++;
        }

        while(j < R.length){
            A[k++] = R[j];
            j++;
        }
    }
}
