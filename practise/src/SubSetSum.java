import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SubSetSum {

    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        int T = Integer.valueOf(br.readLine());
        while(T > 0){

            String[] line1 = br.readLine().trim().split(" ");
            int N = Integer.valueOf(line1[0]);
            int K = Integer.valueOf(line1[1]);

            String[] line2 = br.readLine().trim().split(" ");
            int[] arr = new int[N];
            for(int i = 0 ; i < N ; i++){
                arr[i] = Integer.valueOf(line2[i]);
            }

            if(sol(arr, K)){
                System.out.println("YES");
            }
            else{
                System.out.println("NO");
            }

            T--;
        }
        br.close();

    }

    private static boolean func(int[] arr, int k){
        long subsets = 1L << arr.length;
        for(int i = 0 ; i < subsets ; i++){
            int sum = 0;
            for(int j = 0 ; j < arr.length ; j++){
                if(((i >> j)&1)==1){
                    sum += arr[j];
                }
            }
            if(sum == k){
                return true;
            }
        }
        return false;
    }

    private static boolean sol(int[] arr, int k){
        return sol(arr, 0, k, 0);
    }

    private static boolean sol(int[] arr, int sum, int k, int i){
        if(sum == k){
            return true;
        }
        if(i == arr.length){
            return false;
        }
        return sol(arr, sum, k, i+1) || sol(arr, sum + arr[i], k, i+1);
    }
}
