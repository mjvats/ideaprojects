public class RotateArray {

    public static void main(String args[]){
        int[] nums = {1,2};
        new RotateArray().rotate(nums, 3);
    }

    public void rotate(int[] nums, int k) {
        if(!validateInput(nums, k))
            return;

        reverse(nums, 0 , nums.length-k-1);
        reverse(nums, nums.length-k , nums.length-1);
        reverse(nums, 0, nums.length-1);

        for(int i = 0 ; i < nums.length ; i++){
            System.out.print(nums[i]);
        }
    }

    private int[] reverse(int[] input, int i , int j){
        while(i < j){
            input[i] = input[i] ^ input[j];
            input[j] = input[i] ^ input[j];
            input[i] = input[i] ^ input[j];
            i++;
            j--;
        }
        return input;
    }

    private boolean validateInput(int[] nums, int k){
        if(nums.length < 1 || nums.length > 100000 || k < 0 || k > 100000 || k > nums.length){
            return false;
        }
        for(int i = 0 ; i < nums.length ; i++){
            if(nums[i] < Integer.MIN_VALUE || nums[i] > Integer.MAX_VALUE){
                return false;
            }
        }
        return true;
    }

}
