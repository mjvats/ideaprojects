import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DuplicateRemoval {

    public static void main(String[] args) {
        Integer[] A = {0};
        List<Integer> list = Arrays.asList(A);
        int size = removeDuplicates(list);
    }

    public static int removeDuplicates(List<Integer> b) {
        ArrayList<Integer> a = new ArrayList<>(b);
        int i = 0;
        int j = 0;
        while(j < a.size()-1) {
            if(!a.get(j).equals(a.get(j+1))){
                a.set(i+1, a.get(j+1));
                i++;
            }
            j++;
        }
        return i+1;
    }

}
