public class MoveZeroes {

    public static void main(String[] args) {
        int[] nums = {0,1,0,3,12};
        new MoveZeroes().moveZeroes(nums);
        for(int i = 0 ; i < nums.length ; i++){
            System.out.println(nums[i]);
        }
    }

    public void moveZeroes(int[] nums) {
        if(!validateInput(nums))
            return;

        for(int i = 0 ; i < nums.length ; i++){
            if(nums[i] < Integer.MIN_VALUE || nums[i] > Integer.MAX_VALUE){
                return;
            }
            if(nums[i] == 0 && i < nums.length-1){
                System.out.println("i:" + i);
                int j = i+1;
                while(nums[j] == 0){
                    j++;
                }
                if(j == nums.length){
                    return;
                }
                else{
                    nums[i] = nums[i]^nums[j];
                    nums[j] = nums[i]^nums[j];
                    nums[i] = nums[i]^nums[j];
                }
            }
        }
    }

    private boolean validateInput(int[] nums){
        if(nums.length < 1 || nums.length > 10000){
            return false;
        }
        return true;
    }

}
