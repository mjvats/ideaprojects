public class ExcelColNumber {

    public static void main(String[] args) {
       // System.out.println(titleToNumber("AZ"));
        System.out.println(numberToTitle(52));
    }

    public static int titleToNumber(String A) {
        int result = 0;
        int x = 1;
        for(int i = 0, j = A.length()-1 ; i < A.length() && j >= 0 ; i++, j--){
            if(i > 0){
                x = x*26;
            }
            result = result + (A.charAt(j)-'A'+1)*x;
        }

        return result;
    }

    public static String numberToTitle(int A){

        StringBuilder sb = new StringBuilder();
        while(A > 0){
            int remainder = A%26;
            if(remainder == 0){
                sb.append("Z");
                A = (A/26)-1;
            }
            else{
                sb.append((char)(remainder-1+'A'));
                A = A/26;
            }
        }
        sb = sb.reverse();
        return sb.toString();
    }

}
