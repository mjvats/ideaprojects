import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class TripletSum
{

    public static void main(String[] args) throws IOException
    {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.valueOf(br.readLine());

        while(T > 0)
        {

            String[] line1 = br.readLine().trim().split(" ");
            int N = Integer.valueOf(line1[0]);
            int K = Integer.valueOf(line1[1]);
            int[] A = new int[N];
            String[] input = br.readLine().trim().split(" ");
            for(int i = 0 ; i < N; i++)
            {
                A[i] = Integer.valueOf(input[i]);
            }
            bw.write(String.valueOf(tripletSum(A, K)));
            bw.write("\n");
            T--;

        }

        br.close();
        bw.close();
    }

    static boolean tripletSum(int[] A, int K)
    {
        mergeSort(A, 0, A.length-1);
        for(int i = 0; i < A.length-2 ; i++)
        {
            int firstNum = A[i];
            int diff = K - firstNum;
            for(int j = i+1, k = A.length-1 ; j < k;)
            {
                int sum = A[j] + A[k];
                if(sum == diff)
                {
                    return true;
                }
                else if(sum < diff)
                {
                    j++;
                }
                else {
                    k--;
                }
            }
        }
        return false;
    }

    static public void mergeSort(int arr[], int start, int end)
    {
        if(start == end)
        {
            return;
        }
        else {
            int mid = (start+end)/2;
            mergeSort(arr, start, mid);
            mergeSort(arr, mid+1, end);
            merge(arr, start, mid, end);
        }
    }

    static void merge(int arr[], int start, int mid, int end)
    {
        int[] left = new int[mid-start+1];
        int[] right = new int[end-mid];

        int k = 0;
        for(int i = start ; i <= mid ; i++)
            left[k++] = arr[i];
        k = 0;
        for(int i = mid+1 ; i <= end ; i++)
            right[k++] = arr[i];

        k = start;
        int i = 0, j = 0;
        while(i < left.length && j < right.length)
        {
            if(left[i] <= right[j])
            {
                arr[k] = left[i];
                i++;
            }
            else {
                arr[k] = right[j];
                j++;
            }
            k++;
        }

        while(i < left.length)
        {
            arr[k] = left[i];
            i++;k++;
        }
        while(j < right.length)
        {
            arr[k] = right[j];
            j++;
            k++;
        }
    }

}
