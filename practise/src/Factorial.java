import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.Integer;

public class Factorial {

    public static void main(String[] args) {
        int a = Integer.toBinaryString(697264981).length();
        System.out.println(fact(a));
    }
    public static void main() throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.valueOf(br.readLine());
        int[] input = new int[T];
        for(int i = 0 ; i < input.length ; i++){
            input[i] = Integer.valueOf(br.readLine());
        }

        int max = getMax(input);
        long[] result = new long[max];

        fact(input, result, bw);
        bw.close();
        br.close();

    }

    private static int getMax(int[] arr){
        int max = arr[0];
        for(int val : arr){
            if(val > max)
                max = val;
        }
        return max;
    }

    private static void fact(int[] input, long[] result, BufferedWriter bw) throws IOException{
        for(int i = 0 ; i < input.length ; i++){
            int num = input[i];
            if(num == 0){
                bw.write(String.valueOf(num)+"\n");
                continue;
            }
            if(result[num-1] > 0){
                bw.write(String.valueOf(result[num-1])+"\n");
            }
            else{
                long val = 1L;
                while(num > 1){
                    if(result[num-1] > 0){
                        val = (val*result[num-1])%1000000007;
                        break;
                    }
                    else{
                        val = (val*num)%1000000007;
                    }
                    num--;
                }
                result[input[i]-1] = val;
                bw.write(String.valueOf(val)+"\n");
            }
        }
    }


    private static long fact(int N){
        if(N == 0){
            return 0L;
        }
        long result = 1L;
        while(N > 1){
            result = (result*N)%1000000007;
            N--;
        }
        return result;
    }

}
