import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrintQueryCount {

    public static void main(String[] args) {
        Integer[] inp = {3, 8, 5, 10, 5, -6, -10, 2, 2, 5, 3, 3, 25, 3};
        Integer[] query = {3, 5, -6, 5};
        List<Integer> inpList = Arrays.asList(inp);
        List<Integer> q = Arrays.asList(query);
        func(inpList, q);
    }

    public static void func(List<Integer> inp, List<Integer> q){
        List<Integer> tempList = new ArrayList<>(q);
        Collections.sort(inp);
        Collections.sort(tempList);

        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0, j = 0 ; i < tempList.size() && j < inp.size() ; i++){
            if(!map.containsKey(tempList.get(i))){
                while(j < inp.size() && !inp.get(j).equals(tempList.get(i))){
                    j++;
                }
                if(j < inp.size() && inp.get(j).equals(tempList.get(i))){
                    int count = 0;
                    while (j < inp.size() && inp.get(j).equals(tempList.get(i))){
                        j++;
                        count++;
                    }
                    map.put(tempList.get(i), count);
                }
            }
        }

        for(int val : q){
            System.out.println(val + " : "+ map.get(val));
        }

    }

}
