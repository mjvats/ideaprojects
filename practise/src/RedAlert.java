import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class RedAlert {

    public static void main (String[] args) throws java.lang.Exception
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.valueOf(br.readLine());
        while(T > 0)
        {
            String[] line1 = br.readLine().trim().split(" ");
            int N = Integer.valueOf(line1[0]);
            int D = Integer.valueOf(line1[1]);
            int H = Integer.valueOf(line1[2]);

            String[] line2 = br.readLine().trim().split(" ");
            int[] A = new int[N];
            for(int i = 0 ; i < N ; i++)
            {
                A[i] = Integer.valueOf(line2[i]);
            }
            if(redalert(A, N, D, H))
            {
                bw.write("YES");
            }
            else
            {
                bw.write("NO");
            }
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static boolean redalert(int[] A, int N, int D, int H)
    {
        int[] temp = new int[N];
        temp[0] = A[0];
        for(int i = 1; i < N ; i++)
        {
            if(A[i] == 0)
            {
                temp[i] = Integer.max(temp[i-1]-D, 0);
            }
            else
            {
                temp[i] = temp[i-1] + A[i];
            }
            if(temp[i] > H)
            {
                return true;
            }
        }
        return false;
    }


}
