public class SetBits {

    public static void main(String[] args) {
        int result = setBitCount(7);
        System.out.println(result);
    }

    private static int countSet(int N){
        long count = 0;
        N++;
        for(int i = 0 ; i < 32 ; i++){
            long power = (long) Math.pow(2, i);
            long totalPair = N/power;
            if(totalPair % 2 == 0){
                count = count + (totalPair/2)*power;
            }
            else{
                count = count + (totalPair/2)*power + N%power;
            }
        }

        return (int) (count%1000000007);
    }

    private static int setBitCount(int N){
        int count = 0;
        while (N != 0){
            N = N & (N-1);
            count++;
        }

        return count;
    }

}
