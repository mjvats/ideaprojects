import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TwoOutOfThree {

    public static void main(String[] args) {
        ArrayList<Integer> A = new ArrayList<>(Arrays.asList(1, 2));
        ArrayList<Integer> B = new ArrayList<>(Arrays.asList(1, 3));
        ArrayList<Integer> C = new ArrayList<>(Arrays.asList(2, 3));

        ArrayList<Integer> result = solve(A, B, C);
        for (Integer val : result) {
            System.out.print(val+",");
        }
    }

    public static ArrayList<Integer> solve(ArrayList<Integer> A, ArrayList<Integer> B,
        ArrayList<Integer> C) {
        int a = A.size();
        int b = B.size();
        int c = C.size();
        Map<Integer, Set<Character>> map = new HashMap<>();
        int i = 0, j = 0, k = 0;
        for (;i < a && j < b && k < c ; i++, j++, k++) {
            if (map.containsKey(A.get(i))) {
                Set<Character> value = map.get(A.get(i));
                value.add('a');
            } else {
                Set<Character> tempset = new HashSet<>();
                tempset.add('a');
                map.put(A.get(i), tempset);
            }

            if (map.containsKey(B.get(j))) {
                Set<Character> value = map.get(B.get(j));
                value.add('b');
            } else {
                Set<Character> tempset = new HashSet<>();
                tempset.add('b');
                map.put(B.get(j), tempset);
            }

            if (map.containsKey(C.get(k))) {
                Set<Character> value = map.get(C.get(k));
                value.add('c');
            } else {
                Set<Character> tempset = new HashSet<>();
                tempset.add('c');
                map.put(C.get(k), tempset);
            }
        }

        while (i < a) {
            if (map.containsKey(A.get(i))) {
                Set<Character> value = map.get(A.get(i));
                value.add('a');
            } else {
                Set<Character> tempset = new HashSet<>();
                tempset.add('a');
                map.put(A.get(i), tempset);
            }
            i++;
        }

        while (j < b) {
            if (map.containsKey(B.get(j))) {
                Set<Character> value = map.get(B.get(j));
                value.add('b');
            } else {
                Set<Character> tempset = new HashSet<>();
                tempset.add('b');
                map.put(B.get(j), tempset);
            }
            j++;
        }

        while (k < c) {
            if (map.containsKey(C.get(k))) {
                Set<Character> value = map.get(C.get(k));
                value.add('c');
            } else {
                Set<Character> tempset = new HashSet<>();
                tempset.add('c');
                map.put(C.get(k), tempset);
            }
            k++;
        }

        ArrayList<Integer> result = new ArrayList<>();
        for (Map.Entry<Integer, Set<Character>> entry : map.entrySet()) {
            if (entry.getValue().size() >= 2) {
                result.add(entry.getKey());
            }
        }

        Collections.sort(result);
        return result;
    }
}
