import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class WoodCutting {

    public static void main(String[] args) {
        List<Integer> A = Arrays.asList(73, 79, 79, 115, 109, 82);
        int result = solve(A, 96);
        System.out.println(result);
    }

    public static int solve(List<Integer> A, int B) {

        Collections.sort(A);
        int max = Collections.max(A);
        int start = 0, end = max, mid, diff = Integer.MAX_VALUE, result = 0;

        while (start <= end)
        {
            mid = (start + end)/2;
            int sum = cutMid(A, mid);
            if(sum == B)
            {
                return mid;
            }
            else if(sum < B)
            {
                end = mid-1;
            }
            else
            {
                if((sum-B) < diff)
                {
                    diff = sum-B;
                    result = mid;
                }
                start = mid+1;
            }
        }
        return result;
    }

    public static int cutMid(List<Integer> A, int x)
    {
        int start = 0, end = A.size()-1, mid = 0;
        while(start <= end)
        {
            mid = (start + end)/2;
            if(mid == 0 ){
                if(A.get(mid) >= x)
                {
                    break;
                }
                else
                {
                    start = mid+1;
                }
            }
            else if(A.get(mid) >= x && A.get(mid-1)< x)
            {
                break;
            }
            else if(A.get(mid) >= x && A.get(mid-1) >= x)
            {
                end = mid-1;
            }
            else
            {
                start = mid+1;
            }
        }
        int sum = 0;
        for(int i = mid; i < A.size() ; i++)
        {
            if((A.get(i)-x) >= 0)
            {
                sum += (A.get(i)-x);
            }
        }
        return sum;
    }

}
