public class Sqrt {

    public static void main(String[] args) {
        int result = sqrt(930675566);
        System.out.println(result);
    }
    public static int sqrt(int A) {

        if(A == 0 || A == 1)
        {
            return A;
        }
        long start = 0;
        long end = A/2;
        long ans = 0;
        while(start <= end)
        {
            long mid = (start + end)/2;
            if(mid*mid == A)
            {
                ans = mid;
                break;
            }
            else if(mid*mid < A){
                ans = mid;
                start = mid+1;
            }
            else{
                end = mid-1;
            }
        }
        return (int)ans;
    }
}
