import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MaxSumContigousSubArray {

    public static void main(String[] args) {
        int[] A = {1, 2, 3, 4, -10};
        ArrayList<Integer> inputList = new ArrayList<>();
        inputList.add(1);
        inputList.add(2);
        inputList.add(3);
        inputList.add(4);
        inputList.add(-10);

        int result = getMaxSumContigousSubArray(inputList);
        System.out.println(result);
    }

    private static int getMaxSumContigousSubArray(final List<Integer> A){
        ArrayList<Integer> temp = new ArrayList<>(A.size());
        temp.add(0, A.get(0));
        for(int i = 1; i < A.size() ; i++) {
            temp.add(i, getMax(A.get(i), temp.get(i - 1), A.get(i) + temp.get(i - 1)));
        }
        return Collections.max(temp);
    }

    private static int getMax(int a, int b, int c){
        return Integer.max(a, Integer.max(b,c));
    }

}
