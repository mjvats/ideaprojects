import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class PowerGame {

    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0)
        {
            int n = Integer.parseInt(br.readLine());
            int[] a = new int[n];
            int[] b = new int[n];
            String[] line1 = br.readLine().trim().split(" ");
            for(int i = 0 ; i < n ; i++) a[i] = Integer.parseInt(line1[i]);

            String[] line2 = br.readLine().trim().split(" ");
            for(int i = 0 ; i < n ; i++) b[i] = Integer.parseInt(line2[i]);

            bw.write(String.valueOf(sol(a, b)));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int sol(int[] a, int[] b)
    {
        int n = a.length;
        int m = b.length;
        mergeSort(a, 0, n-1);
        mergeSort(b, 0, m-1);

        int ans = 0;
        for(int i = 0, j = 0 ; i < n && j < m ;)
        {
            if(b[j] < a[i])
            {
                j++;
                ans++;
            }
            i++;
        }
        return ans;
    }

    private static void mergeSort(int[] arr, int start, int end)
    {
        if(start < end)
        {
            int mid = (start+end)/2;
            mergeSort(arr, start, mid);
            mergeSort(arr, mid+1, end);
            merge(arr, start, end);
        }
    }

    private static void merge(int[] arr, int start, int end)
    {
        int mid = (start+end)/2;
        int[] L = new int[mid-start+1];
        int[] R = new int[end-mid];

        int k = start;
        for(int i = 0 ; i < L.length ; i++) L[i] = arr[k++];
        for(int j = 0 ; j < R.length ; j++) R[j] = arr[k++];

        int i = 0 , j = 0;
        k = start;
        while (i < L.length && j < R.length)
        {
            if(L[i] < R[j]) arr[k++] = L[i++];
            else arr[k++] = R[j++];
        }

        while (i < L.length) arr[k++] = L[i++];
        while (j < R.length) arr[k++] = R[j++];

    }

}
