import java.util.Arrays;
import java.util.List;

public class MedianOfSortedArrays {

    public static void main(String[] args) {
        int[] A = {1, 4, 5};
        //final List<Integer> a = Arrays.asList(A);

        int[] B = {2, 3};
        //final List<Integer> b = Arrays.asList(B);

        System.out.println(func(A, B));
    }

    public static double findMedianSortedArrays(final List<Integer> a, final List<Integer> b) {

        int m = a.size();
        int n = b.size();
        double result = 0.0;
        int mid = ((m+n-1)/2);
        int i = 0, j = 0, k = 0;

        while(i < a.size() && j < b.size())
        {
            if(k == mid)
            {
                if((m+n)%2 != 0)
                {
                    result = Integer.min(a.get(i), b.get(j));
                }
                else
                {
                    if(a.get(i) < b.get(j))
                    {
                        result = i < a.size()-1 ? a.get(i) + Integer.min(a.get(i+1), b.get(j)) :
                            a.get(i) + b.get(j) ;
                    }
                    else
                    {
                        result = j < b.size()-1 ? b.get(j) + Integer.min(b.get(j+1), a.get(i)) :
                            b.get(j) + a.get(i);
                    }
                    result = result/2.0;
                }

                return result;
            }
            else if(a.get(i) < b.get(j))
            {
                i++;
            }
            else
            {
                j++;
            }
            k++;
        }

        while(i < a.size())
        {
            if(k == mid)
            {
                if((m+n)%2 != 0)
                {
                    result = a.get(i);
                }
                else
                {
                    result = (a.get(i)+ a.get(i+1))/2.0;
                }

                return result;
            }
            else{
                i++;
                k++;
            }
        }

        while(j < b.size())
        {
            if(k == mid)
            {
                if((m+n)%2 != 0)
                {
                    result = b.get(j);
                }
                else
                {
                    result = (b.get(j) + b.get(j+1))/2.0;
                }

                return result;
            }
            else{
                j++;
                k++;
            }
        }
        return result;

    }

    static int func(int[] A, int[] B){

        int low = Integer.MAX_VALUE;
        for(int val : A) low = Math.min(low, val);
        for(int val : B) low = Math.min(low, val);

        int high = Integer.MIN_VALUE;
        for(int val : A) high = Math.max(high, val);
        for(int val : B) high = Math.max(high, val);

        int N = A.length;
        int M = B.length;

        while(low <= high){
            int mid = (low + high)/2;
            int lt = bs1(A, N, mid) + bs2(B, M, mid);
            int gt = bs1(A, N, mid) + bs2(B, M, mid);

            if(lt < gt){
                low = mid+1;
            } else if(lt > gt){
                high = mid-1;
            } else {
                return mid;
            }
        }

        return -1;

    }

    //count of numbers greater than equal to k
    static int bs1(int[] arr, int N, int k){
        int low = 0;
        int high = N-1;
        int ans = -1;
        while(low <= high){
            int mid = (low + high)/2;
            if(arr[mid] >= k){
                ans = mid;
                high = mid-1;
            } else {
                low = mid+1;
            }
        }
        return ans;
    }

    //count of numbers less than equal to k
    static int bs2(int[] arr, int M, int k){
        int low = 0;
        int high = M-1;
        int ans = -1;
        while(low <= high){
            int mid = (low + high)/2;
            if(arr[mid] <= k){
                ans = mid;
                low = mid+1;
            } else {
                high = mid-1;
            }
        }
        return ans;
    }
}
