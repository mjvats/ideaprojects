import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestData {

    public static void main(String[] args) {
        String val = "BTecnotree2@";
        System.out.println(validatePassword(val));
    }

    public static boolean validatePassword(String password){
        char[] arr = password.toCharArray();
        int n = arr.length;

        //checking if length of password is not less than 10 and not greater than 12
        if(n < 10 || n > 12){
            return false;
        }

        int numCount = 0, specialCharCount = 0;
        boolean consecLowerCase = false, consecUpperCase = false;

        for(char c : arr){

            //checking count of numbers.
            if(c >= 48 && c <= 57){
                numCount++;
            }

            //checking if atleast special character is present or not. Couting special characters.
            if(c == 33 || (c >= 35 && c <= 38) || c == 42 || (c >= 63 && c <= 64)){
                specialCharCount++;
            }
        }

        //checking for consecutive lowercase characters
        for(int i = 1 ; i < n ; i++){
            if(arr[i] >= 97 && arr[i] <= 122 && arr[i-1] >= 97 && arr[i-1] <= 122){
                consecLowerCase = true;
            }
        }

        //checking for consecutive uppercase characters
        for(int i = 1 ; i < n ; i++){
            if(arr[i] >= 65 && arr[i] <= 90 && arr[i-1] >= 65 && arr[i-1] <= 90){
                consecUpperCase = true;
            }
        }

        return numCount >= 1 && specialCharCount >= 1 && consecLowerCase && consecUpperCase;
    }
}
