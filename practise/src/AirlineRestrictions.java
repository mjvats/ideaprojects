import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class AirlineRestrictions {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        String[] input = br.readLine().trim().split(" ");
        String result = func(input);
        bw.write(result);

        br.close();
        bw.close();
    }

    public static String func(String[] arr){
        int A = Integer.parseInt(arr[0]);
        int B = Integer.parseInt(arr[1]);
        int C = Integer.parseInt(arr[2]);

        int D = Integer.parseInt(arr[3]);
        int E = Integer.parseInt(arr[4]);

        if((A+B) <= D && C <= E) return "YES";
        else if((B+C) <= D && A <= E) return "YES";
        else if((A+C) <= D && B <= E) return "YES";
        else return "NO";
    }

}
