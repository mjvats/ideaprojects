import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class CountTriangles {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            int n = Integer.parseInt(br.readLine());
            int[] a = new int[n];
            String[] line1 = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(line1[i]);
            }
            long result = sol(a);
            bw.write(result + "");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    private static long sol(int[] arr) {
        int n = arr.length;
        long ans = 0;
        mergeSort(arr, 0, n - 1);
        for (int k = n - 1; k > 0; k--) {
            int i = k - 1;
            int j = 0;
            while (j < i) {
                if (arr[j] + arr[i] > arr[k]) {
                    ans += i - j;
                    i--;
                } else {
                    j++;
                }
            }
        }
        return ans;
    }

    private static void mergeSort(int[] A, int start, int end) {
        if (start == end) {
            return;
        }
        int mid = (start + end) / 2;
        mergeSort(A, start, mid);
        mergeSort(A, mid + 1, end);
        merge(A, start, end);
    }

    private static void merge(int[] A, int start, int end) {
        int mid = (start + end) / 2;
        int[] L = new int[mid - start + 1];
        int[] R = new int[end - mid];

        int k = 0;
        for (int i = start; i <= mid; i++) {
            L[k++] = A[i];
        }

        k = 0;
        for (int i = mid + 1; i <= end; i++) {
            R[k++] = A[i];
        }

        int i = 0;
        int j = 0;
        k = start;
        while (i < L.length && j < R.length) {
            if (L[i] <= R[j]) {
                A[k++] = L[i++];
            } else {
                A[k++] = R[j++];
            }
        }

        while (i < L.length) {
            A[k++] = L[i++];
        }

        while (j < R.length) {
            A[k++] = R[j++];
        }
    }

}
