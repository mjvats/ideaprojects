import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class KthMaximum {

    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            String[] line1 = br.readLine().trim().split(" ");
            int n = Integer.parseInt(line1[0]);
            int k = Integer.parseInt(line1[1]);

            String[] input = br.readLine().trim().split(" ");
            long[] arr = new long[input.length];
            for (int i = 0; i < input.length; i++)
                arr[i] = Long.parseLong(input[i]);
            int result = kthmax(arr, k);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int kthmax(long arr[], int k)
    {
        long max = Integer.MIN_VALUE;
        for (long val : arr) max = Math.max(max, val);

        int n = arr.length;
        int count = 0;
        for(int i = 0 ; i <= n-k; i++)
        {
            if(arr[i+k-1] == max)
            {
                int j = i+k-1;
                while(j < n)
                {
                    if(j-i+1 >= k)
                    {
                        count++;
                    }
                    j++;
                }
            }
        }
        return count;
    }

}
