package strings;

public class RemoveConsecutiveCharacters {

    public static void main(String[] args) {
        String input = "aaabcccddd";
        String result = solve(input, 2);
        System.out.println(result);
    }

    public static String solve(String A, int B) {

        int N = A.length();
        StringBuffer sb = new StringBuffer();
        StringBuilder res = new StringBuilder();
        sb.append(A.charAt(0));
        int length = 1;
        for (int i = 1; i < N; i++) {
            if ((A.charAt(i) == A.charAt(i - 1))) {
                length++;
                sb.append(A.charAt(i));

            } else {
                if (length != B) {
                    res.append(sb);
                }
                sb = new StringBuffer().append(A.charAt(i));
                length = 1;
            }
        }
        if (length != B) {
            res.append(sb);
        }
        return res.toString();
    }

}
