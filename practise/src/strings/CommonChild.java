package strings;

public class CommonChild {

    public static void main(String[] args) {
        String s1 = "ABDC";
        String s2 = "ABCD";
        int result = commonChild(s1, s2);
        System.out.println(result);
    }

    public static int commonChild(String s1, String s2) {

        int N = s1.length();
        int M = s2.length();

        int[][] m = new int[N+1][M+1];
        for(int i = 1; i <= N ; i++)
        {
            for(int j = 1 ; j <= M ; j++)
            {
                if(s1.charAt(i-1) == s2.charAt(j-1))
                {
                    m[i][j] = m[i-1][j-1]+1;
                }
                else
                {
                    m[i][j] = Integer.max(Integer.max(m[i-1][j], m[i][j-1]), m[i-1][j-1]);
                }
            }
        }
        return m[N][M];

    }
}
