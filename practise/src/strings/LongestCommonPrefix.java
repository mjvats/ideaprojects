package strings;

import java.util.Arrays;
import java.util.List;

public class LongestCommonPrefix {

    public static void main(String[] args) {
        List<String> A = Arrays.asList("abab", "abab", " ");
        String result = longestCommonPrefix(A);
        System.out.println(result);
    }

    public static String longestCommonPrefix(List<String> A) {

        int N = A.size();
        String ref = "";
        int min = Integer.MAX_VALUE;
        int minIndex = -1;
        for (int i = 0; i < N; i++) {
            if (A.get(i).length() <= min) {
                ref = A.get(i);
                min = A.get(i).length();
                minIndex = i;
            }
        }
        A.set(minIndex, null);

        for (int j = 0 ; j < N ; j++) {
            String temp = A.get(j);
            if(temp != null){
                int M = ref.length();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < M; i++) {
                    if (ref.charAt(i) != temp.charAt(i)) {
                        ref = sb.toString();
                        break;
                    } else {
                        sb.append(ref.charAt(i));
                    }
                }
            }
        }
        return ref;
    }
}
