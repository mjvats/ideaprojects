package strings;

public class LengthOfLastWord {

    public static void main(String[] args) {
        String input = "H    ";
        int result = new LengthOfLastWord().lengthOfLastWord(input);
        System.out.println(result);
    }

    public int lengthOfLastWord(final String A) {

        int n = A.length();

        for(int i = n-1; i >= 0 ; i--)
        {
            boolean b = (A.charAt(i) >= 65 && A.charAt(i) <= 90) || (A.charAt(i) >= 97
                && A.charAt(i) <= 122);
            if(b)
            {
                int size = 0;
                while((A.charAt(i) >= 65 && A.charAt(i) <= 90) || (A.charAt(i) >= 97
                    && A.charAt(i) <= 122))
                {
                    size++;
                    if(i > 0) i--;
                    else return size;
                }
                return size;
            }
        }
        return 0;
    }

}
