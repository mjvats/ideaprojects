package strings;

import java.util.HashMap;

public class SherlockAndAnagrams {

    public static void main(String[] args) {
        String input = "ifailuhkqq";
        int result = sherlockAndAnagrams(input);
        System.out.println(result);
    }

    public static int sherlockAndAnagrams(String s) {
        HashMap<String, Integer> map = new HashMap<>();
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = i; j < s.length(); j++) {
                sb.append(s.charAt(j));
                String sortedString = sort(sb.toString());
                if (map.containsKey(sortedString)) {
                    count += map.get(sortedString);
                    map.put(sortedString, map.get(sortedString) + 1);
                } else {
                    map.put(sortedString, 1);
                }
                sb = new StringBuilder(sortedString);
            }
        }
        return count;
    }

    private static String sort(String sb) {
        int N = sb.length();
        char[] chars = sb.toCharArray();
        for (int j = N - 1; j > 0; j--) {
            if (chars[j] >= chars[j - 1]) {
                break;
            }
            char temp = chars[j];
            chars[j] = chars[j - 1];
            chars[j - 1] = temp;
        }
        return String.valueOf(chars);
    }

}
