package strings;

public class TwoCharacters {

    public static void main(String[] args) {
        String input = "hackerworld";
        String result = alternate(input);
        System.out.println(result);
    }

    public static String alternate(String s) {
        int N = s.length();
        String ref = "hackerrank";
        int M = ref.length();

        int i = 0, j = 0;
        int matchCount = 0;
        while (i < N && j < M) {
            if (ref.charAt(j) == s.charAt(i)) {
                i++;
                j++;
                matchCount++;
            } else {
                i++;
            }
            if (matchCount == M) {
                return "YES";
            }
        }
        return "NO";
    }

}
