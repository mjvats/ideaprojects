package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;

public class AnagramsEasy {

    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {

            String[] input = br.readLine().trim().split(" ");
            String a = input[0];
            String b = input[1];

            if (isAnagram(a, b)) {
                bw.write("True");
            } else {
                bw.write("False");
            }
            bw.write("\n");

            T--;
        }

        br.close();
        bw.close();
    }

    public static boolean isAnagram(String a, String b) {
        if (a.length() != b.length()) {
            return false;
        }

        char[] charArrA = a.toCharArray();
        char[] charArrB = b.toCharArray();

        Arrays.sort(charArrA);
        Arrays.sort(charArrB);
        int n = charArrA.length;
        for (int i = 0; i < n; i++) {
            if (charArrA[i] != charArrB[i]) {
                return false;
            }
        }
        return true;
    }


}
