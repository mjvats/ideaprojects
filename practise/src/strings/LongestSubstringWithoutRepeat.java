package strings;

public class LongestSubstringWithoutRepeat {

    public static void main(String[] args) {
        String input = "";
        int result = lengthOfLongestSubstring(input);
        System.out.println(result);
    }

    public static int lengthOfLongestSubstring(String A) {

        int n = A.length();
        int ans = Integer.MIN_VALUE;
        int countA[] = new int[126];
        countA[A.charAt(0)]++;
        for(int i = 0, j = 0 ; i <= j && j < n ;)
        {
            if(isRepeat(countA))
            {
                countA[A.charAt(i)]--;
                i++;
            }
            else
            {
                ans = Math.max(ans, j-i+1);
                j++;
                if(j < n) countA[A.charAt(j)]++;
            }
        }
        return ans;
    }

    public static boolean isRepeat(int count[])
    {
        for(int i = 0 ;i < 126 ; i++) if(count[i] > 1) return true;
        return false;
    }

}
