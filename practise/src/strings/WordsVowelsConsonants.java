package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WordsVowelsConsonants {

    static List<Integer> vowels = Arrays.asList(97, 101, 105, 111, 117, 65, 69, 73, 79, 85);

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {

            String[] input = br.readLine().trim().split(" ");
            int[] result = count(input);
            bw.write(result[0] + " " + result[1] + " " + result[2]);
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int[] count(String[] A) {

        ArrayList<String> list = new ArrayList<>();
        for(String s : A) if(!s.equals("")) list.add(s);

        int N = !list.isEmpty() ? list.size() : 0;
        int v = 0;
        int c = 0;

        for (String s : list) {
            int M = s.length();
            for (int i = 0; i < M; i++) {
                if (isVowel(s.charAt(i))) {
                    v++;
                } else if (isConsonant(s.charAt(i))) {
                    c++;
                }
            }
        }

        int[] result = new int[3];
        result[0] = N;
        result[1] = v;
        result[2] = c;
        return result;
    }

    public static boolean isVowel(char c) {
        for(int val : vowels)
        {
            if (c == val)
            {
                return true;
            }
        }
        return false;
    }

    public static boolean isConsonant(char c) {
        return !isVowel(c) && ((c > 97 && c <= 122) || (c > 65 && c <= 90));
    }

}
