package strings;

public class Pangrams {

    public static void main(String[] args) {
        String input = "We promptly judged antique ivory buckles for the prize";
        String result = pangrams(input);
        System.out.println(result);
    }

    public static String pangrams(String s) {
        // Write your code here
        int N = s.length();
        int[] count = new int[26];
        for(int i = 0 ; i < N ; i++)
        {
            if(s.charAt(i) >= 65 && s.charAt(i) <= 90)
            {
                count[s.charAt(i) - 'A']++;
            }
            else if(s.charAt(i) >= 97 && s.charAt(i) <= 122)
            {
                count[s.charAt(i)-'a']++;
            }
        }

        for(int i = 0 ; i < count.length ; i++)
        {
            if(count[i] == 0)
            {
                return "not pangram";
            }
        }
        return "pangram";
    }

}
