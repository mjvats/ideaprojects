package strings;

public class StrongPassword {

    public static void main(String[] args) {
        int result = minimumNumber(7, "AUzs-nV");
        System.out.println(result);
    }

    public static int minimumNumber(int n, String password) {
        // Return the minimum number of characters to make the password strong
        boolean hasNum = false;
        boolean hasLowerCase = false;
        boolean hasUpperCase = false;
        boolean hasSpecialChar = false;

        char[] charArr = password.toCharArray();
        for(int i = 0 ; i < charArr.length ; i++)
        {
            if(charArr[i] >= 65 && charArr[i] <= 90)
            {
                hasUpperCase = true;
            }
            else if(charArr[i] >= 97 && charArr[i] <= 122)
            {
                hasLowerCase = true;
            }
            else if(charArr[i] >= 48 && charArr[i] <= 57)
            {
                hasNum = true;
            }
            else if((charArr[i] >= 40 && charArr[i] <= 43) || (charArr[i] == 45) || (charArr[i] >= 35 && charArr[i] <= 38) || (charArr[i] == 33) || charArr[i] == 64 || charArr[i] == 94)
            {
                hasSpecialChar = true;
            }
        }

        int count = 0;
        if(!hasNum)
            count++;
        if(!hasUpperCase)
            count++;
        if(!hasLowerCase)
            count++;
        if(!hasSpecialChar)
            count++;

        if((n+count) < 6)
        {
            count = count + (6-(n+count));
        }
        return count;
    }

}
