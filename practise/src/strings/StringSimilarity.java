package strings;

public class StringSimilarity {

    public static void main(String[] args) {
        String input = "aa";
        int result = stringSimilarity(input);
        System.out.println(result);
    }

    public static int stringSimilarity(String s) {
        // Write your code here
        int N = s.length();
        int sum = 0;
        for(int j = 0 ; j < N ; j++)
        {
            int k = j;
            int count = 0;
            for(int i = 0 ; i < N && k < N;)
            {
                if(s.charAt(i) == s.charAt(k))
                {
                    count++;
                    i++;
                    k++;
                }
                else
                {
                    break;
                }
            }
            sum += count;
        }
        return sum;
    }
}
