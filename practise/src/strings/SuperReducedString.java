package strings;

public class SuperReducedString {

    public static void main(String[] args) {
        String s = "bba" ;
        String result = getReducedString(s);
        System.out.println(result);
    }

    public static String getReducedString(String s)
    {
        StringBuilder sb = new StringBuilder(s);

        boolean adjacentMatch = true;
        while(adjacentMatch)
        {
            int count = 0;
            s = sb.toString();
            char[] charArr = s.toCharArray();
            sb = new StringBuilder();
            for(int i = 0 ; i < charArr.length ;)
            {
                if((i == charArr.length-1 && charArr[i] != 42) || (charArr[i] != charArr[i+1]))
                {
                    sb.append(charArr[i]);
                    i = i+1;
                }
                else
                {
                    count++;
                    charArr[i] = '*';
                    charArr[i+1] = '*';
                    i = i+2;
                }

            }
            adjacentMatch = count > 0;
        }
        if(s.isEmpty())
        {
           s = "Empty String";
        }
        return s;
    }
}
