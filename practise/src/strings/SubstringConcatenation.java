package strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubstringConcatenation {

    static int[] countB;

    public static void main(String[] args) {
        String s = "wordgoodgoodgoodbestword";
        String[] words = {"word", "good", "best", "good"};
        final List<String> B = Arrays.asList(words);

        List<Integer> result = new SubstringConcatenation().findSubstring(s, B);
        for (Integer val : result) {
            System.out.print(val + ",");
        }
    }

    public List<Integer> findSubstring(String s, String[] words) {
        StringBuilder sb = new StringBuilder();
        for (String word : words) {
            sb.append(word);
        }

        int m = sb.length();
        SubstringConcatenation.countB = new int[126];
        for (int i = 0; i < m; i++) {
            SubstringConcatenation.countB[sb.charAt(i)]++;
        }

        int countA[] = new int[126];
        for (int i = 0; i < m; i++) {
            countA[s.charAt(i)]++;
        }

        int n = s.length();
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i <= n - m; i++) {
            if (contains(countA, countB)) {
                result.add(i);
            }
            countA[s.charAt(i)]--;
            if ((i + m) < n) {
                countA[s.charAt(i + m)]++;
            }
        }
        return result;
    }

    public List<Integer> findSubstringIndex(String s, String[] words) {
        int wordsArrLen = words.length;
        int wordLen = words[0].length();
        Map<String, Integer> wordMap = new HashMap<>();
        for (String word : words) {
            if (wordMap.containsKey(word)) {
                wordMap.replace(word, wordMap.get(word) + 1);
            } else {
                wordMap.put(word, 1);
            }
        }

        List<Integer> result = new ArrayList<>();
        int strLen = s.length();
        for (int i = 0; i <= strLen - (wordsArrLen * wordLen); i++) {
            String subStr = s.substring(i, i + (wordsArrLen * wordLen));
            int subStrLen = subStr.length();
            Map<String, Integer> map = new HashMap<>();
            for (int j = 0; j < subStrLen; ) {
                String word = subStr.substring(j, j + wordLen);
                if (!wordMap.containsKey(word)) {
                    break;
                } else {
                    if (map.containsKey(word)) {
                        map.replace(word, map.get(word) + 1);
                    } else {
                        map.put(word, 1);
                    }
                }
                j = j + wordLen;
            }
            boolean validSubtring = true;
            for (Map.Entry<String, Integer> entry : wordMap.entrySet()) {
                if (!map.containsKey(entry.getKey()) || !map.get(entry.getKey())
                    .equals(entry.getValue())) {
                    validSubtring = false;
                }
            }
            if (validSubtring) {
                result.add(i);
            }
        }
        return result;
    }

    public ArrayList<Integer> findSubstring(String A, final List<String> B) {
        int wordsArrLen = B.size();
        int wordLen = B.get(0).length();
        Map<String, Integer> wordMap = new HashMap<>();
        for (String word : B) {
            if (wordMap.containsKey(word)) {
                wordMap.replace(word, wordMap.get(word) + 1);
            } else {
                wordMap.put(word, 1);
            }
        }

        ArrayList<Integer> result = new ArrayList<>();
        int strLen = A.length();
        for (int i = 0; i <= strLen - (wordsArrLen * wordLen); i++) {
            String subStr = A.substring(i, i + (wordsArrLen * wordLen));
            int subStrLen = subStr.length();
            Map<String, Integer> map = new HashMap<>();
            for (int j = 0; j < subStrLen; ) {
                String word = subStr.substring(j, j + wordLen);
                if (!wordMap.containsKey(word)) {
                    break;
                } else {
                    if (map.containsKey(word)) {
                        map.replace(word, map.get(word) + 1);
                    } else {
                        map.put(word, 1);
                    }
                }
                j = j + wordLen;
            }
            boolean validSubtring = true;
            for (Map.Entry<String, Integer> entry : wordMap.entrySet()) {
                if (!map.containsKey(entry.getKey()) || !map.get(entry.getKey())
                    .equals(entry.getValue())) {
                    validSubtring = false;
                }
            }
            if (validSubtring) {
                result.add(i);
            }
        }
        return result;
    }

    private boolean contains(int[] countA, int[] countB) {
        for (int i = 0; i < 126; i++) {
            if (countA[i] != countB[i]) {
                return false;
            }
        }
        return true;
    }

}
