package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;

public class ExpressAasPowB {

    static HashMap<Integer, String> map = new HashMap<>();

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {

            int N = Integer.parseInt(br.readLine());
            if ((map.containsKey(N) && map.get(N).equals("Yes")) || func(N)) {
                bw.write("Yes");
                map.put(N, "Yes");
            } else {
                bw.write("No");
                map.put(N, "No");
            }
            bw.write("\n");
            T--;

        }

        br.close();
        bw.close();
    }

    public static int getMod(int a, int n) {
        int remainder = n % a;
        int dividend = 0;
        while (remainder == 0) {
            dividend = n / a;
            remainder = dividend % a;
        }
        return dividend;
    }

    public static boolean func(int n) {
        if(n == 1) return true;

        for(int i = 2; i <= (int)Math.sqrt(n) ; i++)
        {
            double p = Math.log(n)/Math.log(i);
            if((Math.ceil(p) == Math.floor(p)) && p > 1) return true;
        }
        return false;
    }

    private static boolean isPower(int a, int n) {

        int low = 2, high = n / 2;
        while (low <= high) {
            int mid = (low + high) / 2;
            long value = getPower(a, mid);
            if (value == n) {
                return true;
            } else if (value < n) {
                low = ++mid;
            } else {
                high = --mid;
            }
        }
        return false;
    }

    private static long getPower(int a, int b) {
        int bitSize = Integer.toBinaryString(b).length();
        long ans = 1, x = 1;
        int k = 64735492;
        for (int i = 0; i < bitSize; i++) {
            x = i == 0 ? a : (x * x) % k;
            if (((b >> i) & 1) == 1) {
                ans = (ans * x) % k;
            }
        }
        return ans;
    }

}
