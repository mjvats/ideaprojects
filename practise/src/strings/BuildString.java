package strings;

import java.util.HashSet;

public class BuildString {

    public static void main(String[] args) {
        String input = "aabaacaba";
        //int result = mincost(input, 4, 5);
        for(int i = 0 ; i < 1000 ; i++)
            System.out.print("a");
    }

    public static int mincost(String s, int A, int B)
    {
        int N = s.length();
        HashSet<String> set = new HashSet<>();
        for(int i = 0 ; i < s.length() ; i++)
        {
            StringBuilder sb = new StringBuilder();
            for(int j = i ; j < s.length() ; j++)
            {
                sb.append(s.charAt(j));
                String str = String.valueOf(sb);
                if(!set.contains(str))
                {
                    set.add(String.valueOf(str));
                }
            }
        }

        int cost = A;
        for(int i = 1; i < N ; i++)
        {
            if(set.contains(String.valueOf(s.charAt(i))))
            {
                int k = i;
                StringBuilder temp = new StringBuilder(s.charAt(i));
                while(i < N-1 && !set.contains(String.valueOf(temp.append(s.charAt(i+1)))))
                {
                    i++;
                }
                String subStr = temp.substring(k, i);
                if(subStr.length() > 1)
                    cost += B;
                else
                    cost += A;
            }
            else
            {
                cost += A;
            }
        }
        return cost;
    }

}
