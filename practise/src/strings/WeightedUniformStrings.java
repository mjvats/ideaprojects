package strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class WeightedUniformStrings {

    public static void main(String[] args) {
        String input = "aaabbbbcccddd";
        List<Integer> queries = Arrays.asList(9,7,8,12,5);
        List<String> result = weightedUniformStrings(input, queries);
        for (String val : result)
        {
            System.out.println(val);
        }
    }

    public static List<String> weightedUniformStrings(String s, List<Integer> queries) {
        // Write your code here
        int N = s.length();
        HashSet<Integer> set = new HashSet<>();
        int val = s.charAt(0)-'a'+1;
        set.add(val);
        for(int i = 1 ; i < N ; i++)
        {
            if(s.charAt(i) == s.charAt(i-1))
            {
                val += (s.charAt(i)-'a'+1);
            }
            else
            {
                val = (s.charAt(i)-'a'+1);
            }
            set.add(val);
        }

        List<String> result = new ArrayList<>();
        for(Integer value : queries)
        {
            if (set.contains(value))
            {
                result.add("YES");
            }
            else
            {
                result.add("NO");
            }
        }
        return result;
    }
}
