package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class WindowMaximum {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            String[] input = br.readLine().trim().split(" ");
            int N = Integer.parseInt(input[0]);
            int B = Integer.parseInt(input[1]);

            int[] A = new int[N];
            String[] inputArr = br.readLine().trim().split(" ");
            for (int i = 0; i < N; i++) {
                A[i] = Integer.parseInt(inputArr[i]);
            }
            long result = getWindowMaxSum(A, B);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static long getWindowMaxSum(int[] A, int B) {
        int N = A.length;
        int m1 = 0;
        int m2 = 0;
        int m3 = 0;

        for(int i = 0 ; i < B ; i++)
        {
            if(A[i] > m1)
            {
                m3 = m2;
                m2 = m1;
                m1 = A[i];
            }
            else if(A[i] > m2)
            {
                m3 = m2;
                m2 = A[i];
            }
            else if(A[i] > m3)
            {
                m3 = A[i];
            }
        }

        long sum = m1;
        for (int i = 1; i <= N - B; i++) {
            int j = i  + B - 1;

            if (A[i - 1] == m1) {
                m1 = A[j];
            } else if (A[i - 1] == m2) {
                m2 = A[j];
            } else if (A[i - 1] == m3 || A[j] >= m3) {
                m3 = A[j];
            }

            List<Integer> list = Arrays.asList(m1, m2, m3);
            Collections.sort(list);

            m1 = list.get(2);
            m2 = list.get(1);
            m3 = list.get(0);

            sum += m1;
        }

        return sum;
    }

    public static long getWindowMax(int[] A, int B) {
        int N = A.length;
        long sum = 0L;
        int max = getMax(A, 0, B - 1);
        sum += max;
        for (int i = 1; i <= N - B; i++) {
            int j = i + B - 1;
            if (A[i - 1] == max) {
                max = getMax(A, i, j);
            } else if (A[j] >= max) {
                max = A[j];
            }
            sum += max;
        }
        return sum;
    }

    public static int getMax(int[] A, int i, int j) {
        int max = Integer.MIN_VALUE;
        while (i <= j) {
            max = Integer.max(max, A[i++]);
        }
        return max;
    }

}
