package strings;

import java.util.HashMap;
import java.util.Map;

public class SubarrayXOR {

    public static void main(String[] args) {
        int[] arr = {4, 2, 2, 6, 4};
        int m = 6;

        int result = func(arr, m);
        System.out.println(result);
    }

    public static int func(int[] a, int m) {
        int n = a.length;
        int[] xorarr = new int[n];
        xorarr[0] = a[0];
        for (int i = 1; i < n; i++) {
            xorarr[i] = xorarr[i - 1] ^ a[i];
        }

        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            if (map.containsKey(xorarr[i])) {
                map.replace(xorarr[i], map.get(xorarr[i]) + 1);
            } else {
                map.put(xorarr[i], 1);
            }
        }

        int count = 0;
        for (int i = 0; i < n; i++) {
            int temp = xorarr[i]^m;
            count += (map.getOrDefault(temp, 0));
            if (xorarr[i] == m) {
                count++;
            }

            if (map.containsKey(xorarr[i])) {
                map.put(xorarr[i], map.get(xorarr[i]) + 1);
            } else {
                map.put(xorarr[i], 1);
            }
        }
        return count;
    }

}
