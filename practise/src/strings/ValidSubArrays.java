package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

public class ValidSubArrays {

    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            int n = Integer.parseInt(br.readLine());
            int[] a = new int[n];

            String[] input = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(input[i]);
            }

            int result = func(a);
            bw.write(result + "");
            bw.write("\n");

            T--;
        }

        br.close();
        bw.close();
    }

    public static int func(int[] a) {
        int n = a.length;
        //iterate over a to replace all 0 with -1
        for (int i = 0; i < n; i++) {
            if (a[i] == 0) {
                a[i] = -1;
            }
        }

        int count = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                map.put(a[0], 1);
            } else {
                a[i] += a[i - 1];
                if (a[i] == 0) {
                    count++;
                }

                if (map.containsKey(a[i])) {
                    count += map.get(a[i]);
                    map.replace(a[i], map.get(a[i]) + 1);
                } else {
                    map.put(a[i], 1);
                }
            }

        }

        return count;
    }

}
