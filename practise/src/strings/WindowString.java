package strings;

public class WindowString {

    static int[] countB;
    public static void main(String[] args) {
        String S = "ADOBECODEBANCB";
        String T = "ABC";
        String result = new WindowString().minWindow(S, T);
        System.out.println(result);
    }

    public String minWindow(String A, String B) {
        int n = A.length();
        int m = B.length();
        WindowString.countB = new int[126];
        for(int i = 0 ; i < m ; i++) WindowString.countB[B.charAt(i)]++;

        int min_i = -1, min_j = Integer.MAX_VALUE-10;
        int[] countA = new int[126];
        countA[A.charAt(0)]++;
        for(int i = 0, j = 0; i <= j && j < n ;)
        {
            if(contains(countA, B))
            {
                if((j-i+1) < (min_j-min_i+1))
                {
                    min_j = j;
                    min_i = i;
                }
                else if((j-i+1) == (min_j-min_i+1))
                {
                    min_i = Math.min(i, min_i);
                    min_j = Math.min(j, min_j);
                }
                countA[A.charAt(i)]--;
                i++;
            }
            else
            {
                j++;
                if(j < n)
                    countA[A.charAt(j)]++;
            }
        }
        return min_i == -1 ? "" : A.substring(min_i, min_j+1);
    }

    public static boolean contains(int[] count, String B) {
        for (int k = 0; k < 126; k++) {
            if(count[k] < countB[k]) {
                return false;
            }
        }
        return true;
    }

}
