package strings;

public class ReverseString {

    public static void main(String[] args) {
        String input = "qxkpvo  f   w vdg t wqxy ln mbqmtwwbaegx      mskgtlenfnipsl bddjk znhksoewu zwh bd fqecoskmo";
        String result = new ReverseString().solve(input);
        System.out.println(result+":");
    }

    public String solve(String A) {

        String[] arr = A.trim().split(" ");
        int n = arr.length;
        StringBuilder sb = new StringBuilder();
        for(int i = n-1 ; i >= 0 ; i--)
        {
            if(!arr[i].equals(""))sb.append(arr[i]).append(" ");
        }

        return sb.toString().trim();

    }

}
