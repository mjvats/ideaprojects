package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class SumOfPairs {

    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.valueOf(br.readLine());

        while (T > 0) {

            String[] line1 = br.readLine().trim().split(" ");
            int size = Integer.valueOf(line1[0]);
            int K = Integer.valueOf(line1[1]);

            String[] line2 = br.readLine().trim().split(" ");
            int[] A = new int[size];
            for (int i = 0; i < size; i++) {
                A[i] = Integer.valueOf(line2[i]);
            }

            bw.write(String.valueOf(twoSum(A, K)[0]));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int[] twoSum(int[] arr, int target) {
        int n = arr.length;
        mergeSort(arr, 0, n-1);
        int[] result = new int[2];
        for(int i = 0 ; i < n-1; i++){
            int diff = target - arr[i];
            for(int j = i+1 ; j < n ; j++){
                int start = j;
                int end = n-1;
                while(start <= end){
                    int mid = (start + end)/2;
                    if(arr[mid] == diff){
                        result[0] = i;
                        result[1] = mid;
                    } else if (arr[mid] < diff){
                        start = mid+1;
                    } else {
                        end = mid-1;
                    }
                }
            }
        }
        return result;
    }

    static boolean sumOfPairs(int[] A, int K) {
        mergeSort(A, 0, A.length - 1);
        for (int i = 0, j = A.length - 1; i < j;) {
            int sum = A[i] + A[j];
            if (sum == K) {
                return true;
            } else if (sum < K) {
                i++;
            } else {
                j--;
            }
        }
        return false;
    }

    static void mergeSort(int[] A, int start, int end) {
        if (start == end) {
            return;
        }
        int mid = (start + end) / 2;
        mergeSort(A, start, mid);
        mergeSort(A, mid + 1, end);
        merge(A, start, end);
    }

    static void merge(int[] A, int start, int end) {
        int mid = (start + end) / 2;
        int[] L = new int[mid - start + 1];
        int[] R = new int[end - mid];
        int k = 0;
        for (int i = start; i <= mid; i++) {
            L[k++] = A[i];
        }
        k = 0;
        for (int i = mid + 1; i <= end; i++) {
            R[k++] = A[i];
        }

        int i = 0;
        int j = 0;
        k = start;
        while (i < L.length && j < R.length) {
            if (L[i] <= R[j]) {
                A[k++] = L[i];
                i++;
            } else {
                A[k++] = R[j];
                j++;
            }
        }

        while (i < L.length) {
            A[k++] = L[i];
            i++;
        }

        while (j < R.length) {
            A[k++] = R[j];
            j++;
        }
    }
}