package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class EnclosingSubstringTwoPointer {

    static int[] countB ;
    static int[] countA ;

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {

            countA = new int[26];
            countB = new int[26];
            String[] input = br.readLine().trim().split(" ");
            String B = input[0];
            String A = input[1];

            int N = A.length();
            int M = B.length();
            int result = contains(A, N, B, M);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    //Enclosing substring using two pointer.
    private static int contains(String a, int n, String b, int m) {

        for(int i = 0 ; i < m ; i++) countB[b.charAt(i)-'a']++;
        int ans = Integer.MAX_VALUE;

        countA[a.charAt(0)-'a']++;
        for(int i = 0, j = 0 ; i <= j && j < n ; )
        {
            if(contains(countA, b))
            {
                countA[a.charAt(i)-'a']--;
                ans = Integer.min(ans,j-i+1);
                i++;
            }
            else
            {
                j++;
                if(j < n-1) countA[a.charAt(j)-'a']++;
            }
        }
        return ans == Integer.MAX_VALUE ? -1 : ans;
    }

    public static boolean contains(int[] count, String B) {
        int M = B.length();
        for (int k = 0; k < M; k++) {
            if (count[B.charAt(k) - 'a'] < countB[B.charAt(k) - 'a']) {
                return false;
            }
        }
        return true;
    }

}
