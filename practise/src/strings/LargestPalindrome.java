package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class LargestPalindrome {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){

            int N = Integer.parseInt(br.readLine());
            String input = br.readLine();
            int result = largestPalindrome(input, N);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int largestPalindrome(String s, int N)
    {
        N = s.length();
        long[] l2r = new long[N];
        long[] r2l = new long[N];
        int p = 53;
        int k = 1000000007;
        long p1 = p;
        //precomputing left2right
        l2r[0] = (s.charAt(0)*p1)%k;
        for(int i = 1 ; i < N ; i++)
        {
            p1 = (p1*p)%k;
            l2r[i] = (l2r[i-1]+(s.charAt(i)*p1)%k)%k;
        }

        p1 = p;
        //precomputing right2left
        r2l[N-1] = (s.charAt(N-1)*p1)%k;
        for(int i = N-2 ; i >= 0 ; i--)
        {
            p1 = (p1*p)%k;
            r2l[i] = (r2l[i+1] + (s.charAt(i)*p1)%k)%k;
        }

        int ans = 0;
        //For odd palindromes
        for(int i = 0; i < N ; i++)
        {
            int low = 0;
            int high = Integer.min(i, N-i-1);

            while(low <= high)
            {
                int mid = (low + high)/2;
                //if mid == 0, size of largest palindromic should be 1.
                if(mid == 0)
                {
                    ans = Integer.max(ans, 1);
                    low = mid + 1;
                    continue;
                }
                int left = i-mid;
                int right = i+mid;
                long hashA, hashB;
                hashA = left == 0 ? l2r[i-1] : (l2r[i-1] - l2r[left-1]%k+k)%k;
                hashB = right == N-1 ? r2l[i+1] : (r2l[i+1] - r2l[right+1]%k+k)%k;
                int l = i+1;
                int r = N-i;
                if(l < r)
                {
                    hashA = (hashA*getPower(p, r-l))%k;
                }
                else if(r < l)
                {
                    hashB = (hashB*getPower(p, l-r))%k;
                }
                if(hashA == hashB)
                {
                    ans = Integer.max(ans, right-left+1);
                    low = mid+1;
                }
                else
                {
                    high = mid-1;
                }
            }
        }

        //For even palindromes
        for(int i = 0 ; i < N-1 ; i++)
        {
            int j = i+1;
            int low = 0;
            int high = Integer.min(i, N-j-1);
            while (low <= high)
            {
                int mid = (low + high)/2;
                int left = i - mid;
                int right = j + mid;
                long hashA, hashB;
                hashA = left == 0 ? l2r[i]%k : (l2r[i] - l2r[left-1]%k+k)%k;
                hashB = right == N-1 ? r2l[j]%k : (r2l[j] - r2l[right+1]%k+k)%k;
                int l = left;
                int r = N-right-1;
                if(l < r)
                {
                    hashA = (hashA*getPower(p, r-l))%k;
                }
                else if(r < l)
                {
                    hashB = (hashB*getPower(p, l-r)%k);
                }
                if(hashA == hashB)
                {
                    ans = Integer.max(ans, right-left+1);
                    low = mid+1;
                }
                else
                {
                    high = mid-1;
                }
            }
        }
        return ans;
    }

    private static long getPower(int a, int b){
        int bitSize = Integer.toBinaryString(b).length();
        long ans = 1, x = 1;
        int k = 1000000007;
        for(int i = 0 ; i < bitSize ; i++){
            x = i == 0 ? a : (x*x)%k;
            if(((b >> i) & 1) == 1){
                ans = (ans * x)%k;
            }
        }
        return ans;
    }
}