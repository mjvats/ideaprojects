package strings;

public class FunnyString {

    public static void main(String[] args) {
        String input = "bcxz";
        System.out.println(funnyString(input));
    }

    public static String funnyString(String s) {
        // Write your code here
        int N = s.length();
        for(int i = 0, j = N-1; i < N-1 & j > 0 ; i++, j--)
        {
            if(Math.abs(s.charAt(i)-s.charAt(i+1)) != Math.abs(s.charAt(j) - s.charAt(j-1)))
            {
                return "Not Funny";
            }
        }
        return "Funny";
    }

}
