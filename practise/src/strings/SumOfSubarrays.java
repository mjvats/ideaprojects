package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class SumOfSubarrays {

    static long[] inpArr;
    static int k = 1000000007;
    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        inpArr = new long[T];
        String[] input = br.readLine().trim().split(" ");
        inpArr[0] = Long.parseLong(input[0]);
        for(int i = 1 ; i < T ; i++)
        {
            inpArr[i] = (inpArr[i-1] + Long.parseLong(input[i]));
        }

        int Q = Integer.parseInt(br.readLine());
        while (Q > 0)
        {
            String[] query = br.readLine().trim().split(" ");
            int i = Integer.parseInt(query[0]);
            int j = Integer.parseInt(query[1]);

            if(i == 0)
            {
                bw.write(String.valueOf(inpArr[j]));
            }
            else
            {
                bw.write(String.valueOf(inpArr[j]-inpArr[i-1]));
            }
            bw.write("\n");
            Q--;
        }

        br.close();
        bw.close();
    }
}
