package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class EnclosingSubstringBruteForce {

    static int[] countB ;
    static int[] countA ;

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {

            countA = new int[26];
            countB = new int[26];
            String[] input = br.readLine().trim().split(" ");
            String B = input[0];
            String A = input[1];

            int N = A.length();
            int M = B.length();
            for (int i = 0; i < M; i++) {
                countB[B.charAt(i) - 'a']++;
            }
            for (int i = 0; i < M; i++) {
                countA[A.charAt(i) - 'a']++;
            }

            int result = contains(A, N, B, M);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int contains(String A, int N, String B, int M) {
        int ans = Integer.MAX_VALUE;
        for (int i = 0; i <= N - M; i++) {
            int[] countA = new int[26];
            for (int k = 0; k < 26; k++) {
                countA[k] = EnclosingSubstringBruteForce.countA[k];
            }
            if (contains(countA, B)) {
                ans = Integer.min(ans, M);
            }
            if (i < N - M) {
                EnclosingSubstringBruteForce.countA[A.charAt(i) - 'a']--;
                EnclosingSubstringBruteForce.countA[A.charAt(i + M) - 'a']++;
            }
            for (int j = i + M; j < N; j++) {
                countA[A.charAt(j) - 'a']++;
                if (contains(countA, B)) {
                    ans = Integer.min(ans, j - i + 1);
                }
            }
        }
        return ans == Integer.MAX_VALUE ? -1 : ans;
    }

    public static boolean contains(int[] count, String B) {
        int M = B.length();
        for (int k = 0; k < M; k++) {
            if (count[B.charAt(k) - 'a'] < countB[B.charAt(k) - 'a']) {
                return false;
            }
        }
        return true;
    }

    public static boolean contains(String A, int i, int j, String B) {
        int[] countA = new int[26];
        while (i <= j) {
            countA[A.charAt(i) - 'a']++;
            i++;
        }

        int M = B.length();
        for (int k = 0; k < M; k++) {
            if (countA[B.charAt(k) - 'a'] < countB[B.charAt(k) - 'a']) {
                return false;
            }
        }
        return true;
    }

}
