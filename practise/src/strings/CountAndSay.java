package strings;

public class CountAndSay {

    public static void main(String[] args) {
        String result = countAndSay(1);
        System.out.println(result);
    }

    public static String countAndSay(int A)
    {
        A = A-1;
        String s = "1";
        StringBuilder sb = null;
        while (A > 0)
        {
            sb = new StringBuilder();
            int count = 0;
            int i = 0;
            for(; i < s.length() ; i++)
            {
                if(i == 0)
                {
                    count++;
                    continue;
                }
                if(s.charAt(i) == s.charAt(i-1))
                {
                    count++;
                }
                else
                {
                    sb.append(count);
                    sb.append(s.charAt(i-1));
                    count = 1;
                }
            }
            sb.append(count);
            sb.append(s.charAt(i-1));
            s = sb.toString();
            A--;
        }
        return sb != null ? sb.toString() : s;
    }
}
