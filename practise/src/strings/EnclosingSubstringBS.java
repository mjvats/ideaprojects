package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class EnclosingSubstringBS {

    static int[] countB ;
    static int[] countA ;

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {

            countA = new int[26];
            countB = new int[26];
            String[] input = br.readLine().trim().split(" ");
            String B = input[0];
            String A = input[1];

            int N = A.length();
            int M = B.length();
            int result = contains(A, N, B, M);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    //Enclosing substring binary search.
    private static int contains(String a, int n, String b, int m) {

        for(int i = 0 ; i < m ; i++) countB[b.charAt(i)-'a']++;
        int ans = Integer.MAX_VALUE;
        int low = m , high = n;
        while(low <= high)
        {
            int mid = (low + high)/2;
            countA = new int[26];
            for(int i = 0 ; i < mid ; i++) countA[a.charAt(i)-'a']++;
            if(contains(countA, b)) {
                ans = Integer.min(ans, mid);
                high = mid-1;
            }
            else {
                boolean isPresent = false;
                for(int k = 1 ; k <= n-mid ; k++)
                {
                    countA[a.charAt(k-1)-'a']--;
                    countA[a.charAt(k+mid-1)-'a']++;
                    if(contains(countA, b))
                    {
                        ans = Integer.min(ans, mid);
                        isPresent = true;
                        break;
                    }
                }
                if(isPresent) high = mid-1;
                else low = mid+1;
            }
        }
        return ans == Integer.MAX_VALUE ? -1 : ans;
    }

    public static boolean contains(int[] count, String B) {
        int M = B.length();
        for (int k = 0; k < M; k++) {
            if (count[B.charAt(k) - 'a'] < countB[B.charAt(k) - 'a']) {
                return false;
            }
        }
        return true;
    }

}
