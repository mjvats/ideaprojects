package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class LargestPalindromicSubsequence {

    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){

            int N = Integer.parseInt(br.readLine());
            String input = br.readLine();
            int result = largestPalindromicSubsequence(input);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int largestPalindromicSubsequence(String s)
    {
        int ans = 0;
        for(int i = 1; i < s.length()-1 ; i++)
        {
            int right = s.length()-i-1;
            int low = 0, high = Integer.min(i, right);
            while(low <= high)
            {
                int mid = (low + high)/2;
                if(isPalindrome(s, i-(mid), i, i+(mid)))
                {
                    ans = Integer.max(ans, (mid*2)+1);
                    low = mid+1;
                }
                else
                {
                    high = mid-1;
                }
            }
        }
        for(int i = 0; i < s.length()-1 ; i++)
        {
            int j = i+1;
            int right = s.length()-j;
            int low = 0, high = Integer.min(i+1, right);
            while (low <= high)
            {
                int mid = (low+high)/2;
                if(isPalindrome(s, i, mid))
                {
                    ans = Integer.max(ans, mid*2);
                    low = mid+1;
                }
                else
                {
                    high = mid-1;
                }
            }
        }
        return ans;
    }

    public static boolean isPalindrome(String s, int start, int mid, int end)
    {
        int p = 3;
        long p1 = getPower(p, start+1);
        long p2 = getPower(p, s.length()-end);

        long hashA = 0L;
        long hashB = 0L;

        int k = 1000000007;

        for(int i = start, j = end ; i < mid && j > mid ; i++, j--)
        {
            hashA = (hashA + (s.charAt(i)*p1)%k)%k;
            hashB = (hashB + (s.charAt(j)*p2)%k)%k;
            //start++;
            p1 = (p1*p)%k;
            p2 = (p2*p)%k;
        }
        hashB = (hashB*getPower(p, (int) (p2/p1)))%k;
        return hashA == hashB;
    }

    public static boolean isPalindrome(String s, int left, int size)
    {
        int right = left+1;
        int p = 3;
        long p1 = getPower(p, left+1);
        long p2 = getPower(p, s.length()-right);

        long hashA = 0L;
        long hashB = 0L;

        int k = 1000000007;

        for(int i = left-size+1, j = right+size-1 ; i <=left && j >= right ; i++, j--)
        {
            hashA = (hashA + (s.charAt(i)*p1)%k)%k;
            hashB = (hashB + (s.charAt(j)*p2)%k)%k;
            //start++;
            p1 = (p1*p)%k;
            p2 = (p2*p)%k;
        }
        hashB = (hashB*getPower(p, (int) (p2/p1)))%k;
        return hashA == hashB;
    }

    private static long getPower(int a, int b){
        int bitSize = Integer.toBinaryString(b).length();
        long ans = 1, x = 1;
        int k = 64735492;
        for(int i = 0 ; i < bitSize ; i++){
            x = i == 0 ? a : (x*x)%k;
            if(((b >> i) & 1) == 1){
                ans = (ans * x)%k;
            }
        }
        return ans;
    }
}
