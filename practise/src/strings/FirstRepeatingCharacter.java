package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class FirstRepeatingCharacter {

    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {

            String input = br.readLine();
            char result = getFirstRepeatingCharacter(input);
            bw.write(result + "");
            bw.write("\n");

            T--;
        }

        br.close();
        bw.close();
    }

    public static char getFirstRepeatingCharacter(String s)
    {
        int n = s.length();
        int[] count = new int[26];
        for(int i = 0 ; i < n ; i++) count[s.charAt(i)-'a']++;
        for(int i = 0 ; i < n ; i++) if(count[s.charAt(i)-'a'] > 1) return s.charAt(i);

        return '.';
    }
}
