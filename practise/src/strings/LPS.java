package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class LPS {

    static int[] l2r;
    static int[] r2l;
    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){

            int N = Integer.parseInt(br.readLine());
            String input = br.readLine();
            int result = sol(input);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int sol(String s)
    {
        l2r = new int[s.length()];
        r2l = new int[s.length()];

        int p = 3;
        int p1 = p;
        int k = 1000000007;
        l2r[0] = (s.charAt(0)*p1)%k;
        r2l[0] = (s.charAt(s.length()-1)*p1)%k;
        for(int i = 1, j = s.length()-2 ; i < s.length() && j > 0 ; i++, j--)
        {
            l2r[i] = (l2r[i-1] + (s.charAt(i)*p1)%k)%k;
            r2l[i] = (r2l[i-1] + (s.charAt(j)*p1)%k)%k;
            p1 = (p1*p)%k;
        }

        int ans = 0;
        for(int i = 1; i < s.length()-1 ; i++)
        {
            int right = s.length()-i-1;
            int low = 0, high = Integer.min(i, right);
            while(low <= high)
            {
                int mid = (low + high)/2;
                if(mid == 0 || isPalindrome(s, i-mid, i-1, i+1, i+mid))
                {
                    ans = Integer.max(ans, mid*2+1);
                    low = mid+1;
                }
                else
                {
                    high = mid-1;
                }
            }
        }
        for(int i = 0; i < s.length()-1 ; i++) {
            int j = i + 1;
            int right = s.length() - j;
            int low = 0, high = Integer.min(i + 1, right);
            while (low <= high)
            {
                int mid = (low+high)/2;
                if(mid == 0 || isPalindrome(s, i-mid+1, i, j, j+mid-1))
                {
                    ans = Integer.max(ans, mid*2+1);
                    low = mid+1;
                }
                else
                {
                    high = mid-1;
                }
            }
        }
        return ans;
    }

    public static boolean isPalindrome(String s, int i, int j, int k, int l)
    {
        int hashA = 0;
        if(i == 0)
        {
            hashA = l2r[j];
        }
        else
        {
            hashA = l2r[j] - l2r[i-1];
        }

        int hashB = 0;
        if(l == s.length()-1)
        {
            hashB = r2l[s.length()-k-1];
        }
        else
        {
            hashB = r2l[s.length()-k-1] - r2l[s.length()-l-2];
        }

        return (hashA == hashB);
    }

}
