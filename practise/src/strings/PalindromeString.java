package strings;

import java.util.ArrayList;

public class PalindromeString {

    public static void main(String[] args) {
        String input = "A man, a plan, a canal: Panama";
        int result = isPalindrome(input);
        System.out.println(result);
    }

    public static int isPalindrome(String A) {
        ArrayList<Character> charList = new ArrayList<>();
        for(int i = 0 ; i < A.length() ; i++)
        {
            if((A.charAt(i) >= 65 && A.charAt(i) <= 90) || (A.charAt(i) >= 97 && A.charAt(i) <= 122) || (A.charAt(i) >= 48 && A.charAt(i) <= 57))
            {
                charList.add(A.charAt(i));
            }
        }

        boolean isPalindrome = true;
        for(int i = 0, j = charList.size()-1 ; i <= j ; i++, j--)
        {
            if(!((charList.get(i) == charList.get(j)) || (charList.get(i) == charList.get(j)-32) || (charList.get(i) == charList.get(j)+32)))
            {
                isPalindrome = false;
                break;
            }
        }
        if(isPalindrome)
        {
            return 1;
        }
        return 0;
    }

}
