package strings;

public class VowelConsonent {

    public static void main(String[] args) {
        String input = "inttnikjmjbemrberk";
        int result = getCount(input);
        System.out.println(result);
    }

    public static int getCount(String A) {
        int N = A.length();
        int v = 0, c = 0;
        int ans = 0;
        for (int i = 0; i < N; i++) {
            char temp = A.charAt(i);
            if (isVowel(temp)) {
                v++;
                ans += c;
            } else if (isConsonent(temp)) {
                c++;
                ans += v;
            }
        }
        return ans % 1000000007;
    }

    public static boolean isVowel(char c) {
        return (c == 97 || c == 101 || c == 105 || c == 111 || c == 117);
    }

    public static boolean isConsonent(char c) {
        return !isVowel(c) && (c > 97 && c <= 122);
    }

}
