package strings;

public class CheckPalindrome {

    public static void main(String[] args) {
        String input = "ababccc";
        int result = new CheckPalindrome().solve(input);
        System.out.println(result);
    }

    public int solve(String A) {

        int n = A.length();
        int[] count = new int[26];

        for(int i = 0 ; i < n ; i++) count[A.charAt(i)-'a']++;

        int evencount = 0, oddcount = 0;

        for(int i = 0 ; i < 26 ; i++)
        {
            if(count[i]%2 == 0) evencount++;
            else oddcount++;
        }

        if(oddcount == 0 || oddcount == 1) return 1;
        else return 0;
    }

}
