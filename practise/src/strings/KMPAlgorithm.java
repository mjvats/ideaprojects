package strings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class KMPAlgorithm {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {

            String[] input = br.readLine().trim().split(" ");
            String A = input[0];
            String B = input[1];

            int result = getCount(A, B);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int getCount(String A, String B) {
        int count = 0;
        long hashA = 0;
        long hashB = 0;

        long p = 53;
        long p1 = p;
        int k = 1000000007;
        int i = 0;
        for (; i < A.length(); i++) {
            hashA = (hashA + (A.charAt(i) * p1) % k) % k;
            hashB = (hashB + (B.charAt(i) * p1) % k) % k;
            p1 = (p1 * p) % k;
        }
        long p2 = p;
        if (hashA == hashB) {
            count++;
        }
        for (; i < B.length(); i++) {
            hashB = (hashB + (B.charAt(i) * p1) % k) % k;
            hashB = (hashB - k + (B.charAt(i - A.length()) * p2) % k) % k;
            p1 = (p1 * p) % k;
            p2 = (p2 * p) % k;
            hashA = (hashA * p) % k;
            if (hashA == hashB) {
                count++;
            }
        }
        return count;
    }
}