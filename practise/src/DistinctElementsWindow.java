import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DistinctElementsWindow {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {

            String[] line1 = br.readLine().trim().split(" ");
            int n = Integer.parseInt(line1[0]);
            int k = Integer.parseInt(line1[1]);

            String[] line2 = br.readLine().trim().split(" ");
            int[] a = new int[n];
            for(int i = 0 ; i < n ; i++) a[i] = Integer.parseInt(line2[i]);

            List<Integer> result = getDistinctElement(a, k);
            for(Integer res : result) bw.write(String.valueOf(res)+" ");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static List<Integer> getDistinctElement(int[] a, int k) {
        HashMap<Integer, Integer> map = new HashMap<>();
        ArrayList<Integer> result = new ArrayList<>();

        for(int i = 0 ; i < k ; i++)
        {
            if(map.containsKey(a[i]))
            {
                map.put(a[i], map.get(a[i])+1);
            }
            else
            {
                map.put(a[i], 1);
            }
        }

        int count = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet())
        {
            if(entry.getValue() > 0)
            {
                count++;
            }
        }
        result.add(count);

        int n = a.length;
        for(int i = 1; i <= n-k ; i++)
        {
            if(map.containsKey(a[i-1])) map.replace(a[i-1], map.get(a[i-1])-1);
            if(map.containsKey(a[i+k-1]))
            {
                map.put(a[i+k-1], map.get(a[i+k-1])+1);
            }
            else
            {
                map.put(a[i+k-1], 1);
            }
            count = 0;
            for (Map.Entry<Integer, Integer> entry : map.entrySet())
            {
                if(entry.getValue() > 0)
                {
                    count++;
                }
            }
            result.add(count);
        }
        return result;
    }

}
