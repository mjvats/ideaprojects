import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class PenaltyShots {

    public static void main(String[] args) throws Exception
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());

        while(T > 0){

            String[] input = br.readLine().trim().split(" ");
            int[] A = new int[input.length];
            for(int i = 0 ; i < input.length ; i++)
            {
                A[i] = Integer.parseInt(input[i]);
            }
            int result = getWinner(A);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;

        }

        br.close();
        bw.close();

    }

    public static int getWinner(int[] A)
    {
        int countA = 0, countB = 0;
        for(int i = 0; i < A.length ; i++)
        {
            if(A[i] == 1)
            {
                if((i+1)%2 == 0)
                    countB++;
                else
                    countA++;
            }
        }
        if(countA > countB)
        {
            return 1;
        }
        else if (countB > countA)
        {
            return 2;
        }
        else
        {
            return 0;
        }
    }

}
