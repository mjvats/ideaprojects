package multilayeredcache;

import multilayeredcache.cache.Cache;
import multilayeredcache.kvcache.KVCache;
import multilayeredcache.queuecache.QueueCache;
import multilayeredcache.stackcache.StackCache;

public class CustomCache {

    Cache cache;

    public CustomCache(){
        cache = null;
    }

    public Cache getCache(String param){
        if(param.equalsIgnoreCase("fifo")){
            cache = new QueueCache();
        } else if(param.equalsIgnoreCase("lifo")){
            cache = new StackCache();
        } else if(param.equalsIgnoreCase("kv")){
            cache = new KVCache();
        }
        //scheduleEvictionTask();
        return cache;
    }


    public boolean clearCache(){
        //code to clear this.cache;
        return true;
    }

}
