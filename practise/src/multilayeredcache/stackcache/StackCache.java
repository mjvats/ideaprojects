package multilayeredcache.stackcache;

import multilayeredcache.cache.Cache;

public class StackCache implements Cache {

    @Override
    public boolean insert(Object data) {
        return false;
    }

    @Override
    public boolean insert(Object data, int millisec) {
        return false;
    }

    @Override
    public void scheduleEvictionTask() {

    }
}
