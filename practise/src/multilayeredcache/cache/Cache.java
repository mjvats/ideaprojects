package multilayeredcache.cache;

public interface Cache<T> {

    public boolean insert(T data);

    public boolean insert(T data, int millisec);

    public T getData(T key);

    public boolean remove(T key);

    public void scheduleEvictionTask();

}
