package multilayeredcache.evictionTask;

import java.util.TimerTask;

public abstract class EvictionTask extends TimerTask {

    public abstract boolean evict();
}
