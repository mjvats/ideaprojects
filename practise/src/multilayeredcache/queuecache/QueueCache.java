package multilayeredcache.queuecache;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import multilayeredcache.cache.Cache;

public class QueueCache<T> implements Cache {

    Queue<Object> queue;
    Queue<Object> squeue;
    Queue<Object> tempQueue;
    Map<Object, Object> map = new HashMap<>();
    int defaultExpiryTime;


    QueueCache(){
        this.queue = new LinkedList<>();
        this.squeue = new LinkedList<>();
        this.defaultExpiryTime = 1000;//milliseconds
    }

    @Override
    public boolean insert(Object data) {
        boolean result = false;
        if(map.containsKey(data)){
            queue.remove(data);
            result = queue.add(data);
        } else {
            if(queue.add(data)){
                map.put(data, defaultExpiryTime);
                result = true;
            }
        }
        return result;
    }

    @Override
    public boolean insert(Object data, int millisec) {
        boolean result = false;
        if(map.containsKey(data)){
            queue.remove(data);
            result = queue.add(data);
        } else {
            if(queue.add(data)){
                map.put(data, millisec);
                result = true;
            }
        }
        return result;
    }

    @Override
    public Object getData(Object key) {
        if(map.containsKey(key)){
            while (!queue.isEmpty()){
                Object data = queue.poll();
                if(data)
            }
        }
        return null;
    }

    @Override
    public boolean remove(Object key) {
        return false;
    }

    @Override
    public void scheduleEvictionTask() {

    }
}
