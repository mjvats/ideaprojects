import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class CollectingWater {

    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            int N = Integer.parseInt(br.readLine());
            String[] input = br.readLine().trim().split(" ");
            int[] arr = new int[N];
            for (int i = 0; i < N; i++)
                arr[i] = Integer.parseInt(input[i]);
            int result = func(arr);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;
        }
        br.close();
        bw.close();
    }
    public static int func(int a[])
    {
        int n = a.length;
        int l2r[] = new int[n];
        int r2l[] = new  int[n];

        l2r[0] = a[0];
        for(int i = 1; i < n ; i++) l2r[i] = Math.max(l2r[i-1], a[i]);

        r2l[n-1] = a[n-1];
        for(int i = n-2; i >= 0 ; i--) r2l[i] = Math.max(r2l[i+1], a[i]);

        int ans = 0;
        for(int i = 1; i < n-1 ; i++) ans += ((Math.min(l2r[i], r2l[i]))-a[i]);

        return ans;
    }

    public static int func(final List<Integer> A)
    {
        int n = A.size();
        int[] l2r = new int[n];
        int[] r2l = new  int[n];

        l2r[0] = A.get(0);
        for(int i = 1; i < n ; i++) l2r[i] = Math.max(l2r[i-1], A.get(i));

        r2l[n-1] = A.get(n-1);
        for(int i = n-2; i >= 0 ; i--) r2l[i] = Math.max(r2l[i+1], A.get(i));

        int ans = 0;
        for(int i = 1; i < n-1 ; i++) ans += ((Math.min(l2r[i], r2l[i]))-A.get(i));

        return ans;
    }

}
