public class CounterGame {

    public static void main(String[] args) {
        String result = counterGame(132);
        System.out.println(result);
    }

    public static String counterGame(long n) {
        // Write your code here
        boolean flag = true;
        while(true){
            int msb_index = 0;
            boolean isPowerOfTwo = false;
            int i = 63;
            for(; i >= 0 ; i--){
                if(((n >> i) & 1L)==1){
                    if(msb_index == 0)
                        msb_index = i;
                    else{
                        isPowerOfTwo = false;
                        break;
                    }
                }
                isPowerOfTwo = true;
            }
            if(isPowerOfTwo){
                n = n/2;
            }
            else{
                n = n - (1L << ++i);
            }
            flag = !flag;
            if(n == 1){
                break;
            }
        }

        if(!flag){
            return "Louise";
        }
        else{
            return "Richard";
        }
    }

}
