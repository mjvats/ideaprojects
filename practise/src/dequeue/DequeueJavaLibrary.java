package dequeue;

import java.util.ArrayDeque;
import java.util.Deque;

public class DequeueJavaLibrary {

    public static void main(String[] args) {

        Deque<Integer> d = new ArrayDeque<>();
        d.addFirst(1);
        d.addFirst(2);
        d.addLast(3);
        d.addLast(4);
        System.out.println(d.getFirst());
    }

}
