package dequeue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class WindowMaximum {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            String[] input = br.readLine().trim().split(" ");
            int n = Integer.parseInt(input[0]);
            int k = Integer.parseInt(input[1]);

            int[] arr = new int[n];
            String[] line2 = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                arr[i] = Integer.parseInt(line2[i]);
            }
            long result = windowMaximum(arr, k);
            bw.write(result + "");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static long windowMaximum(int[] arr, int k) {
        int n = arr.length;
        int[] temp = new int[n]; // temp array for storing k elements in descending order.

        int i = 0, j = 0, f = -1, b = -1; // i and j will be pointers on input arr. f and b are pointers on temp array
        for (; j < k; j++) {
            if (f == -1 && b == -1) { //if temp array is empty, move both f and b to 0. Else move only b.
                f++;
            }
            if (j != 0) {
                while (b >= f && (temp[b] < arr[j])) { // remove all numbers in temp which are less than incoming element from arr to maintain desc order of elements.
                    b--;
                }
            }
            temp[++b] = arr[j]; // store jth element of input array at temp[b].
        }
        long sum = temp[f]; //once the first k elements are stored in descending order in temp array, start taking sum of max. Since temp is in desc order, max will always be at front. Hence f will point to max.
        for (; j < n; j++, i++) { //after inserting first k elements of arr in temp, start inserting other elements one by one and remove one element. j will point to element to be added and i will point to element to be removed.
            while (b >= f && (temp[b] < arr[j])) { // remove all numbers in temp which are less than incoming element from arr to maintain desc order of elements.
                b--;
            }
            temp[++b] = arr[j];
            if ((arr[i] == temp[f]) && (f < b)) { // if element to be removed equals current max, increment f which is always pointing to max.
                f++;
            }
            sum += temp[f]; // add max to sum. f will always point to max in current window.
        }
        return sum; // return final sum.
    }

    public ArrayList<Integer> slidingMaximum(final List<Integer> A, int B) {
        int n = A.size();
        int[] temp = new int[n]; // temp array for storing k elements in descending order.

        int i = 0, j = 0, f = -1, b = -1; // i and j will be pointers on input arr. f and b are pointers on temp array
        for (; j < B; j++) {
            if (f == -1 && b == -1) { //if temp array is empty, move both f and b to 0. Else move only b.
                f++;
            }
            if (j != 0) {
                while (b >= f && (temp[b] < A.get(j))) { // remove all numbers in temp which are less than incoming element from arr to maintain desc order of elements.
                    b--;
                }
            }
            temp[++b] = A.get(j); // store jth element of input array at temp[b].
        }

        ArrayList<Integer> result = new ArrayList<>();
        //int sum = temp[f]; //once the first k elements are stored in descending order in temp array, start taking sum of max. Since temp is in desc order, max will always be at front. Hence f will point to max.
        result.add(temp[f]);
        for (; j < n; j++, i++) { //after inserting first k elements of arr in temp, start inserting other elements one by one and remove one element. j will point to element to be added and i will point to element to be removed.
            while (b >= f && (temp[b] < A.get(j))) { // remove all numbers in temp which are less than incoming element from arr to maintain desc order of elements.
                b--;
            }
            temp[++b] = A.get(j);
            if ((A.get(i) == temp[f]) && (f < b)) { // if element to be removed equals current max, increment f which is always pointing to max.
                f++;
            }
            result.add(temp[f]); // add max to sum. f will always point to max in current window.
        }
        return result; // return final sum.
    }
}
