package dequeue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class DequeueDemo {

    static int[] arr;
    static int f, b, N;

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        arr = new int[T * 2];
        N = 2 * T; //Initializing array size = twice the test case size.
        f = -1; // pointer for tracking elements from front
        b = -1; // pointer for tracking elements from back
        while (T > 0) {
            String[] input = br.readLine().trim().split(" ");
            String operation = input[0];
            if (operation.equalsIgnoreCase("push_back")) {
                push_back(Integer.parseInt(input[1]));
            } else if (operation.equalsIgnoreCase("push_front")) {
                push_front(Integer.parseInt(input[1]));
            } else if (operation.equalsIgnoreCase("pop_back")) {
                bw.write(pop_back());
                bw.write("\n");
            } else if (operation.equalsIgnoreCase("pop_front")) {
                bw.write(pop_front());
                bw.write("\n");
            }
            T--;
        }

        br.close();
        bw.close();
    }

    static void push_back(int a) {
        if (isEmpty()) {
            f++;
            b++;
        } else {
            b++;
            if (b > N - 1) {
                b = 0;
            }
        }
        arr[b] = a;
    }

    static void push_front(int a) {
        if (isEmpty()) {
            b++;
            f++;
        } else {
            f = f - 1;
            if (f < 0) {
                f = N - 1;
            }
        }
        arr[f] = a;
    }

    static String pop_back() {
        if (isEmpty()) {
            return "Empty";
        } else {
            int val = arr[b];
            if (b == f) {
                b = -1;
                f = -1;
            } else {
                b = b - 1;
                if (b == -1) {
                    b = N - 1;
                }
            }
            return String.valueOf(val);
        }
    }

    static String peek_back(){
        if(isEmpty()){
            return "Empty";
        }else{
            return String.valueOf(arr[b]);
        }
    }

    static String pop_front() {
        if (isEmpty()) {
            return "Empty";
        } else {
            int val = arr[f];
            if (b == f) {
                b = -1;
                f = -1;
            } else {
                f = f + 1;
                if (f > N - 1) {
                    f = 0;
                }
            }
            return String.valueOf(val);
        }
    }

    static String peek_front(){
        if(isEmpty()){
            return "Empty";
        }else{
            return String.valueOf(arr[f]);
        }
    }

    static boolean isEmpty() {
        return f == -1 && b == -1;
    }

}
