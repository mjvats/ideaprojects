import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ProgrammingLanguages {

    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0)
        {
            String[] input = br.readLine().trim().split(" ");
            int[] arr = new int[input.length];
            for(int i = 0 ; i < input.length ; i++) arr[i] = Integer.parseInt(input[i]);
            int result = getLanguage(arr);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int getLanguage(int arr[])
    {
        int A = arr[0];
        int B = arr[1];

        if((arr[2] == A && arr[3] == B) || (arr[2] == B && arr[3] == A)) return 1;
        else if((arr[4] == A && arr[5] == B) || (arr[4] == B && arr[5] == A)) return 2;
        else return 0;
    }

}
