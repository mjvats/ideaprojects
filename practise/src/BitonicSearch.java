import java.util.Arrays;
import java.util.List;

public class BitonicSearch {

    public static void main(String[] args) {

        List<Integer> A = Arrays.asList(3, 9, 10, 20, 17, 5, 1);
        int result = solve(A, 2);
        System.out.println(result);
    }

    public static int solve(List<Integer> A, int B) {

        int bitonicIndex = findBitonicPoint(A);
        if(bitonicIndex < 0 || A.get(bitonicIndex) < B)
        {
            return -1;
        }
        else if(A.get(bitonicIndex) == B)
        {
            return bitonicIndex;
        }
        else
        {
            int searchLeftOfBitonic = binarySearchLeftOfBitonic(A, B, 0, bitonicIndex);
            if(searchLeftOfBitonic > -1)
            {
                return searchLeftOfBitonic;
            }
            else
            {
                int searchRightOfBitonic = binarySearchRightOfBitonic(A, B, bitonicIndex+1, A.size()-1);
                if(searchRightOfBitonic > -1)
                {
                    return searchRightOfBitonic;
                }
            }
        }
        return -1;

    }

    private static int findBitonicPoint(List<Integer> A)
    {
        int start = 0, end = A.size()-1, mid = 0;
        while (start <= end)
        {
            mid = (start + end)/2;
            if(A.get(mid-1) < A.get(mid) && A.get(mid+1) < A.get(mid))
            {
                return mid;
            }
            else if (A.get(mid-1) < A.get(mid) && A.get(mid+1) > A.get(mid))
            {
                start = mid+1;
            }
            else {
                end = mid-1;
            }
        }
        return -1;
    }

    private static int binarySearchLeftOfBitonic(List<Integer> a, int b, int start, int end){
        if(start > end)
        {
            return -1;
        }
        else{
            int mid = (start + end)/2;
            if(a.get(mid) == b){
                return mid;
            }
            else if(a.get(mid) < b){
                return binarySearchLeftOfBitonic(a, b, mid+1, end);
            }
            else{
                return binarySearchLeftOfBitonic(a, b, start, mid-1);
            }
        }
    }

    private static int binarySearchRightOfBitonic(List<Integer> a, int b, int start, int end){
        if(start > end)
        {
            return -1;
        }
        else{
            int mid = (start + end)/2;
            if(a.get(mid) == b){
                return mid;
            }
            else if(a.get(mid) > b){
                return binarySearchRightOfBitonic(a, b, mid+1, end);
            }
            else{
                return binarySearchRightOfBitonic(a, b, start, mid-1);
            }
        }
    }
}
