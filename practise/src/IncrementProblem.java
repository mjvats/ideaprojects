import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class IncrementProblem {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 2, 3, 1, 4, 2, 1, 3);
        List<Integer> result = new IncrementProblem().solve(list);
        for (int val : result) {
            System.out.print(val+" ");
        }
    }

    public List<Integer> solve(List<Integer> A) {

        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < A.size(); i++) {
            if (!map.containsKey(A.get(i))) {
                map.put(A.get(i), i);
            } else {
                int firstOccurance = map.get(A.get(i));
                A.set(firstOccurance, A.get(firstOccurance) + 1);
            }
        }
        return A;
    }

}
