import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class CarsAndBikes {

    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            int n = Integer.parseInt(br.readLine());
            bw.write(sol(n));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();

    }

    public static String sol(int n){
        if(n%4 >= 2) return "YES";
        return "NO";
    }

}
