import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class LargestConcatenatedNumber {

    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0)
        {
            int n = Integer.parseInt(br.readLine());
            int[] a = new int[n];
            String[] input = br.readLine().trim().split(" ");
            for(int i = 0 ; i < n ; i++) a[i] = Integer.parseInt(input[i]);
            sol(a);
            StringBuilder sb = new StringBuilder();
            for(int i = 0 ; i < n ; i++) sb.append(a[i]);
            bw.write(sb.toString());
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static void sol(int[] arr)
    {
        mergeSort(arr, 0, arr.length-1);
    }

    public static void mergeSort(int[] arr, int start, int end)
    {
        if (start == end) return;
        int mid = (start + end) / 2;
        mergeSort(arr, start, mid);
        mergeSort(arr, mid + 1, end);
        merge(arr, start, end);
    }

    public static void merge(int[] arr, int start, int end)
    {
        int mid = (start + end) / 2;
        int[] L = new int[mid - start + 1];
        int[] R = new int[end - mid];
        int k = start;
        for (int i = 0; i < L.length; i++, k++) L[i] = arr[k];
        for(int i = 0 ; i < R.length ; i++, k++) R[i] = arr[k];
        int i = 0, j = 0;
        k = start;
        while (i < L.length && j < R.length)
        {
            if (compare(L[i], R[j]) == L[i])
            {
                arr[k] = L[i];
                k++;
                i++;
            }
            else
            {
                arr[k] = R[j];
                k++;
                j++;
            }
        }

        while (i < L.length)
        {
            arr[k] = L[i];
            k++;
            i++;
        }
        while (j < R.length)
        {
            arr[k] = R[j];
            k++;
            j++;
        }
    }

    public static int compare(int a, int b)
    {
        int lenA = String.valueOf(a).length();
        int lenB = String.valueOf(b).length();

        int valA = (int) (a*(Math.pow(10, lenB))+b);
        int valB = (int)(b*(Math.pow(10, lenA))+a);
        if(valA < valB) return b;
        else return a;
    }

}
