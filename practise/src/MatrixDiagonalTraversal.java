public class MatrixDiagonalTraversal {

    public static void main(String[] args) {
        int[][] m = {{1},{2},{3}};
        traverse(m);
    }

    private static void traverse(int[][] m){
        if(m.length == 0)
            return;

        int x = m.length-1, y = m[0].length-1;
        //System.out.print(m[0][0] + " ");
        int i = 0 , j = 0;

        while(true){

            while(i >= 0 && j <= y){
                System.out.print(m[i][j] + " ");
                i--;
                j++;
            }
            if(j <= y){
                i++;
            }
            else{
                i = i+2; j--;
            }

            while (j >= 0 && i <= x){
                System.out.print(m[i][j] + " ");
                i++;j--;
            }
            if(i <= x){
                j++;
            }
            else{
                i--;
                j = j+2;
            }

            if(i >= x && j >= y){
                if(i == x && j == y){
                    System.out.print(m[i][j] + " ");
                }
                return;
            }
        }
    }

}
