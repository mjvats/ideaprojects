public class PermutationOfSizeK {

    public static void main(String[] args) {
        int N = 6;
        int k = 3;
        int[] A = {3, 10, -1, 8, 2, -5};
        boolean[] usd = new boolean[6];
        int[] cmb = new int[k];

        func(A, cmb, usd, N, k, 0, 0);
    }

    public static void func(int[] A, int[] cmb, boolean[] usd, int N, int k, int aidx, int cidx){
        if(cidx == k){
            for(int val : cmb){
                System.out.print(val+" ");
            }
            System.out.println();
            return;
        }

        for(int i = aidx ; i < N ; i++){
            if(!usd[i]){
                usd[i] = true;
                cmb[cidx] = A[i];
                func(A, cmb, usd, N, k, i+1, cidx+1);
                usd[i] = false;
             }
        }
    }

}
