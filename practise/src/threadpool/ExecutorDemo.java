package threadpool;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorDemo {

    public static void main(String args[]){
        PrintJob[] jobs = {new PrintJob("Durga"), new PrintJob("Ravi"),
        new PrintJob("Shiva"), new PrintJob("Pawan"), new PrintJob("Suresh")};

        ExecutorService service = Executors.newFixedThreadPool(3);
        for(PrintJob job : jobs){
            Future<String> f = service.submit(job);
            try{
                System.out.println(f.get().length());
            }
            catch (Exception e){

            }
        }

        service.shutdown();

        //Callable<String> c = new PrintJob("Durga");
    }

}
