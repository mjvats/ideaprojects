import java.io.InputStreamReader;

public class FindingNumber {

    public static void main(String[] args) {
        int[] arr = {1, 3, 4, 6, 7, 9, 10};
        int index = getIndex(arr, 7);
        if(index != -1){
            System.out.println();
        } else {

        }
    }

    public static int getIndex(int[] arr, int target){
        int index = -1;

        int low = 0;
        int high = arr.length;

        while (low <= high){
            int mid = (low/2 + high/2);
            if(arr[mid] == target){
                index = mid;
                break;
            } else if(arr[mid] < target){
                high = mid-1;
            } else {
                low = mid+1;
            }
        }

        return index;
    }

}
