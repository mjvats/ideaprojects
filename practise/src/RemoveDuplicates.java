public class RemoveDuplicates {

    public static void main(String[] args) {
        int[] input = {1,1,2};
        int length = new RemoveDuplicates().removeDuplicates(input);
        for(int i = 0 ; i < length ; i++){
            System.out.print(input[i] + "\0");
        }
    }

    public int removeDuplicates(int[] nums) {

        if(!validateInput(nums))
            return 0;
        else{
            int maxNum = nums[nums.length-1];
            if(nums[0] == maxNum){
                return 1;
            }
            int i = 1, j = 0;
            boolean allUnique = true;
            while(i < nums.length){
                if(nums[i] < -10000 || nums[i] > 10000)
                    return 0;
                else if(nums[i] != nums[i-1]){
                    i++;
                    continue;
                }
                else{
                    allUnique = false;
                    j = i+1;
                    while(i < nums.length){
                        if(nums[i] == maxNum){
                            return i;
                        }
                        while(j < nums.length){
                            if(nums[j] != nums[i] && nums[j] != nums[i-1]){
                                nums[i] = nums[j++];
                                break;
                            }else{
                                j++;
                            }
                        }
                        if(nums[i] == maxNum){
                            return i+1;
                        }
                        i++;
                    }
                }
            }
            if(allUnique)
                return i;
            else
                return i+1;
        }
    }

    private boolean validateInput(int[] nums){
        if(nums.length == 0 || nums.length > 30000){
            return false;
        }
        return true;
    }

}
