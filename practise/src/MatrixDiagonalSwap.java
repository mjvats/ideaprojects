import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MatrixDiagonalSwap {

    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.valueOf(br.readLine());
        int count = 0;
        while (T > 0) {
            int size = Integer.valueOf(br.readLine());
            int M[][] = new int[size][size];

            int rowCount = 0;
            while (size > 0) {
                String[] input = br.readLine().trim().split(" ");
                for (int i = 0; i < input.length; i++) {
                    M[rowCount][i] = Integer.valueOf(input[i]);
                }
                rowCount++;
                size--;
            }
            bw.write("Test Case #" + (++count) + ":\n");
            rotateMatrix(M);
            for (int i = 0; i < M.length; i++) {
                for (int j = 0; j < M[0].length; j++) {
                    bw.write(M[i][j] + " ");
                }
                bw.write("\n");
            }
            T--;
        }
        br.close();
        bw.close();
    }

    static private void rotateMatrix(int[][] M) {

        //Swapping horizontal columns. Ex - in a 4*4 matrix, swap col1 with col4 and col2 aith col3
        for (int j = 0, k = M[0].length - 1; j < k; j++, k--) {
            for (int i = 0; i < M.length; i++) {
                M[i][j] = M[i][j] ^ M[i][k];
                M[i][k] = M[i][j] ^ M[i][k];
                M[i][j] = M[i][j] ^ M[i][k];
            }
        }

        //Swapping diagonally.
        for (int x = 0, y = M[0].length - 1, m = 0, n = M[0].length - 1; y >= 0 && m < M[0].length;
            y--, m++) {
            diagonalSwap(M, x, y, m, n);
        }

        for (int x = 1, y = 0, m = M.length - 1, n = M[0].length - 2; x < M.length && n >= 0;
            x++, n--) {
            diagonalSwap(M, x, y, m, n);
        }

    }

    static private void diagonalSwap(int[][] M, int x, int y, int m, int n) {
        while (true) {
            if (m-x == 0 && n-y == 0) {
                return;
            }
            M[x][y] = M[x][y] ^ M[m][n];
            M[m][n] = M[x][y] ^ M[m][n];
            M[x][y] = M[x][y] ^ M[m][n];

            if (m - x == 1 && n - y == 1) {
                return;
            } else {
                x++;
                y++;
                m--;
                n--;
            }
        }
    }

}
