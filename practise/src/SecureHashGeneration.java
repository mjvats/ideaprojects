import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import sun.misc.BASE64Encoder;

public class SecureHashGeneration {

    public static void main(int[] args) throws NoSuchAlgorithmException {
        String originalString = "https://dev10.ezetap.com/api/2.0/payment/status";
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedhash = digest.digest(
            originalString.getBytes(StandardCharsets.UTF_8));
        String byteToHex = bytesToHex(encodedhash);
        System.out.println(byteToHex);
    }

    public static void main(String[] args) throws NoSuchAlgorithmException, IOException {
        FileInputStream fr = new FileInputStream("/Users/ezetap/Documents/GitLab/cnpware/src/main/java/com/ezetap/cnpware/pg/adapter/cybersource/CyberSourcePgAdapter.java");
        InputStreamReader isr = new InputStreamReader(fr);
        BufferedReader br = new BufferedReader(isr);
        StringBuilder sb = new StringBuilder();
        while (!br.readLine().isEmpty()){
            sb.append(br.readLine());
        }

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedhash = digest.digest(sb.toString().getBytes(StandardCharsets.UTF_8));
        String byteToHex = bytesToHex(encodedhash);
        System.out.println(byteToHex);
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

}
