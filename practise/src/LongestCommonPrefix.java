import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LongestCommonPrefix {

    public static void main(String args[]){
        String[] str = {"flower", "flow", "flight"};
        System.out.println(longestCommonPrefix(str));
    }

    private static boolean validateInput(String[] strs) {
        if (strs.length == 0 || strs.length < 1 || strs.length > 200) {
            return false;
        }

        for (String str : strs) {
            if (str.length() > 200) {
                return false;
            }
        }
        return true;
    }

    public static String longestCommonPrefix(String[] strs) {

        if (!validateInput(strs)) {
            return "";
        } else if (strs.length == 1) {
            return strs[0];
        } else {
            List<String> strList = Arrays.asList(strs);
            Collections.sort(strList, new MyStringComparator());

            String firstStr = strList.get(0);

            int sizeOfLcp = firstStr.length();
            for (int i = 1; i < strList.size(); i++) {
                String temp = strList.get(i);
                boolean noMatch = true;
                int tempSize = 0;
                for (int j = 0; j < firstStr.length(); j++) {
                    if (firstStr.charAt(j) == (temp.charAt(j))) {
                        noMatch = false;
                        tempSize++;
                    } else {
                        break;
                    }
                }
                if (noMatch) {
                    return "";
                } else {
                    sizeOfLcp = tempSize < sizeOfLcp ? tempSize : sizeOfLcp;
                }
            }

            if (sizeOfLcp == firstStr.length()) {
                return firstStr;
            } else {
                return firstStr.substring(0, sizeOfLcp);
            }
        }

    }
}
class MyStringComparator implements Comparator {

    public int compare(Object o1, Object o2){

        String str1 = (String)o1;
        String str2 = (String)o2;

        if(str1.length() > str2.length()){
            return 1;
        }

        else if(str1.length() < str2.length()){
            return -1;
        }

        else{
            return 0;
        }

    }
}