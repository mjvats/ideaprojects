import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class TrashBins {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        int count = 1;
        while (count <= T){
            int n = Integer.parseInt(br.readLine());
            String[] input = br.readLine().trim().split("");

            byte[] arr = new byte[n];
            for(int i = 0 ; i < n ; i++) arr[i] = Byte.parseByte(input[i]);

            long result = totalDistance(arr);
            bw.write("Case #"+count+": "+result);
            bw.write("\n");
            count++;
        }

        br.close();
        bw.close();
    }

    public static long totalDistance(byte[] arr){
        int n = arr.length;
        int[] l2r = new int[n];
        l2r[0] = arr[0] == 0 ? -1 : 0;
        for(int i = 1 ; i < n ; i++){
            l2r[i] = arr[i] == 1 ? i : l2r[i-1];
        }

        int[] r2l = new int[n];
        r2l[n-1] = arr[n-1] == 1 ? n-1 : -1;
        for(int i = n-2 ; i >= 0 ; i--){
            r2l[i] = arr[i] == 1 ? i : r2l[i+1];
        }

        long sum = 0L;
        for(int i = 0 ; i < n ; i++){
            int right = Math.abs(i-r2l[i]);
            int left =  Math.abs(i-l2r[i]);
            if(l2r[i] == -1){
                sum += right;
            }else if(r2l[i] == -1){
                sum += left;
            }else{
                sum += Math.min(left,right);
            }
        }

        return sum;
    }

}
