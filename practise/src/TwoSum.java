import java.util.Scanner;

public class TwoSum {

    public static void main (String[] args) throws java.lang.Exception
    {
        // your code goes here
        Scanner sc = new Scanner(System.in);

        int t = Integer.parseInt(sc.nextLine());
        if(t < 1 || t > 1000){
            return;
        }
        while(t > 0){
            String[] input = sc.nextLine().trim().split(" ");
            int a = Integer.parseInt(input[0]);
            int b = Integer.parseInt(input[1]);
            if(a < 0 || a > 10000 || b < 0 || b > 10000){
                return;
            }
            System.out.println(a+b);
            t--;
        }

    }

}
