import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PascalsTriangle2 {

    public static void main(String[] args) {
        List<Integer> result = new PascalsTriangle2().getRow(6);
        for(Integer val : result){
            System.out.print(val + "\0");
        }
    }

    public List<Integer> getRow(int rowIndex) {
        if(rowIndex < 0 || rowIndex > 33)
            return null;

        List<Integer> list = new ArrayList<>();
        int[] arr = new int[rowIndex+1];
        arr[0] = 1;
        for(int i = 0 ; i <= rowIndex ; i++){
            arr[i] = 1;
            int temp_1 = arr[0];
            int temp_2 = 0;
            for(int j = 1; j < i ; j++){
                temp_2 = arr[j];
                arr[j] = arr[j] + temp_1;
                temp_1 = temp_2;
            }
        }
        for(int i = 0 ; i < arr.length ; i++){
            list.add(arr[i]);
        }
        return list;
    }

}

