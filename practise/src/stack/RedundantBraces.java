package stack;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class RedundantBraces {

    public static void main(String[] args) {
        String input = "((a) + (a + b))";
        int result = braces(input);
        System.out.println(result);
    }

    public static int braces(String A) {
        Stack<Character> s = new Stack<>();
        int n = A.length();
        for(int i = 0 ; i < n ; i++){
            boolean isBalanced = false;
            if(A.charAt(i) == 40 || A.charAt(i) == 42 || A.charAt(i) == 43 || A.charAt(i) == 45 || A.charAt(i) == 47){
                s.push(A.charAt(i));
            }
            else if(A.charAt(i) == 41){
                while(!s.isEmpty() && s.peek() != 40){
                    isBalanced = true;
                    s.pop();
                }
                if(!isBalanced){
                    return 0;
                } else{
                    s.pop();
                }
            }
        }
        return 1;
    }

}
