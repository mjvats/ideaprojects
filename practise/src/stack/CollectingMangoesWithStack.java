package stack;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Stack;

public class CollectingMangoesWithStack {

    static Stack<Long> s;
    static long max;

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        int c = 1;
        while (c <= T) {
            max = 0;
            bw.write("Case " + c + ":\n");
            int Q = Integer.parseInt(br.readLine());
            s = new Stack<>();
            while (Q > 0) {
                String[] input = br.readLine().trim().split(" ");
                if (input[0].equals("A")) {
                    add(Integer.parseInt(input[1]));
                } else if (input[0].equals("R")) {
                    pop();
                } else {
                    bw.write(getMax());
                    bw.write("\n");
                }
                Q--;
            }
            c++;
        }

        br.close();
        bw.close();
    }

    public static void add(int a) {
        if (max == 0) {
            max = a;
        }

        if (a <= max) {
            s.push((long) a);
        } else {
            s.push((2L *a)-max);
            max = a;
        }
    }

    public static String getMax(){
        if(s.isEmpty() || max == 0){
            return "Empty";
        }else{
            return String.valueOf(max);
        }
    }

    public static void pop(){
        if(!s.isEmpty()){
            long val = s.pop();
            if(val > max){
                max = 2*max - val;
            }
        }
    }


}
