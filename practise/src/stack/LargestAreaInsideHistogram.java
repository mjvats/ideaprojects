package stack;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Stack;

public class LargestAreaInsideHistogram {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            int n = Integer.parseInt(br.readLine());
            String[] input = br.readLine().trim().split(" ");
            int[] a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(input[i]);
            }
            int result = getMaxArea(a);
            bw.write(result + "");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int getMaxArea(int[] a) {
        int[] l2r = findl2r(a);
        int[] r2l = findr2l(a);

        int ans = 0;
        for (int i = 0; i < a.length; i++) {
            ans = Math.max(ans, a[i] * (r2l[i] - l2r[i] + 1));
        }
        return ans;
    }

    public static int[] findl2r(int[] a) {
        int n = a.length;
        int[] l2r = new int[n];
        Stack<Integer> s = new Stack<>();
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                l2r[i] = i;
                s.push(i);
            } else {
                while (!s.empty()) {
                    int j = s.peek();
                    if (a[j] < a[i]) {
                        l2r[i] = j + 1;
                        s.push(j + 1);
                        if (s.peek() != i) {
                            s.push(i);
                        }
                        break;
                    } else if (j == 0) {
                        l2r[i] = 0;
                        if (s.peek() != i) {
                            s.push(i);
                        }
                        break;
                    } else {
                        s.pop();
                    }
                }
            }
        }
        return l2r;
    }

    public static int[] findr2l(int[] a) {
        int n = a.length;
        int[] r2l = new int[n];
        Stack<Integer> s = new Stack<>();

        for (int i = n - 1; i >= 0; i--) {
            if (i == n - 1) {
                r2l[i] = n - 1;
                s.push(n - 1);
            } else {
                while (!s.empty()) {
                    int j = s.peek();
                    if (a[j] < a[i]) {
                        r2l[i] = j - 1;
                        s.push(j - 1);
                        if (s.peek() != i) {
                            s.push(i);
                        }
                        break;
                    } else if (j == n - 1) {
                        r2l[i] = n - 1;
                        if (s.peek() != i) {
                            s.push(i);
                        }
                        break;
                    } else {
                        s.pop();
                    }
                }
            }
        }
        return r2l;
    }

}
