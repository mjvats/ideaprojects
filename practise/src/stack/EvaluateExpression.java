package stack;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class EvaluateExpression {

    public static void main(String[] args) {
        String[] inp = {"5", "1", "2", "+", "4", "*", "+", "3", "-"};
        List<String> A = Arrays.asList(inp);
        int result = evalRPN(A);
        System.out.println(result);
    }

    public static int evalRPN(List<String> A) {

        Stack<Integer> s = new Stack<>();
        for (String val : A) {
            if (!isExpression(val)) {
                s.push(Integer.parseInt(val));
            } else {
                if (val.equals("+") && s.size() >= 2) {
                    int a = s.pop();
                    int b = s.pop();
                    s.push(a + b);
                } else if (val.equals("-") && s.size() >= 2) {
                    int a = s.pop();
                    int b = s.pop();
                    s.push(b - a);
                } else if (val.equals("*") && s.size() >= 2) {
                    int a = s.pop();
                    int b = s.pop();
                    s.push(b * a);
                } else {
                    if (s.size() >= 2) {
                        int a = s.pop();
                        int b = s.pop();
                        s.push(b / a);
                    }
                }
            }
        }
        if (!s.isEmpty()) {
            return s.pop();
        } else {
            return Integer.MIN_VALUE;
        }
    }

    public static boolean isExpression(String A) {
        return A.equals("+") || A.equals("-") || A.equals("*") || A.equals("/");
    }

}
