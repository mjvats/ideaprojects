package stack;

import java.util.Stack;

public class SimplifyDirectoryPath {

    public static void main(String[] args) {
        String input = "/a/./b/../../c/";
        String result = simplifyPath(input);
        System.out.println(result);
    }

    public static String simplifyPath(String A) {

        Stack<String> s = new Stack<>();
        String[] a = A.trim().split("/");
        for(String val : a){
            if(val.equals(".") || val.equals(""));
            else if(val.equals("..")){
                if(!s.isEmpty()){
                    s.pop();
                }
            }
            else{
                s.push(val);
            }
        }

        if(s.isEmpty()){
            return "/";
        }
        StringBuilder result = new StringBuilder();
        while(!s.isEmpty()){
            result.insert(0, "/" + s.pop());
        }
        return result.toString();
    }

}
