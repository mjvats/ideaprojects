package stack;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class stack {

    static int[] arr;
    static int k = -1;
    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        arr = new int[T];
        while(T > 0){

            String[] input = br.readLine().trim().split(" ");
            String op = input[0];
            if(op.equals("pop"))
            {
                bw.write(pop());
                bw.write("\n");
            }
            else push(Integer.parseInt(input[1]));
            T--;
        }

        br.close();
        bw.close();
    }

    public static void push(int val)
    {
        arr[++k] = val;
    }

    public static String pop()
    {
        if(k == -1) return "Empty";
        else return String.valueOf(arr[k--]);
    }

}
