package stack;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ReverseTheSentence {

    static String[] stack;
    static int k = -1;
    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            String[] input = br.readLine().trim().split(" ");
            stack = new String[input.length];
            for(int i = 0 ; i < input.length ; i++)
            {
                push(input[i]);
            }

            while (k > -1)
            {
                bw.write(pop()+" ");
            }
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static void push(String s)
    {
        stack[++k] = s;
    }

    public static String pop()
    {
        if(k == -1) return "Empty";
        else return String.valueOf(stack[k--]);
    }

}
