package stack;

public class MinStackTest {

    public static void main(String[] args) {
        MinStack m = new MinStack();
        m.push(512);
        m.push(-1024);
        m.push(-1024);
        m.push(512);

/*        System.out.println(m.getMin());
        m.push(9);
        System.out.println(m.getMin());
        m.push(8);
        System.out.println(m.getMin());
        m.push(7);
        System.out.println(m.getMin());
        m.push(6);
        System.out.println(m.getMin());*/
        m.pop();
        System.out.println(m.getMin());
        m.pop();
        System.out.println(m.getMin());
        m.pop();
        System.out.println(m.getMin());
        m.pop();
        System.out.println(m.getMin());
        m.pop();
        System.out.println(m.getMin());
        System.out.println(m.getMin());

    }

}
