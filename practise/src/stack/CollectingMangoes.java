package stack;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class CollectingMangoes {

    static long[] arr;
    static int k;
    static long max;

    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        int c = 1;
        while (c <= T) {
            k = -1;
            max = 0;
            bw.write("Case " + c + ":\n");
            int Q = Integer.parseInt(br.readLine());
            arr = new long[Q];
            while (Q > 0) {
                String[] input = br.readLine().trim().split(" ");
                if (input[0].equals("A")) {
                    add(Integer.parseInt(input[1]));
                } else if (input[0].equals("R")) {
                    pop();
                } else {
                    bw.write(getmax());
                    bw.write("\n");
                }
                Q--;
            }
            c++;
        }

        br.close();
        bw.close();
    }

    public static void add(int a) {
        if(max == 0) max = a;
        if (a <= max) {
            arr[++k] = a;
        } else {
            arr[++k] = (2L * a) - max;
            max = a;
        }
    }

    public static String getmax() {
        if (k > -1 && max > 0) {
            return String.valueOf(max);
        } else {
            return "Empty";
        }
    }

    public static void pop() {
        if (k > -1) {
            long val = arr[k];
            if (val > max) {
                max = 2 * max - val;
            }
            k--;
        }
        if(k == -1){
            max = 0;
        }
    }

}
