package stack;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Comparator;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

public class CollectingMangoesWithTreeSet {

    static Stack<Integer> s;
    static Set<Integer> set;

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        int c = 1;
        while (c <= T) {
            bw.write("Case " + c + ":\n");
            int Q = Integer.parseInt(br.readLine());
            s = new Stack<>();
            set = new TreeSet<>();
            while (Q > 0) {
                String[] input = br.readLine().trim().split(" ");
                if (input[0].equals("A")) {
                    add(Integer.parseInt(input[1]));
                } else if (input[0].equals("R")) {
                    pop();
                } else {
                    bw.write(getMax());
                    bw.write("\n");
                }
                Q--;
            }
            c++;
        }

        br.close();
        bw.close();
    }

    public static void add(int a) {
        s.push(a);
        set.add(a);
    }

    public static String getMax(){
        if(!s.isEmpty()){
            int max = set.stream().max(new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    if(o1 < o2) return -1;
                    else if (o1 > o2) return 1;
                    return 0;
                }
            }).get();
            return String.valueOf(max);
        }
        return "Empty";
    }

    public static void pop(){
        if(!s.isEmpty()){
            set.remove(s.pop());
        }
    }

}
