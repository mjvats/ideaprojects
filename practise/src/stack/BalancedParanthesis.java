package stack;

import java.util.Stack;

public class BalancedParanthesis {

    public static void main(String[] args) {
        String input = " ";
        System.out.println(solve(input));
    }

    public static int solve(String A) {
        Stack<Character> s = new Stack<>();
        for(int i = 0 ; i < A.length() ; i++){
            if(A.charAt(i) == 40){
                s.push(A.charAt(i));
            }
            else if (!s.isEmpty()){
                s.pop();
            }
            else{
                return 0;
            }
        }

        return s.isEmpty() ? 1 : 0;
    }

}
