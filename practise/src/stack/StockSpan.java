package stack;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class StockSpan {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            int n = Integer.parseInt(br.readLine());
            String[] input = br.readLine().trim().split(" ");
            int[] a = new int[n];
            for(int i = 0 ; i < n ; i++) a[i] = Integer.parseInt(input[i]);
            int[] result = getSpan(a);

            for(int i = 0 ; i < n ; i++)
                bw.write(result[i]+" ");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int[] getSpan(int[] a)
    {
        int n = a.length;
        int[] result = new int[n];

        result[0] = 1;
        for(int i = 1 ; i < n ; i++)
        {
            int count = 0;
            for(int j = i ; j >= 0 ; j--)
            {
                if(a[j] > a[i]) break;
                else count++;
            }
            result[i] = count;
        }
        return result;
    }

}
