package stack;

public class StackDemo<E> {

    private Object[] arr;
    private int k = -1;

    public StackDemo(int size){
        this.arr = new Object[size];
    }

    public void push(E val){
        arr[++k] = val;
    }

    public E pop(){
        if(!isEmpty()){
            Object val = arr[k];
            k--;
            return (E) val;
        }
        return null;
    }

    public E peek(){
        if(!isEmpty()){
            Object val = arr[k];
            return (E) val;
        }
        return null;
    }

    public boolean isEmpty(){
        return k == -1;
    }

}
