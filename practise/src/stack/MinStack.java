package stack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MinStack {

    DLLStack s;
    MinList minList;

    public MinStack() {
        s = new DLLStack();
        this.minList = new MinList();
    }

    public void push(int val) {
        s.push(val);
        minList.add(val);
    }

    public void pop() {
        int data = s.pop();
        minList.delete(data);
    }

    public int top() {
        return s.top();
    }

    public int getMin() {
        if (!s.isEmpty()) {
            return minList.getMin();
        }
        return Integer.MIN_VALUE;
    }

}


class Node {

    int data;
    Node next;
    Node prev;

    Node(int data) {
        this.data = data;
        this.next = null;
        this.prev = null;
    }
}

class DLLStack {

    Node head;
    Node last;

    DLLStack() {
        head = null;
        last = null;
    }

    public void push(int data) {
        Node newNode = new Node(data);
        if (last == null) {
            last = newNode;
            head = last;
        } else {
            newNode.prev = last;
            last.next = newNode;
            last = newNode;
        }
    }

    public int pop() {
        int data = Integer.MIN_VALUE;
        if (!isEmpty()) {
            data = last.data;
            last = last.prev;
            if(last != null){
                last.next = null;
            }
        }
        return data;
    }

    public int top() {
        if (!isEmpty()) {
            return last.data;
        }
        return -1;
    }

    public boolean isEmpty() {
        return last == null;
    }
}

class MinList {

    Node head;
    Map<Integer, List<Node>> map;

    MinList() {
        this.head = null;
        this.map = new HashMap<>();
    }

    public void add(int data) {
        Node node = new Node(data);
        if (head == null) {
            head = node;
        } else {
            if (data < head.data) {
                node.next = head;
                head.prev = node;
                head = node;
            } else {
                if (map.containsKey(data)) {
                    List<Node> list = map.get(data);
                    if (!list.isEmpty()) {
                        Node temp = list.get(0);
                        if (temp.prev == null) {
                            node.next = temp;
                            temp.prev = node;
                            head = node;
                        } else {
                            node.next = temp;
                            node.prev = temp.prev;
                            temp.prev.next = node;
                            temp.prev = node;
                        }
                        list.add(node);
                    }
                }
                return;
            }
        }
        if (map.containsKey(data)) {
            map.get(data).add(head);
        } else {
            List<Node> list = new ArrayList<>();
            list.add(head);
            map.put(data, list);
        }
    }

    public int getMin() {
        return head != null ? head.data : Integer.MIN_VALUE;
    }

    public void delete(int data) {
        if (map.containsKey(data)) {
            List<Node> list = map.get(data);
            int i = 0;
            for (; i < list.size(); i++) {
                Node node = list.get(i);
                if (node != null) {
                    if (node.prev == null) {
                        head = node.next;
                        if (head != null) {
                            head.prev = null;
                        }
                    } else {
                        node.prev.next = node.next;
                    }
                    list.set(i, null);
                    break;
                }
            }
            if (i == list.size() - 1) {
                map.remove(data);
            }
        }
    }
}
