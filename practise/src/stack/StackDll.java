package stack;

public class StackDll<E> {

    private ListNode<E> root;
    private ListNode<E> last;

    public StackDll() {
        this.root = null;
        this.last = null;
    }

    public void push(E data) {
        if (isEmpty()) {
            this.root = new ListNode<E>(data);
            this.last = root;
        } else {
            ListNode<E> node = new ListNode<E>(data);
            node.prev = this.last;
            this.last.next = node;
            this.last = node;
        }
    }

    public E pop() {
        if (!isEmpty() && this.last != null) {
            E val = (E) this.last.data;
            if(this.last == this.root){
                this.root = null;
            }
            this.last = this.last.prev;
            if(this.last != null)
                this.last.next = null;
            return val;
        }
        return null;
    }

    public E peek(){
        if (!isEmpty() && this.last != null) {
            return (E) this.last.data;
        }
        return null;
    }

    public boolean isEmpty() {
        return this.last == this.root && this.last == null;
    }

}

class ListNode<E> {

    Object data;
    ListNode<E> next;
    ListNode<E> prev;

    ListNode(Object data) {
        this.data = data;
        this.next = null;
        this.prev = null;
    }
}
