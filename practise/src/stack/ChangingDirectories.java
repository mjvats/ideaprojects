package stack;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ChangingDirectories {

    static String[] arr;
    static int k;

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            arr = new String[200];
            k = -1;
            int Q = Integer.parseInt(br.readLine());
            while (Q > 0) {
                String input = br.readLine().trim();
                if (input.equals("pwd")) {
                    if (k > -1) {
                        for (int i = 0; i <= k; i++) {
                            bw.write("/" + arr[i]);
                        }
                    }
                    bw.write("/");
                    bw.write("\n");
                } else {
                    String[] strArr = input.split(" ");
                    String path = strArr[1];
                    String[] directories = path.split("/");
                    if (path.charAt(0) == '/') {

                        addFromRoot(directories);
                    } else {
                        add(directories);
                    }
                }
                Q--;
            }
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static void addFromRoot(String[] d) {
        k = -1;
        for (String s : d) {
            if (s.equals("..")) {
                if(k > -1) k--;
            } else if (!s.equals("")) {
                arr[++k] = s;
            }
        }
    }

    public static void add(String[] d) {
        for (String s : d) {
            if (s.equals("..") && k > -1) {
                k--;
            } else {
                arr[++k] = s;
            }
        }
    }

}
