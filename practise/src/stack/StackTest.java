package stack;

public class StackTest {

    public static void main(String[] args) {
        StackDll<Integer> s = new StackDll<Integer>();
        s.push(5);
        s.push(6);
        s.push(7);
        System.out.println(s.peek());
        System.out.println(s.pop());
        System.out.println(s.pop());
        s.push(null);
        System.out.println(s.pop());
        System.out.println(s.isEmpty());
        System.out.println(s.pop());
        System.out.println(s.isEmpty());
        System.out.println(s.pop());
    }

}
