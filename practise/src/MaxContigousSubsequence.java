import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashSet;

public class MaxContigousSubsequence {

    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            int n = Integer.parseInt(br.readLine());
            String[] input = br.readLine().trim().split(" ");

            int[] arr = new int[n];
            for(int i = 0 ; i < n ; i++) arr[i] = Integer.parseInt(input[i]);

            int result = sol(arr);
            bw.write(result+"");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }


    public static int sol(int arr[])
    {
        int n = arr.length;
        int ans = Integer.MIN_VALUE;
        for(int i = 0 ; i < n ; i++)
        {
            HashSet<Integer> set = new HashSet<>();
            int max = arr[i];
            int min = arr[i];
            for(int j = i ; j < n ; j++)
            {
                if(set.contains(arr[j])) break;
                else set.add(arr[j]);

                if((Math.abs(arr[i]-arr[j])+1) == 2) ans = Integer.max(ans, 2);
                max = Integer.max(max, arr[j]);
                min = Integer.min(min, arr[j]);
                if((max - min + 1) == (j-i+1)) ans = Integer.max(ans, (j-i+1));

            }
        }
        return ans;
    }

}
