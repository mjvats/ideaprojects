package anagraming;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Anagraming {

    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        int count = 1;
        while(count <= T){
            String input = br.readLine();
            String result = getAnagram(input);
            bw.write("Case #"+count+": "+result);
            bw.write("\n");
            count++;
        }

        br.close();
        bw.close();

    }

    public static String getAnagram(String s)
    {
        if(s.length() == 1)
        {
            return "IMPOSSIBLE";
        }
        char[] charArr = s.toCharArray();
        boolean ifPossible = false;
        for(int i = 1 ; i < charArr.length ; i++)
        {
            if(charArr[i] != charArr[i-1])
            {
                ifPossible = true;
                break;
            }
        }
        if (!ifPossible)
        {
            return "IMPOSSIBLE";
        }
        else
        {
            int i = 0; int j = charArr.length-1;
            while (i < j)
            {
                if(charArr[i] != charArr[j])
                {
                    char temp = charArr[i];
                    charArr[i] = charArr[j];
                    charArr[j] = temp;
                    break;
                }
            }
        }
        return String.valueOf(charArr);
    }

}
