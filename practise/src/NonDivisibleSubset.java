import java.util.Arrays;
import java.util.List;

public class NonDivisibleSubset {

    public static void main(String[] args) {
        Integer[] A = {1, 7, 2, 4};
        List<Integer> list = Arrays.asList(A);
        System.out.println(subset(list, 3));
    }

    public static int subset(List<Integer> s, int k){
        if(k == 0 || k == 1)
        {
            return 0;
        }

        for(int i = 0 ; i < s.size() ; i++)
        {
            s.set(i, s.get(i)%k);
        }
        int[] countarr = new int[k];

        for(int i = 0 ; i < s.size() ; i++)
        {
            countarr[s.get(i)]++;
        }

        for(int i = 0, j = k-1 ; i < j ; )
        {
            if(i+j == k)
            {
                if(countarr[i] < countarr[j])
                {
                    countarr[i] = 0;
                    i++;
                }
                else
                {
                    countarr[j] = 0;
                    j--;
                }
            }
            else if(i+j < k)
            {
                i++;
            }
            else
            {
                j--;
            }
        }

        int count = 0;
        for(int i = 0; i < k ; i++)
        {
            count += countarr[i];
        }
        return count;

        /*s.clear();
        for(int i = 0 ; i < countarr.length ; i++)
        {
            if(countarr[i] > 0)
            {
                while(countarr[i] != 0){
                    s.add(i);
                    countarr[i]--;
                }
            }
        }*/

        /*for(int i = 0, j = s.size()-1 ; i < j ; )
        {
            int a = s.get(i);
            int b = s.get(j);
            if(a == 0)
            {

            }
            if((a+b) == k)
            {
                if(countarr[a] < countarr[b])
                {
                    while(s.get(i) == a)
                    {
                        i++;
                    }
                }
                else
                {
                    while(s.get(j) == b)
                    {
                        j--;
                    }
                }
            }
        }*/
    }

}
