public class LongDemo {

    public static void main(String[] args) {
       long result  = xorSequence(100000000000000L, 1000000000000000L);
       System.out.println(result);
    }

    // Complete the xorSequence function below.
    static long xorSequence(long l, long r) {

        long L = func(l);
        long result = L;
        while(l < r){
            L = L^(l+1);
            l = l+1;
        }
        return L;
    }

    static long func(long index){
        if(index == 1){
            return 1L;
        }
        return index^func(index-1L);
    }
}
