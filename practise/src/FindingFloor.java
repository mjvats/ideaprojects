import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class FindingFloor {

    static int[] arr;
    static int n;
    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        n = Integer.parseInt(br.readLine());
        arr = new int[n];
        String[] line1 = br.readLine().trim().split(" ");
        for (int i = 0; i < n; i++) {
            arr[i] = Integer.parseInt(line1[i]);
        }
        mergeSort(arr, 0 , n-1);

        int Q = Integer.parseInt(br.readLine());
        while(Q > 0)
        {
            int k = Integer.parseInt(br.readLine());
            long result = sol(k);
            bw.write(result + "");
            bw.write("\n");
            Q--;
        }

        br.close();
        bw.close();
    }

    public static int sol(int k){
        int index = bs(k);
        if(index == -1)
        {
            return Integer.MIN_VALUE;
        }
        return arr[index];
    }

    public static int bs(int k){
        int start = 0 , end = n-1;
        while (start <= end)
        {
            int mid = (start+end)/2;
            if(arr[mid] <= k)
            {
                if(mid== n-1 || (mid < n-1 && arr[mid+1] > k))
                {
                    return mid;
                }
                else
                {
                    start = mid+1;
                }
            }
            else
            {
                end = mid-1;
            }
        }
        return -1;
    }

    static void mergeSort(int[] A, int start, int end){
        if(start == end){
            return;
        }
        int mid = (start + end)/2;
        mergeSort(A, start, mid);
        mergeSort(A, mid+1, end);
        merge(A, start, end);
    }

    static void merge(int[] A, int start, int end){
        int mid = (start + end)/2;
        int[] L = new int[mid - start +1];
        int[] R = new int[end - mid];
        int k = 0;
        for(int i = start ; i <= mid ; i++){
            L[k++] = A[i];
        }
        k=0;
        for(int i = mid+1 ; i <= end ; i++){
            R[k++] = A[i];
        }

        int i = 0; int j = 0; k = start;
        while(i < L.length && j < R.length){
            if(L[i] <= R[j]){
                A[k++] = L[i];
                i++;
            }
            else{
                A[k++] = R[j];
                j++;
            }
        }

        while(i < L.length){
            A[k++] = L[i];
            i++;
        }

        while(j < R.length){
            A[k++] = R[j];
            j++;
        }
    }

}
