package triedatastructure;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;

public class QuadruplesOfXOR {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            int n = Integer.parseInt(br.readLine());
            int[] a = new int[n];
            int[] b = new int[n];
            int[] c = new int[n];
            int[] d = new int[n];

            String[] line1 = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(line1[i]);
            }

            String[] line2 = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                b[i] = Integer.parseInt(line2[i]);
            }

            String[] line3 = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                c[i] = Integer.parseInt(line3[i]);
            }

            String[] line4 = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                d[i] = Integer.parseInt(line4[i]);
            }

            int result = count(a, b, c, d);
            bw.write(result + "");
            bw.write("\n");

            T--;
        }

        br.close();
        bw.close();
    }

    public static int sol(int[] a, int[] b, int[] c, int[] d) {
        int count = 0;
        for (int i = 0; i < 32; i++) {
            int _0count = 0;
            int _1count = 0;
            for (int k : a) {
                if (((k >> i) & 1) == 1) {
                    _1count++;
                } else {
                    _0count++;
                }
            }

            for (int k : b) {
                if (((k >> i) & 1) == 1) {
                    _1count++;
                } else {
                    _0count++;
                }
            }

            for (int k : c) {
                if (((k >> i) & 1) == 1) {
                    _1count++;
                } else {
                    _0count++;
                }
            }

            for (int k : d) {
                if (((k >> i) & 1) == 1) {
                    _1count++;
                } else {
                    _0count++;
                }
            }

            if (_0count % 2 == 0 && _1count % 2 == 1) {
                count++;
            }
        }
        return count;
    }

    public static int count(int[] a, int[] b, int[] c, int[] d) {
        ArrayList<Integer> list1 = new ArrayList<>();
        for (int value : a) {
            for (int i : b) {
                list1.add(value ^ i);
            }
        }

        ArrayList<Integer> list2 = new ArrayList<>();
        for (int value : c) {
            for (int i : d) {
                list2.add(value ^ i);
            }
        }

        Collections.sort(list2);
        int count = 0;
        for (int val : list1) {
            if(bs(list2, val)){
                count++;
            }
        }
        return count;
    }

    public static boolean bs(ArrayList<Integer> list, int k) {
        int start = 0;
        int end = list.size() - 1;

        while (start <= end) {
            int mid = (start + end) / 2;
            if (list.get(mid) == k) {
                return true;
            } else if (list.get(mid) < k) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return false;
    }

}
