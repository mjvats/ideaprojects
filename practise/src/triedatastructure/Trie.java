package triedatastructure;

public class Trie {

    TrieNode rootNode;

    public Trie() {
        rootNode = new TrieNode();
    }

    public boolean insert(String str) {
        if (str == null || str.isEmpty()) {
            return false;
        }
        if (rootNode == null) {
            rootNode = new TrieNode();
        }

        char[] charArr = str.toCharArray();
        TrieNode currNode = rootNode;
        for (int i = 0; i < charArr.length; i++) {
            int index = charArr[i] - 97;
            if (currNode.nodes[index] == null) {
                TrieNode newNode = i == charArr.length - 1 ? new TrieNode(charArr[i], true)
                    : new TrieNode(charArr[i], false);
                currNode.nodes[index] = newNode;
                currNode = newNode;
            } else {
                currNode = currNode.nodes[index];
            }
        }
        return true;
    }

    public boolean search(String str) {
        if (str == null || str.isEmpty()) {
            return false;
        }

        //boolean doesExist = false;
        char[] charArr = str.toCharArray();
        TrieNode currNode = rootNode;
        for (int i = 0; i < charArr.length; i++) {
            int index = charArr[i] - 97;
            if (currNode.nodes[index] == null) {
                return false;
            } else {
                currNode = currNode.nodes[index];
                if (currNode.value == charArr[i]) {
                    if ((i == charArr.length - 1 && !currNode.word_end) || (i < charArr.length - 1
                        && currNode.word_end)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
