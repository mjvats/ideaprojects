package triedatastructure;

public class TrieNode {

    public char value;
    public boolean word_end;
    public TrieNode[] nodes;

    TrieNode(){
        this.word_end = false;
        nodes = new TrieNode[26];
    }

    TrieNode(char value, boolean word_end) {
        this.value = value;
        this.word_end = word_end;
        this.nodes = new TrieNode[26];
    }

}
