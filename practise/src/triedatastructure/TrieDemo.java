package triedatastructure;

public class TrieDemo {

    public static void main(String args[]){
        Trie trie = new Trie();
        trie.insert("abc");
        trie.insert("aab");
        trie.insert("aba");
        trie.insert("bcd");
        trie.insert("ccd");

        System.out.println(trie.search("aba"));
        System.out.println(trie.search("adf"));
    }

}
