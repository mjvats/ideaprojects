package triedatastructure;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class FirstMissingPositiveInteger {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0){
            int n = Integer.parseInt(br.readLine());
            List<Integer> list = new ArrayList<>();
            String[] input = br.readLine().trim().split(" ");
            for(String val : input){
                list.add(Integer.parseInt(val));
            }

            int ans = sol(list);
            bw.write(ans+"\n");
            T--;
        }
        br.close();
        bw.flush();
    }

    public static int sol(List<Integer> input) {
        int n = input.size();
        int[] temp = new int[n + 1];

        for (int val : input) {
            if (val > 0 && val <= n) {
                temp[val] = -1;
            }
        }

        for (int i = 1; i <= n; i++) {
            if (temp[i] != -1) {
                return i;
            }
        }
        return -1;
    }

}
