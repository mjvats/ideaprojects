import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StringDemo {

    public static void main(String args[]){

        /*Student student = new Student();
        System.out.println("student : " + String.valueOf(student));*/
        /*if(student == null){
            System.out.println("student : " + null);
        }
        else{
            System.out.println("student : " + student.name);
        }*/

        String[] strArray = {"mj", "vats", "k"};
        List<String> strList = Arrays.asList(strArray);

        System.out.println("string list before sorting:");
        for (String value : strList) {
            System.out.print(value+"\n");
        }

        Collections.sort(strList, new MyComparator());
        System.out.println("string list after sorting:");
        for (String value : strList) {
            System.out.print(value+"\n");
        }

    }
}

class Student{
    String name = "Mj";

    Student(){
        this.name = "Vats";
    }
}

class MyComparator implements Comparator{

    @Override
    public int compare(Object o1, Object o2) {
        String str1 = (String)o1;
        String str2 = (String)o2;

        if(str1.length() > str2.length())
            return 1;
        else if(str1.length() < str2.length())
            return -1;
        else
            return 0;
    }
}