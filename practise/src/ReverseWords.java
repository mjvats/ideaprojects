public class ReverseWords {

    public static void main(String[] args) {
        String input = "Let's take LeetCode contest";
        System.out.println(new ReverseWords().reverseWords(input));
    }

    public String reverseWords(String s) {
        String[] sArr = s.split(" ");
        String result = "";
        for(String str : sArr){
            char[] charArr = str.toCharArray();
            for(int i = 0, j = charArr.length-1 ; i<j ; i++, j--){
                int x = charArr[i]^charArr[j];
                charArr[j]= (char)(x^charArr[j]);
                charArr[i] = (char)(x^charArr[j]);
            }
            result += String.valueOf(charArr) + " ";
        }
        return result.trim();
    }
}
