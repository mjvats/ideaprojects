import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class LargeSquare {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0)
        {
            String[] input = br.readLine().trim().split(" ");
            long N = Long.parseLong(input[0]);
            long A = Long.parseLong(input[1]);
            long result = func(N);
            bw.write(String.valueOf(result*A));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static long func(long N){
        long start = 1, end = N;
        int ans = 0;
        long prevMid = 0;
        while (start < end)
        {
            long mid = (start + end)/2;
            if(mid == prevMid)
            {
                return ans;
            }
            prevMid = mid;
            long oddCount = mid - (mid/2);
            long sum = (oddCount*(2 + (oddCount-1)* 2L)/2);
            if(sum <= N)
            {
                return oddCount;
            }
            else
            {
                end = mid-1;
            }
        }
        return ans;
    }

}
