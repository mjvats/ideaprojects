package siinternals2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MatrixFlips {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0){
            String[] line1 = br.readLine().trim().split(" ");
            int m = Integer.parseInt(line1[0]);
            int n = Integer.parseInt(line1[1]);

            int[][] dp = new int[m][n];
            for(int i = 0 ; i < m ; i++){
                String[] input = br.readLine().trim().split("");
                for(int j = 0 ; j < n ; j++){
                    dp[i][j] = Integer.parseInt(input[j]);
                }
            }
            bw.write(sol(dp)+"\n");
            T--;
        }
        br.close();
        bw.close();
    }


    public static int sol(int[][] dp) {
        int n = dp.length;
        int m = dp[0].length;

        int ans = 0;
        for(int i = n-1; i >= 0 ; i--){
            for(int j = m-1; j >= 0 ; j--){
                if(dp[i][j] == 0){
                    ans++;
                    for(int x = i ; x >= 0 ; x--){
                        for(int y = j ; y >= 0 ; y--){
                            if(dp[x][y] == 0){
                                dp[x][y] = 1;
                            } else {
                                dp[x][y] = 0;
                            }
                        }
                    }
                }
            }
        }

        return ans;
    }
}
