package siinternals2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

public class LongestSubsetZero {

    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0){
            int n = Integer.parseInt(br.readLine());
            int[] arr = new int[n];

            String[] input = br.readLine().trim().split(" ");
            for(int i = 0 ; i < n ; i++){
                arr[i] = Integer.parseInt(input[i]);
            }

            int result = sol(arr);
            bw.write(result+"\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int sol(int[] arr)
    {
        int n = arr.length;
        Map<Integer, Integer> map = new HashMap<>();
        int length = 0;

        int sum = 0;
        for(int i = 0 ; i < n ; i++){
            sum += arr[i];
            if(arr[i] == 0){
                length = Math.max(length, 1);
            }
            if(sum == 0){
                length = Math.max(length, i+1);
            }
            if(map.containsKey(sum)){
                length = Math.max(length, i - map.get(sum));
            }
            map.put(sum, i);

        }

        return length;
    }
}
