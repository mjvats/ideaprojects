package siinternals2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RangeQueries {

    static List<Integer> inpList = new ArrayList<>();
    static int n = 0;

    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0){
            String[] line1 = br.readLine().split(" ");
            n = Integer.parseInt(line1[0]);
            int k = Integer.parseInt(line1[1]);

            String[] input = br.readLine().trim().split(" ");
            for(int i = 0 ; i < n ; i++){
                inpList.add(Integer.parseInt(input[i]));
            }

            Collections.sort(inpList);

            int q = Integer.parseInt(br.readLine());

            while (q > 0){
                String[] query = br.readLine().trim().split(" ");
                int a = Integer.parseInt(query[0]);
                int b = Integer.parseInt(query[1]);

                int i = bs1(a);
                int j = bs2(b);
                if(i == -1 || j == -1){
                    bw.write("0");
                } else {
                    bw.write((j-i+1)+"");
                }
                bw.newLine();
                q--;
            }
            inpList.clear();
            T--;
        }

        br.close();
        bw.close();
    }

    public static int bs1(int a){
        int low = 0;
        int high = n-1;

        while (low <= high){
            int mid = (low+high)/2;
            if(inpList.get(mid) < a){
                low = mid+1;
            } else {
                if((inpList.get(mid) > a || inpList.get(mid) == a ) && (mid == 0 || inpList.get(mid-1) < a)){
                    return mid;
                } else {
                    high = mid-1;
                }
            }
        }
        return -1;
    }

    public static int bs2(int b){
        int low = 0;
        int high = n-1;

        while (low <= high){
            int mid = (low+high)/2;
            if(inpList.get(mid) > b){
                high = mid-1;
            } else {
                if((inpList.get(mid) < b || inpList.get(mid) == b) && (mid == n-1 || inpList.get(mid+1) > b)){
                    return mid;
                } else {
                    low = mid+1;
                }
            }
        }

        return -1;
    }

}
