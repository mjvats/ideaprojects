import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

public class RearrangeNum {

    public static void main(String[] args) {

        int[] input2 = {9, 12, 23, 8, 5};
        int[] result = arrange(5, input2);
        for (int i = 0; i < input2.length; i++)
            System.out.print(input2[i] + " ");
    }

    private static int[] arrange(int input1, int[] input2){
        /*if(input2.length > input1){
            return new int[0];
        }

        ArrayList<Integer> list = new ArrayList<>();
        for(int i = 0 ; i < input2.length ; i++){
            list.add(input2[i]);
        }

        Collections.sort(list);

        int[] result = new int[list.size()];
        for(int i = 0, j = 0; i < list.size() && j < result.length ; i++, j= j+2){
            if()
        }*/

        if(input1 > input2.length){
            return null; 
        }
        Arrays.sort(input2) ;

        Vector v1 = new Vector(); // to insert even values
        Vector v2 = new Vector(); // to insert odd values

        for (int i = 0; i < input1; i++)
            if (input2[i] % 2 == 0)
                v1.add(input2[i]);
            else
                v2.add(input2[i]);

        int index = 0, i = 0, j = 0;

        boolean flag = false;

        // Set flag to true if first element is even
        if (input2[0] % 2 == 0)
            flag = true;

        // Start rearranging array
        while (index < input1)
        {

            // If first element is even
            if (flag == true)
            {
                input2[index] = (int)v1.get(i);
                i += 1 ;
                index += 1 ;
                flag = !flag;
            }

            // Else, first element is Odd
            else
            {
                input2[index] = (int)v2.get(j) ;
                j += 1 ;
                index += 1 ;
                flag = !flag;
            }
        }

        // Print the rearranged array
//        for (i = 0; i < input1; i++)
//            System.out.print(input2[i] + " ");

        return input2;
    }
}
