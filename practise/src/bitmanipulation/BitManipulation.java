package bitmanipulation;

public class BitManipulation {

    public static void main(String[] args) {
        int num = func(2,2);
        System.out.println(num);
    }

    //return a number which has x 1's and y 0's
    private static int func(int x, int y){
        int num = 0;
        int sum = x+y;
        while(sum > y){
            num += 1<<sum-1;
            sum--;
        }
        return num;
    }
}
