package bitmanipulation;

public class CheckSetBit {

    public static void main(String[] args) {
        System.out.println(checkBits(12, 5));
    }

    static boolean checkBits(long N, int i){
        return ((N >> i) & 1) == 1;
    }
}
