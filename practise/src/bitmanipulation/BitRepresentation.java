package bitmanipulation;

public class BitRepresentation {

    public static void main(String[] args) {
        //StringBuilder sb = new StringBuilder();
        System.out.println(recursion(12));
        //System.out.println(sb.reverse());
        //System.out.println("len:"+sb.length());
    }

    public static void print(int a){
        StringBuilder sb = new StringBuilder();
        while(a/2 > 0){
            sb.append(a%2);
            a = a/2;
        }
        sb.append(a%2);
        System.out.println(sb.reverse());
    }

    public static int recursion(int a){
        if(a/2 == 0){
            return (a%2);
        }
        System.out.print(recursion(a/2));
        return a%2;
    }

}
