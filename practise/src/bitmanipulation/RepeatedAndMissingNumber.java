package bitmanipulation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class RepeatedAndMissingNumber {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        String[] input = br.readLine().trim().split(" ");
        int[] arr = new int[input.length];
        for(int i = 0 ; i < input.length ; i++){
            arr[i] = Integer.parseInt(input[i]);
        }

        List<Integer> result = repeatedAndMissingNumber(arr);
        bw.write("repeated element:"+result.get(0)+"\n");
        bw.write("missing element:"+result.get(1));

        br.close();
        bw.close();
    }

    public static List<Integer> repeatedAndMissingNumber(int[] arr){

        int n = arr.length;
        int X = 0;
        for(int val : arr) X = X^val; // XOR all elements in input arr.
        for(int i = 1 ; i <= n ; i++) X = X^i; // XOR all natural numbers from 1->n.

        int i = 0; // finding the bit which is set for X.
        for( ; i < 32 ; i++){
            if(((X >> i) & 1) == 1) break;
        }

        int M = 0;
        int N = 0;
        for(int val : arr){
            if(((val >> i) & 1) == 1){
                M = M^val;
            }else{
                N = N^val;
            }
        }

        for(int k = 1 ; k <= n ; k++){
            if(((k >> i) & 1) == 1){
                M = M^k;
            }else{
                N = N^k;
            }
        }

        ArrayList<Integer> result = new ArrayList<>();
        for (int j : arr) {
            if (j == M) {
                result.add(M);
                result.add(N);
            } else if (j == N) {
                result.add(N);
                result.add(M);
            }
        }
        return result;
    }
}
