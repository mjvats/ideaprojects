import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;

public class AggressiveCows {

    public static void main(String[] args) throws java.lang.Exception {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {

            String[] input = br.readLine().trim().split(" ");
            int a = Integer.parseInt(input[0]);
            int b = Integer.parseInt(input[1]);

            ArrayList<Integer> A = new ArrayList<>();
            for (int i = 0; i < a; i++) {
                int val = Integer.parseInt(br.readLine());
                A.add(val);
            }
            int result = func(A, b);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int func(ArrayList<Integer> A, int B) {

        Collections.sort(A);

        int start = 0, end = A.get(A.size()-1);
        int max = Integer.MIN_VALUE;
        while (start <= end) {
            int mid = (start + end) / 2;
            if (verify(A, mid, B)) {
                max = Integer.max(max, mid);
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return max;
    }

    public static boolean verify(ArrayList<Integer> A, int mid, int B) {
        int count = 1;
        int left = 0;
        for (int right = left; right < A.size() && count < B; ) {
            if ((A.get(right) - A.get(left)) >= mid) {
                count++;
                left = right;
            } else {
                right++;
            }
        }
        if (count >= B) {
            return true;
        }
        return false;
    }
}
