import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class PositiveSpewing {

    static int[] A;
    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            String[] input = br.readLine().trim().split(" ");
            int N = Integer.parseInt(input[0]);
            int K = Integer.parseInt(input[1]);
            A = new int[N];
            String[] arr = br.readLine().trim().split(" ");
            for(int i = 0 ; i < N ; i++) A[i] = Integer.parseInt(arr[i]);

            bw.write(sol(N, K)+"");
            bw.write("\n");
            T--;
        }
        br.close();
        bw.close();
    }

    public static long sol(int N, int K){
        while(K > 0){

            for(int i = 0 ; i < N ; i++){
                if(A[i] > 0){
                    if(i == 0){
                        A[i+1]++;
                        A[N-1]++;
                    }else if(i == N-1){
                        A[0]++;
                        A[N-1]++;
                    }else{
                        A[i-1]++;
                        A[i+1]++;
                    }
                }
            }
            K--;
        }

        long sum = 0L;
        for(long val : A){
            sum += val;
        }
        return sum;
    }

}
