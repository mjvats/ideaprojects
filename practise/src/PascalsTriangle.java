import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PascalsTriangle {

    public static void main(String args[]){

        List<List<Integer>> result = generate(6);
        for(List list : result){
            for(Object val : list){
                System.out.print(val);
            }
            System.out.println();
        }
    }

    public static List<List<Integer>> generate(int numRows) {

        if(numRows <= 0){
            return null;
        }

        List<Integer> childList = new ArrayList<>(Arrays.asList(1));
        List<List<Integer>> parentList = new ArrayList<>();
        parentList.add(childList);

        if(numRows == 1)
            return parentList;

        int x = 2;
        while(x <= numRows){
            List<Integer> newList = new ArrayList<>();
            newList.add(0, 1);
            for(int i = 1; i < x-1; i++){
                newList.add(i, childList.get(i)+childList.get(i-1));
            }
            newList.add(x-1, 1);
            parentList.add(newList);
            childList = newList;
            x++;
        }
        return parentList;
    }
}
