public class MagicSquare {

    public static void main(String[] args) {
        int N = 4;
        int[] arr = new int[4];
        byte[] usd = new byte[4];
        func(arr, 4, 0, usd);
    }

    public static void func(int[] arr, int N, int idx, byte[] usd){
        if(idx == N){
            for(int val : arr){
                System.out.print(val);
            }
            System.out.println();
        }

        for(int i = 0 ; i < N ; i++){
            if(usd[i] == 0){
                arr[idx] = i+1;
                usd[i] = 1;
                func(arr, N, idx+1, usd);
                usd[i] = 0;
            }
        }
    }

}
