import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ValidPartition {

    static Set<String> set = new HashSet<>();
    static Integer numberOfPossibilities = 0;
    static Integer minNumberOfPossibilities = Integer.MAX_VALUE;

    public static void main(String[] args) {
        String[] L = {"smart", "inter", "int", "art", "sm", "views", "interviews"};
        String T = "smartinterviews";

        set.addAll(Arrays.asList(L));

        System.out.println(func(T, 0));
        numberOfWays(T, 0);
        System.out.println("Number of possibilities : "+numberOfPossibilities);
        minimumNumberOfWays(T, 0, 0);
        System.out.println("Minimum number of possibilities : "+minNumberOfPossibilities);
    }

    public static boolean func(String T, int idx){
        if(idx == T.length()){
            return true;
        }

        for(int j = idx+1 ; j <= T.length() ; j++){
            if(set.contains(T.substring(idx, j))){
                if(func(T, j)){
                    return true;
                }
            }
        }

        return false;
    }

    public static void numberOfWays(String T, int idx){
        if(idx == T.length()){
            numberOfPossibilities++;
        }

        for(int i = idx ; i <= T.length() ; i++){
            if(set.contains(T.substring(idx, i))){
                numberOfWays(T, i);
            }
        }
    }

    public static void minimumNumberOfWays(String T, int idx, int count){
        if(idx == T.length()){
            minNumberOfPossibilities = Math.min(minNumberOfPossibilities, count);
        }

        for(int i = idx ; i <= T.length() ; i++){
            if(set.contains(T.substring(idx, i))){
                minimumNumberOfWays(T, i, count+1);
            }
        }
    }
}
