import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class JavelinQualification {


    public static void main(String[] args) throws Exception
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());

        while(T > 0){

            String[] line1 = br.readLine().trim().split(" ");
            int N = Integer.parseInt(line1[0]);
            int M = Integer.parseInt(line1[1]);
            int X = Integer.parseInt(line1[2]);


            int[] A = new int[N];
            String[] line2 = br.readLine().trim().split(" ");
            for(int i = 0 ; i < line2.length ; i++)
            {
                A[i] = Integer.parseInt(line2[i]);
            }
            ArrayList<Integer> result = getQualifiers(A, N, M, X);
            for(int val : result)
            {
                bw.write(val + " ");
            }
            bw.write("\n");
            T--;

        }

        br.close();
        bw.close();
    }

    public static ArrayList<Integer> getQualifiers(int[] A, int N, int M, int X)
    {
        ArrayList<Integer> result = new ArrayList<>();
        int count = 0;
        int k = 1;
        for(int i = 0 ; i < A.length ; i++)
        {
            if (A[i] >= M)
            {
                A[i] = -1;
                count++;
                k++;
            }
        }
        if(count < X)
        {
            while(count < X)
            {
                int max = Integer.MIN_VALUE;
                int maxIndex = 0;
                for(int i = 0 ; i < A.length ; i++)
                {
                    if(A[i] > max)
                    {
                        maxIndex = i;
                        max = A[i];
                    }
                }
                A[maxIndex] = -1;
                count++;
            }
        }

        result.add(count);
        for(int i = 0 ; i < A.length ; i++)
        {
            if(A[i] == -1)
            {
                result.add(i+1);
            }
        }
        return result;
    }

}
