import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import jdk.nashorn.internal.runtime.JSONListAdapter;

public class PairWithDiff {

    static public void mergeSort(int arr[], int start, int end){
        if(start == end){
            return;
        }
        else{
            int mid = (start+end)/2;
            mergeSort(arr, start, mid);
            mergeSort(arr, mid+1, end);
            merge(arr, start, mid, end);
        }
    }

    static void merge(int arr[], int start, int mid, int end){
        int[] left = new int[mid-start+1];
        int[] right = new int[end-mid];

        int k = 0;
        for(int i = start ; i <= mid ; i++)
            left[k++] = arr[i];
        k = 0;
        for(int i = mid+1 ; i <= end ; i++)
            right[k++] = arr[i];

        k = start;
        int i = 0, j = 0;
        while(i < left.length && j < right.length){
            if(left[i] <= right[j]){
                arr[k] = left[i];
                i++;
            }
            else{
                arr[k] = right[j];
                j++;
            }
            k++;
        }

        while(i < left.length){
            arr[k] = left[i];
            i++;k++;
        }
        while(j < right.length) {
            arr[k] = right[j];
            j++;
            k++;
        }
    }

    static boolean pairDiff(int[] A, int K){

        mergeSort(A, 0, A.length-1);
        for(int i = 0, j = A.length-1 ; i < j ;){
            int diff = A[j]-A[i];
            if(diff == K){
                return true;
            }
            else if(diff < K){
                return false;
            }
            else{
                int diff1 = A[j-1] - A[i];
                int diff2 = A[j] - A[i+1];
                /*if(diff1 < K || diff2 < diff1){
                    i++;
                }
                else{
                    j--;
                }*/
                if(diff1 < K){
                    i++;
                }
                else if(diff2 < K){
                    j--;
                }
                else{
                    if(diff1 < diff2){
                        j--;
                    }
                    else{
                        i++;
                    }
                }
            }
        }

        return false;

    }

    public static void main(String args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.valueOf(br.readLine());

        while(T > 0){

            String[] line1 = br.readLine().trim().split(" ");
            int N = Integer.valueOf(line1[0]);
            int K = Integer.valueOf(line1[1]);
            int[] A = new int[N];
            String[] input = br.readLine().trim().split(" ");
            for(int i = 0 ; i < N; i++){
                A[i] = Integer.valueOf(input[i]);
            }
            bw.write(String.valueOf(pairDiff(A, K)));
            bw.write("\n");
            T--;

        }

        br.close();
        bw.close();
    }

    public static void main(String[] args) {
        Integer[] A = {-259, -825, 459, 825, 221, 870, 626, 934, 205, 783, 850, 398};
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(A));
        System.out.println(solve(list, -42));
    }

    public static int solve(ArrayList<Integer> A, int B) {
        if(B < 0){
            Collections.sort(A, new MyComparator1());
        }
        else{
            Collections.sort(A);
        }
        for(int j = A.size()-1, i = j-1 ; i >= 0 && j >= 0;){
            if(i == j){
                i--;
                continue;
            }
            int diff = A.get(j)-A.get(i);
            if(diff == B){
                return 1;
            }
            else if(diff < B){
                i--;
            }
            else{
                j--;
            }
        }
        return 0;
    }

}

class MyComparator1 implements Comparator<Integer>{
    public int compare(Integer I1, Integer I2){
        if(I1 < I2){
            return 1;
        }
        else if(I1 > I2){
            return -1;
        }
        else{
            return 0;
        }
    }
}
