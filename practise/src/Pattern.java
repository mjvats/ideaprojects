import java.io.BufferedWriter;
import java.io.Console;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class Pattern {

    public static void main(String[] args) throws IOException {

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        long startTime = System.currentTimeMillis();
        int T = 100;
        int count = 0;
        while (T > 0) {
            //int n = Integer.valueOf(br.readLine());
            count++;
            int n = 201;
            if (n > 0) {
                bw.write("Case #" + (count) + ":"+"\n");
                printDiamond(n, bw);
            }
            T--;
        }
        long endTime = System.currentTimeMillis();
        bw.write("Total time taken : " + (endTime - startTime) + "ms");
        bw.close();
    }

    static private void printDiamond(int n, BufferedWriter bw) throws IOException {
        int mid = n / 2;
        int i = mid, j = mid;
        while (i >= 0 && j <= n - 1) {
            print(i, j, n, bw);
            i--;
            j++;
        }
        i = i + 2;
        j = j - 2;
        while (i <= mid && j >= mid) {
            print(i, j, n, bw);
            i++;
            j--;
        }
    }

    static private void print(int i, int j, int n, BufferedWriter bw) throws IOException {
        //BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        int k = 0;
        while (k < i) {
            //System.out.print(" ");
            bw.write(" ");
            k++;
        }
        if (k == i) {
            //System.out.print("*");
            bw.write("*");
            k++;
        }
        while (k < j) {
            //System.out.print(" ");
            bw.write(" ");
            k++;
        }
        if (k == j) {
            //System.out.print("*");
            bw.write("*");
            k++;
        }
        while (k < n) {
            //System.out.print(" ");
            bw.write(" ");
            k++;
        }
        //System.out.println();
        bw.write("\n");
    }
}
