import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Equal {

    ArrayList<Integer> refList;
    public static void main(String[] args) {
/*        ArrayList<Integer> A = new ArrayList<>(Arrays.asList(1, 3, 3, 3, 3, 2, 2));
        ArrayList<Integer> result = new Equal().equal(A);
        for (int val : result) {
            System.out.print(val + ",");
        }*/

        for (int i = 0; i < 500; i++) {
            System.out.print("ab");
        }
    }

    public ArrayList<Integer> equal(ArrayList<Integer> A) {

        int n = A.size();
        Map<Integer, List<Integer>> map = new HashMap<>();

        ArrayList<ArrayList<Integer>> result = new ArrayList<>();
        for(int i = 0 ; i < n-1 ; i++)
        {
            int sum;
            for(int j = i+1 ; j < n ; j++)
            {
                sum = A.get(i)+A.get(j);
                if(map.containsKey(sum))
                {
                    List<Integer> value = map.get(sum);
                    for(int k = 0 ; k < value.size() ; k=k+2)
                    {
                        if(value.get(k) < value.get(k+1) && i < j && value.get(k) < i && value.get(k+1) != i && value.get(k+1) != j)
                        {
                            ArrayList<Integer> tempList = new ArrayList<>();
                            tempList.add(value.get(k));
                            tempList.add(value.get(k+1));
                            tempList.add(i);
                            tempList.add(j);

                            if(refList == null || isValid(tempList)) {
                                refList = tempList;
                                if(result.isEmpty()) result.add(refList);
                                else result.set(0, refList);
                            }
                        }
                    }
                    value.add(i);
                    value.add(j);
                }
                else
                {
                    map.put(sum, new ArrayList<>(Arrays.asList(i, j)));
                }
            }
        }
        return refList;
    }

    public boolean isValid(ArrayList<Integer> list)
    {
        for(int k = 0 ; k < 4 ; k++)
        {
            if(list.get(k) != refList.get(k))
            {
                if(list.get(k) < refList.get(k))
                    return true;
                break;
            }
        }
        return false;
    }

}
