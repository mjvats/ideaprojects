import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;

public class LexicographicalSubset {

    static InputStreamReader isr = new InputStreamReader(System.in);
    static OutputStreamWriter osw = new OutputStreamWriter(System.out);

    static BufferedReader br = new BufferedReader(isr);
    static BufferedWriter bw = new BufferedWriter(osw);

    public static void main(String[] args) throws IOException {

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            int n = Integer.parseInt(br.readLine());
            int[] arr = new int[n];
            String[] input = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                arr[i] = Integer.parseInt(input[i]);
            }
            mergeSort(arr, 0, n-1);
            boolean[] flag = new boolean[n];
            for(int i = 0 ; i < n ; i++){
                flag[i] = true;
            }
            func(arr, flag,0, n);
            bw.write("\n");
            T--;
        }
        br.close();
        bw.close();
    }

    public static void main(int[] args) throws IOException {
        int[] arr = {3, 8, 15, 23};
        boolean[] flag = new boolean[4];
        for(int i = 0 ; i < 4 ; i++){
            flag[i] = true;
        }
        func(arr, flag,0, 4);
    }

    static void func(int[] arr, boolean[] flag, int idx, int n) throws IOException {
        for(int i = idx ; i < n ; i++){
            for(int j = 0 ; j < idx ; j++){
                if(!flag[j]){
                    bw.write(arr[j]+" ");
                }
            }
            if(flag[i]){
                bw.write(arr[i]+"\n");
                flag[i] = false;
                if(i < n-1){
                    func(arr, Arrays.copyOf(flag, n), i+1, n);
                }
                flag[i] = true;
            }
        }
    }

    static void func(int[] arr, int n) throws IOException {
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i]+"\n");
            f(arr, i, i, i + 1, n);
        }
    }

    static void f(int[] arr, int p, int q, int m, int n) throws IOException {
        for (int i = m; i < n; i++) {
            for (int j = p; j <= q; j++) {
                System.out.print(arr[j] + " ");
            }
            System.out.print(arr[i]+"\n");
            f(arr, p, i, i + 1, n);
        }
    }

    public static void mergeSort(int[] arr, int start, int end){
        if(start < end){
            int mid = (start + end) / 2;
            mergeSort(arr, start, mid);
            mergeSort(arr, mid+1, end);
            merge(arr, start, end);
        }
    }

    public static void merge(int[] arr, int start, int end){
        int mid = (start + end)/2;
        int m = mid-start+1;
        int n = end-mid;
        int[] L = new int[m];
        int[] R = new int[n];
        int k = start;
        for(int i = 0; i < m ; i++) {
            L[i] = arr[k++];
        }

        for(int i = 0 ; i < n ; i++) {
            R[i] = arr[k++];
        }

        k = start;
        int i = 0 , j = 0;
        while (i < m && j < n){
            if(L[i] < R[j]){
                arr[k] = L[i];
                k++;
                i++;
            } else {
                arr[k] = R[j];
                k++;
                j++;
            }
        }

        while (i < m){
            arr[k] = L[i];
            k++;
            i++;
        }

        while (j < n){
            arr[k] = R[j];
            k++;
            j++;
        }
    }
}
