package MPL;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ObjectCreation {

    public static void main(String[] args) {
        //method1 : using new keyword
        ObjectCreation oc1 = new ObjectCreation();

        //method2 : using Class.forName()
        try{
            ObjectCreation oc2 = (ObjectCreation) Class.forName("MPL.ObjectCreation").newInstance();
        }
        catch (ClassNotFoundException e){
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        //method3 : using clone api. A new object can only be cloned from an existing object if
        // existing object was created using new keyword.
        try {
            ObjectCreation oc3 = (ObjectCreation) oc1.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        //method4 : using reflection api
        try {
            Constructor<ObjectCreation> constructor = ObjectCreation.class.getDeclaredConstructor();
            try {
                ObjectCreation oc4 = constructor.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

}
