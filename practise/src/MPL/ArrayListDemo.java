package MPL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ArrayListDemo {

    public static void main(String[] args) {
        List list = new ArrayList<>();

        SampleForList s1 = new SampleForList(1,"A", 26);
        SampleForList s2 = new SampleForList(2,"Z", 24);
        SampleForList s3 = new SampleForList(3,"B", 98);

        list.add(s1);
        list.add(s2);
        list.add(s3);

        Collections.sort(list, new SampleForList());
        for (Object s : list){
            System.out.println(((SampleForList)s).id);
        }
    }
}

class SampleForList implements Comparable, Comparator {
    int id;
    String name;
    int age;

    SampleForList(){

    }
    SampleForList(int id, String name, int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }

    @Override
    public int compareTo(Object o) {
        SampleForList s = (SampleForList) o;
        if(this.age > s.age){
            return 1;
        }
        else if(this.age < s.age){
            return -1;
        }
        else{
            return 0;
        }
    }

    @Override
    public int compare(Object o1, Object o2) {
        SampleForList s1 = (SampleForList) o1;
        SampleForList s2 = (SampleForList) o2;
        if(s1.age > s2.age)
            return -1;
        else if(s1.age < s2.age)
            return 1;
        else
            return 0;
    }
}
