package MPL;

import java.util.IdentityHashMap;
import java.util.Map;

public class HashMapDemo {

    public static void main(String[] args) {
        Map<Sample, Integer> map = new IdentityHashMap<>();
        Sample s1 = new Sample(2);
        Sample s2 = new Sample(3);
        Sample s3 = new Sample(3);

        map.put(s1, 22);
        map.put(s2, 33);
        map.put(s3, 44);

        for (Map.Entry<Sample, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }
    }

}

class Sample {

    int id;

    Sample(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sample sample = (Sample) o;
        return id == sample.id;
    }

    @Override
    public int hashCode() {
        return (id);
    }
}
