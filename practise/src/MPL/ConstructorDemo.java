package MPL;

public class ConstructorDemo {

    static int x = 9;
    protected ConstructorDemo(){
        super();
        System.out.println("Creating instance");
    }

    private ConstructorDemo(int val){
        System.out.println("Parameterized constructor");
    }

    public static void main(String[] args) {
        ConstructorDemo cd = new ConstructorDemo(5);
        System.out.println(cd.getClass().getName());

        ConstructorDemo cd1 = new ConstructorDemo();
        System.out.println();

        Tester t = new Tester();
        t.a = 5;

    }

    public void m1(){
        x = 10;
    }

}

class Tester{
    int a = 2;

    final static void m1(){

    }
}
