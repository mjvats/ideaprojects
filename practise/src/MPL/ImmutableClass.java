package MPL;

import java.util.ArrayList;

public final class ImmutableClass {

    final int x;

    final ArrayList<Integer> list = new ArrayList<>();

    public ImmutableClass(int x, int y){
        this.x = x;
        this.list.add(y);
    }

    public int getX(){
        return this.x;
    }
}

class ImmutableDemo{

    static {
        ImmutableClass ic = new ImmutableClass(1,2);
        ic.list.add(6);
    }
}
