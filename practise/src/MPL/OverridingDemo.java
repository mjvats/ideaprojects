package MPL;

public class OverridingDemo extends OverridingDemoParent {

    public static void m1(){
        System.out.println("m1 Method in child class");
    }

    @Override
    protected OverridingDemo m2() {
        System.out.println("m2 method in child");
        return null;
    }

    static public void main(String[] args) throws Exception {
        OverridingDemoParent o = new OverridingDemo();
        o.m2();
    }
}

class OverridingDemoParent{

    public static void m1(){
        System.out.println("m1 Method in parent class");
    }

    OverridingDemoParent m2() throws Exception{
        System.out.println("m2 method in parent");
        return null;
    }
}

