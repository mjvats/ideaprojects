package MPL;

public class OverLoadingDemo {

    static public void main(String args[]){
        System.out.println("main method with String args");
    }

    public static void main(int[] args) {
        System.out.println("main method with int args");
    }

    public void m1(){
        System.out.println("No arg method");
    }

    private static void m1(int x){
        System.out.println("1 arg method");
    }

}
