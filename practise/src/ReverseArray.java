import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReverseArray {

    public static void main(String[] args) {
        List<Integer> input = new ArrayList<Integer>(Arrays.asList(1,4,3,2));
        List<Integer> result = reverseArray(input);
        for (int val : result){
            System.out.print(val);
        }
    }

    public static List<Integer> reverseArray(List<Integer> a) {
        // Write your code here
        if(!validateInput(a)){
            return null;
        }

        List<Integer> result = new ArrayList<>();
        for(int i = a.size()-1 ; i >= 0 ; i--){
            if(a.get(i) < 1 || a.get(i) > 10000){
                return null;
            }
            System.out.println(a.get(i));
            result.add(a.get(i));
        }

        return result;
    }


    private static boolean validateInput(List<Integer> a){

        if(a.isEmpty() || a.size() < 1 || a.size() > 1000){
            System.out.println("validation failed");
            return false;
        }
        return true;
    }

}
