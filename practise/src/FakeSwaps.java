import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class FakeSwaps {

    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0)
        {
            int n = Integer.parseInt(br.readLine());
            String[] s = br.readLine().split("");
            String[] p = br.readLine().split("");
            bw.write(sol(s, p));
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();

    }

    public static String sol(String[] s, String[] p)
    {
        int n = s.length;
        int count = 0;
        int diffs = 0;
        int diffp = 0;
        for(int i = 0; i < n ; i++)
        {
            if(!s[i].equals(p[i]))
            {
                count++;
                diffs += Integer.parseInt(s[i]);
                diffp += Integer.parseInt(p[i]);
            }
        }

        if(count > 3 || Math.abs(diffp-diffs) == 3) return "NO";
        return "YES";
    }

}
