import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class TransposeOfMatrix {

    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        String[] line1 = br.readLine().trim().split(" ");
        int N = Integer.parseInt(line1[0]);
        int M = Integer.parseInt(line1[1]);

        int[][] matrix = new int[N][M];
        for(int i = 0 ; i < N ; i++)
        {
            String[] input = br.readLine().trim().split(" ");
            for(int j = 0 ; j < M ; j++)
            {
                matrix[i][j] = Integer.parseInt(input[j]);
            }
        }

        for(int j = 0 ; j < M ; j++)
        {
            for(int i = 0 ; i < N ; i++)
            {
                bw.write(matrix[i][j]+" ");
            }
            bw.write("\n");
        }

        br.close();
        bw.close();
    }

}
