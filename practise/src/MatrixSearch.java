import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MatrixSearch {

    public static void main(String[] args) {

        List<Integer> list1 = Arrays.asList(1, 3, 5, 7);
        List<Integer> list2 = Arrays.asList(10, 11, 16, 20);
        List<Integer> list3 = Arrays.asList(23, 30, 34, 50);

        List<List<Integer>> A = Arrays.asList(list1, list2, list3);
        int result = searchMatrix(A, 50);
        System.out.println(result);
    }

    public static int searchMatrix(List<List<Integer>> A, int B) {
        for(int i = 0 ; i < A.size() ; i++){
            List<Integer> tempList = A.get(i);
            if(tempList.get(0) <= B && tempList.get(tempList.size()-1) >= B){
                if(tempList.get(0) == B || tempList.get(tempList.size()-1) == B){
                    return 1;
                }
                else if(binarySearch(A.get(i), B, 0, A.get(i).size()-1) > -1){
                    return 1;
                }
                else {
                    return 0;
                }
            }

        }
        return 0;
    }

    private static int binarySearch(List<Integer> a, int b, int start, int end){
        if(start > end)
        {
            return -1;
        }
        else{
            int mid = (start + end)/2;
            if(a.get(mid) == b){
                return mid;
            }
            else if(a.get(mid) < b){
                return binarySearch(a, b, mid+1, end);
            }
            else{
                return binarySearch(a, b, start, mid-1);
            }
        }
    }
}
