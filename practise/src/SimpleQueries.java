import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SimpleQueries {

    static List<Integer> G = new ArrayList<>();

    public static void main(String[] args) {
        List<Integer> A = Arrays.asList(1, 2, 4);
        List<Integer> B = Arrays.asList(1, 2, 3, 4, 5, 6);

        ArrayList<Integer> result = solve(A, B);
        for (int val : result) {
            System.out.print(val + " ");
        }
    }

    public static ArrayList<Integer> solve(List<Integer> A, List<Integer> B) {

        for (int i = 0; i < A.size(); i++) {
            for (int j = i; j < A.size(); j++) {
                int max = getMax(A, i, j);
                G.add(max);
            }
        }

        for (int i = 0; i < G.size(); i++) {
            int divisorProd = getDivisorProd(G.get(i));
            G.set(i, divisorProd);
        }

        G.sort(new MyDivisorComparator());
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < B.size(); i++) {
            result.add(G.get(B.get(i) - 1));
        }
        G.clear();
        return result;
    }

    private static int getMax(List<Integer> A, int i, int j) {
        int max = Integer.MIN_VALUE;
        while (i <= j) {
            max = Integer.max(max, A.get(i));
            i++;
        }
        return max;
    }

    private static int getDivisorProd(int N) {
        long divisorProd = 1;
        for (int i = 1; i <= (int) Math.sqrt(N); i++) {
            if (N % i == 0) {
                if (N / i != i) {
                    divisorProd = (divisorProd*(N/i))%1000000007;
                }
                divisorProd = (divisorProd*i)%1000000007;
            }
        }
        return (int) divisorProd;
    }
}

class MyDivisorComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        if (o1 > o2) {
            return -1;
        } else if (o1 < o2) {
            return 1;
        } else {
            return 0;
        }
    }
}
