import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class OlympicsRanking {

    public static void main (String[] args) throws java.lang.Exception
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.valueOf(br.readLine());

        while(T > 0){

            String[] input = br.readLine().trim().split(" ");

            int G1 = Integer.valueOf(input[0]);
            int S1 = Integer.valueOf(input[1]);
            int B1 = Integer.valueOf(input[2]);
            int G2 = Integer.valueOf(input[3]);
            int S2 = Integer.valueOf(input[4]);
            int B2 = Integer.valueOf(input[5]);

            int teamA = G1+S1+B1;
            int teamB = G2+S2+B2;

            if(teamA > teamB)
            {
                bw.write(1+"");
            }
            else
            {
                bw.write(2+"");
            }
            bw.write("\n");
            T--;

        }

        br.close();
        bw.close();

    }
}
