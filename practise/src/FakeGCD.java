import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;

public class FakeGCD {

    public static void main(String[] args) throws IOException
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0)
        {
            int n = Integer.parseInt(br.readLine());
            int[] res = sol(n);
            if(res != null) for(int val : res) bw.write(val+" ");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();

    }

    public static int[] sol(int n)
    {
        int[] arr = new int[n];
        for(int i = 0 ; i < n ; i++) arr[i] = i+1;

        for(int i = 0 ; i < n-1 ; i++)
        {
            for(int j = i+1 ; j < n ; j++)
            {
                int[] temp = swap(arr, i , j);
                if(valid(temp)) {
                    return temp;
                }
            }
        }
        return null;
    }

    public static int[] swap(int[] arr, int i , int j)
    {
        int[] tempArr = Arrays.copyOf(arr, arr.length);
        int temp = tempArr[i];
        tempArr[i] = tempArr[j];
        tempArr[j] = temp;
        return tempArr;
    }

    public static boolean valid(int[] a)
    {
        int[] tempArr = Arrays.copyOf(a, a.length);
        for(int i = 0 ; i < tempArr.length ; i++)
        {
            tempArr[i] = (i+1)+tempArr[i];
        }
        int gcd = findGCD(tempArr);
        return gcd > 1;
    }

    static int gcd(int a, int b)
    {
        if (a == 0)
            return b;
        return gcd(b % a, a);
    }

    static int findGCD(int[] arr)
    {
        int result = 0;
        for (int element: arr){
            result = gcd(result, element);

            if(result == 1)
            {
                return 1;
            }
        }

        return result;
    }
}
