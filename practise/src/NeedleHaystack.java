public class NeedleHaystack {

    public static void main(String args[]) {
        String haystack = "a";
        String needle = "a";

        int index = getIndex(haystack, needle);
        System.out.println(index);
    }

    private static int getIndex(String haystack, String needle) {
        if (needle == null || needle.isEmpty() || haystack == null) {
            return 0;
        } else if (haystack.length() < needle.length()) {
            return -1;
        }

        boolean matched = false;
        int index = -1;
        for (int i = 0; i < haystack.length(); i++) {
            if (haystack.charAt(i) == needle.charAt(0)) {
                index = i;
                int k = i;
                matched = true;
                for (int j = 1; j < needle.length(); j++) {
                    k++;
                    while (k < haystack.length()) {
                        if (haystack.charAt(k) != needle.charAt(j)) {
                            index = -1;
                            matched = false;
                            break;
                        }
                        k++;
                    }
                }
                if (matched) {
                    break;
                }
            }
        }

        if (matched) {
            return index;
        }
        return -1;
    }
}
