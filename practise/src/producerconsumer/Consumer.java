package producerconsumer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Consumer implements Runnable{

    BlockingQueue<String> blockingQueue = null;

    Consumer(BlockingQueue blockingQueue){
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run(){
        try{
            System.out.println("Consumer taking from blocking queue.....");
            System.out.println(blockingQueue.poll(1500, TimeUnit.MILLISECONDS));
        }
        catch (InterruptedException e){

        }
    }
}
