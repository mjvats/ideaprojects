package producerconsumer;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ProducerConsumerDemo {

    public static void main(String args[]){
        System.out.println("Starting ProducerConsumerDemo...");
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);
        Producer producer = new Producer(blockingQueue);
        Consumer consumer = new Consumer(blockingQueue);

        Thread t1 = new Thread(producer);
        Thread t2 = new Thread(consumer);

        t1.start();
        t2.start();

        while(true){
           /* System.out.println("Producer thread : "+t1.getState());
            System.out.println("Consumer thread : "+t2.getState());*/
            System.out.println("Size of blocking queue : "+blockingQueue.size());
            System.out.println("Producer thread state : "+t1.getState());
            System.out.println("Consumer thread state : "+t2.getState());
            try{
                Thread.sleep(2000);
            }
            catch (InterruptedException e){

            }
        }
    }
}
