package producerconsumer;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {

    BlockingQueue<String> blockingQueue = null;

    Producer(BlockingQueue blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                System.out.println("Producer putting in blocking queue...");
                blockingQueue.put("time: " + System.currentTimeMillis());
                Thread.sleep(100);
            } catch (InterruptedException e) {

            }

        }
    }
}
