public class TowerOfHanoi {

    public static void main(String[] args) {
        System.out.println((int)Math.pow(2, 3)-1);
        func(3,'A', 'C', 'B');
    }

    private static void func(int n, char src, char dest, char temp){
        if(n == 0)
            return;

        func(n-1, src, temp, dest);
        System.out.println("Move "+n+" from "+src+" to "+dest);
        func(n-1, temp, dest, src);
    }

}
