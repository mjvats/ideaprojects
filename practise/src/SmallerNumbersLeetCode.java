import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class SmallerNumbersLeetCode {

    static int[] inputArr;
    static int[] ans;
    static int[] index;
    static int[] temp;

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            int n = Integer.parseInt(br.readLine());
            inputArr = new int[n];
            String[] line1 = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                inputArr[i] = Integer.parseInt(line1[i]);
            }
            List<Integer> result = sol(inputArr);
            int sum = 0;
            for (int val : result) {
                sum += val;
            }
            bw.write(sum + "");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static List<Integer> sol(int[] arr) {
        int n = arr.length;
        ans = new int[n];
        index = IntStream.range(0, n).toArray();
        temp = IntStream.range(0, n).toArray();
        mergeSort(arr, 0, arr.length - 1);
        List<Integer> result = new ArrayList<>();
        for (int val : ans) {
            result.add(val);
        }
        return result;
    }

    public static void mergeSort(int[] arr, int start, int end) {
        if (start < end) {
            int mid = (start + end) / 2;
            mergeSort(arr, start, mid);
            mergeSort(arr, mid + 1, end);
            merge(arr, start, end);
        }
    }

    public static void merge(int[] arr, int start, int end) {

        int mid = (start + end) / 2;
        int i = start;
        int j = mid + 1;
        int k = start;

        while (i <= mid && j <= end) {
            if (arr[temp[j]] < arr[temp[i]]) {
                ans[temp[i]] += end - j + 1;
                index[k] = temp[i];
                k++;
                i++;
            } else {
                index[k] = temp[j];
                k++;
                j++;
            }
        }

        while (i <= mid) {
            index[k] = temp[i];
            k++;
            i++;
        }

        while (j <= end) {
            index[k] = temp[j];
            k++;
            j++;
        }

        while (start <= end) {
            temp[start] = index[start];
            start++;
        }
    }


}
