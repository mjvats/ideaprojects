public class NetworkRank {

    public static void main(String[] args) {

        int[] A = {1,2,11,5};
        int[] B = {2,3,5,6};

        int result = solution(A, B,10);
        System.out.println(result);
    }

    public static int solution(int[] A, int[] B, int N) {
        // write your code in Java SE 8
        if(A.length == 0 || B.length == 0)
            return 0;
        if(!validateInput(A, B, N))
            return -1;

        else {
            int M = A.length;
            int maxNum = 0;
            for(int i = 0 ; i < A.length ; i++){
                int temp = A[i] > B[i] ? A[i] : B[i];
                maxNum = temp > maxNum ? temp : maxNum;
            }

            int[][] m = new int[maxNum][maxNum];

            for(int i = 0 ; i < A.length ; i++){
                m[A[i]-1][B[i]-1] = 1;
                m[B[i]-1][A[i]-1] = 1;
            }

            int maxVal = 0;
            int count = 0;
            for(int i = 0 ; i < m.length ; i++){
                for(int j = 0 ; j < m[0].length ; j++){
                    if(m[i][j] == 1){
                        int nextCity = j;
                        count++;
                        m[j][i] = -1;
                        for(int k = 0 ; k < m[0].length ; k++){
                            if(k == j)
                                continue;
                            else {
                                if(m[i][k] == 1 || m[i][k] == -1)
                                    count++;
                            }
                        }
                        for(int k = 0 ; k < m[0].length ; k++){
                            if(k == i)
                                continue;
                            else{
                                if(m[j][k] == 1 || m[j][k] == -1)
                                    count++;
                            }
                        }
                        maxVal = count > maxVal ? count : maxVal;
                    }
                    count = 0;
                }
            }

            return maxVal;

        }

    }

    private static boolean validateInput(int[] A, int[] B, int N){
        if(N < 2 || N > 100)
            return false;
        else if (A.length != B.length)
            return false;
        else if (A.length < 1 || A.length > 4950)
            return false;
        else {
            for(int i = 0 ; i < A.length ; i++){
                if(A[i] == B[i] || A[i] < 1 || A[i] > N || B[i] < 1 || B[i] > N)
                    return false;
            }
            return true;
        }
    }
}
