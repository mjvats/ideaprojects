import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ConsecutiveSetBits {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.valueOf(br.readLine());

        while(T > 0){

            long N = Long.valueOf(br.readLine());
            bw.write(String.valueOf(consecutivesetbits(N)));
            bw.write("\n");
            T--;

        }

        br.close();
        bw.close();

    }

    public static int consecutivesetbits(long N)
    {
        int max = 0;
        int count = 0;
        for(int i = 0 ; i < 64 ; i++)
        {
            if(((N >> i) & 1) == 1)
            {
                count++;
            }
            else
            {
                if(count > max)
                {
                    max = count;
                }
                count = 0;
            }
        }
        return max;
    }

}
