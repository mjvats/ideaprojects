package queue;

import java.util.HashMap;
import java.util.Map;

public class FirstNonRepeatingCharacter {

    public static void main(String[] args) {
        String input = "";
        String result = solve(input);
        System.out.println(result);
    }

    public static String solve(String A) {
        Map<Character, Integer> map = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        int n = A.length();
        int k = 0;
        for(int i = 0 ; i < n ; i++){
            char c = A.charAt(i);
            if(map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            }else {
                map.put(c, 1);
            }

            boolean flag = false;
            while(k <= i){
                if(map.containsKey(A.charAt(k)) && map.get(A.charAt(k)) == 1){
                    flag = true;
                    break;
                }
                else {
                    k++;
                }
            }
            if(flag) sb.append(A.charAt(k));
            else sb.append("#");
        }
        return sb.toString();
    }

}
