package queue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class QueueDemo {

    static int[] arr;
    static int f;
    static int r;
    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        arr = new int[T];
        f = -1;
        r = -1;
        while(T > 0){
            String[] input = br.readLine().trim().split(" ");
            String operation = input[0];
            if(operation.equalsIgnoreCase("Enqueue"))
            {
                enqueue(Integer.parseInt(input[1]));
            }
            else
            {
                bw.write(dequeue());
                bw.write("\n");
            }
            T--;
        }

        br.close();
        bw.close();
    }

    public static void enqueue(int a)
    {
        if(f == -1) r++;
        arr[++f] = a;
    }

    public static String dequeue()
    {
        if(r > -1)
        {
            String val = String.valueOf(arr[r]);
            if(r == f)
            {
                f = -1;
                r = -1;
            }
            else r++;
            return val;

        }
        else return "Empty";
    }
}
