package queue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class KillingDragons {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            int n = Integer.parseInt(br.readLine());
            int[] d = new int[n];
            int[] v = new int[n];

            String[] line1 = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                d[i] = Integer.parseInt(line1[i]);
            }

            String[] line2 = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                v[i] = Integer.parseInt(line2[i]);
            }

            int result = dungeon(d, v);
            bw.write(result + "");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }


    public static int dungeon(int[] dragons, int[] vessel) {
        int n = dragons.length;

        int carry = 0;
        int deficit = 0;
        int position = 0;

        for (int i = 0; i < n; i++) {
            int sum = (vessel[i] - dragons[i]) + carry;
            if (sum > 0) {
                carry = sum;
            } else {
                carry = 0;
                deficit += Math.abs(sum);
                if(deficit > 0) position = i + 2;
            }
        }

        if (deficit > carry) {
            return -1;
        } else if (position == 0) {
            return position+1;
        } else {
            return position;
        }
    }

}
