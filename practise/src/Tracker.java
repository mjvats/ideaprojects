import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;

public class Tracker {

    Map<String, TempNode> map = new HashMap<>();

    public static void main(String[] args) {
        Tracker t = new Tracker();
        String[] queries = {"A ic",
            "D ic1",
            "A yamjbcvdez",
            "D yamjbcvdez1",
            "A ic",
            "A ic",
            "A ic",
            "D ic2",
            "A yamjbcvdez",
            "D ic3",
            "D yamjbcvdez1",
            "A ic",
            "A ic",
            "A ic",
            "D ic2",
            "D ic1",
            "A ic"};
        List<String> result = t.hostAllocation(queries);
        for(String val : result){
            System.out.println(val);
        }
    }

    List<String> hostAllocation(String[] queries) {
        Tracker tracker = new Tracker();
        List<String> ans = new ArrayList<>();
        for (String query : queries) {
            String[] task = query.split(" ");
            if (task[0].equals("A")) {
                ans.add(tracker.allocate(task[1]));
            }
            if (task[0].equals("D")) {
                tracker.deallocate(task[1]);
            }
        }
        return ans;
    }

    String allocate(String hostName){
        int hostNumber = 0;
        if(map.containsKey(hostName)){
            TempNode t = map.get(hostName);
            HashSet<Integer> set = t.set;
            HashSet<Integer> slots = t.slots;

            if(slots.isEmpty()){
                t.count = t.count+1;
                hostNumber = t.count;
                set.add(hostNumber);
            } else {
                int x = -1;
                for(Integer val : slots){
                    set.add(val);
                    x = val;
                    hostNumber = val;
                    break;
                }
                slots.remove(x);
            }
        } else {
            TempNode node = new TempNode();
            node.count = node.count+1;
            hostNumber = node.count;
            node.set.add(hostNumber);
            map.put(hostName, node);
        }

        return hostName+hostNumber;
    }

    void deallocate(String hostName){
        int n = hostName.length();
        String[] arr = hostName.split("");
        int hostNumber = Integer.parseInt(arr[n-1]);
        TempNode node = map.get(hostName.substring(0, n-1));
        if(node != null){
            node.slots.add(hostNumber);
        }
    }

}

class TempNode{
    HashSet<Integer> set = new HashSet<>();
    HashSet<Integer> slots = new HashSet<>();
    int count = 0;
}
