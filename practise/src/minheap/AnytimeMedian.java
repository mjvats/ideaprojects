package minheap;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class AnytimeMedian {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            Integer.parseInt(br.readLine());
            ArrayList<Integer> inpList = new ArrayList<>();
            String[] input = br.readLine().trim().split(" ");
            for(String val : input){
                inpList.add(Integer.parseInt(val));
            }

            ArrayList<Integer> result = median(inpList);
            for(int val : result){
                bw.write(val+" ");
            }
            bw.newLine();
            T--;
        }

        br.close();
        bw.close();
    }

    public static ArrayList<Integer> median(ArrayList<Integer> arr){
        ArrayList<Integer> result = new ArrayList<>();
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(Comparator.reverseOrder());

        int n = arr.size();
        if(n == 1){
            result.add(arr.get(0));
            return result;
        }

        minHeap.add(Integer.max(arr.get(0), arr.get(1)));
        maxHeap.add(Integer.min(arr.get(0), arr.get(1)));
        result.add(arr.get(0));
        result.add(maxHeap.peek());
        for (int i = 2 ; i < n ; i++) {
            int j = arr.get(i);
            if (!minHeap.isEmpty() && j < minHeap.peek()) {
                maxHeap.add(j);
            } else {
                minHeap.add(j);
            }

            if (maxHeap.size() > minHeap.size()) {
                result.add(maxHeap.peek());
                minHeap.add(maxHeap.poll());
            } else if (minHeap.size() > maxHeap.size()) {
                result.add(minHeap.peek());
                maxHeap.add(minHeap.poll());
            } else {
                result.add(maxHeap.peek());
            }
        }

        return result;
    }

}
