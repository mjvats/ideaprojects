package minheap;

public class MaxHeap {

    int[] heap;
    int f;
    int size;

    MaxHeap(int size) {
        this.size = size;
        this.heap = new int[size];
        f = -1;
    }

    public int parent(int i){
        if(i > 0){
            return (2*i - 1)/2;
        }
        return -1;
    }

    public int leftChild(int i){
        int left = 2*i+1;
        if(left <= f){
            return left;
        }
        return -1;
    }

    public int rightChild(int i){
        int right = 2*i+2;
        if(right <= f){
            return right;
        }
        return -1;
    }

    public void insert(int val) {
        heap[++f] = val;
        heapify(f);
    }

    public void delMax() {
        if (f >= 0) {
            swap(0, f);
            f--;
            percolateDown(0);
        }
    }

    public String getMax() {
        if (f == -1) {
            return "Empty";
        }
        int max = heap[0];
        return String.valueOf(max);
    }

    public void heapify(int index) {
        if (index >= 0) {
            int parent = (index - 1) >> 1;
            while (parent >= 0 && (heap[index] > heap[parent])) {
                swap(index, parent);
                index = parent;
                if(index == 0) break;
                parent = (index - 1) >> 1;
            }
        }
    }

    public void percolateDown(int i){
        int left = leftChild(i);
        int right = rightChild(i);

        if(left == -1 && right == -1){
            return;
        }

        if(left != -1 && heap[left] > heap[i]){
            swap(left, i);
            percolateDown(left);
        }

        if(right != -1 && heap[right] > heap[i]){
            swap(right, i);
            percolateDown(right);
        }

    }

    public void swap(int i, int j) {
        int temp = heap[i];
        heap[i] = heap[j];
        heap[j] = temp;
    }

}
