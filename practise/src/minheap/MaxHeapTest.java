package minheap;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MaxHeapTest {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        MaxHeap heap = new MaxHeap(T + 10);
        while (T > 0) {

            String[] input = br.readLine().trim().split(" ");
            String operation = input[0];
            if (operation.equals("insert")) {
                heap.insert(Integer.parseInt(input[1]));
            } else if (operation.equals("getMax")) {
                bw.write(heap.getMax());
                bw.write("\n");
            } else {
                heap.delMax();
            }
            T--;
        }

        br.close();
        bw.close();
    }

}
