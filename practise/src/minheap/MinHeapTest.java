package minheap;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.PriorityQueue;

public class MinHeapTest {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        MinHeap heap = new MinHeap(T + 10);
        while (T > 0) {

            String[] input = br.readLine().trim().split(" ");
            String operation = input[0];
            if (operation.equals("insert")) {
                heap.insert(Integer.parseInt(input[1]));
            } else if (operation.equals("getMin")) {
                bw.write(heap.getMin());
                bw.write("\n");
            } else {
                heap.delMin();
            }
            T--;
        }

        br.close();
        bw.close();
    }

    public static void main(int[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        while (T > 0) {

            String[] input = br.readLine().trim().split(" ");
            String operation = input[0];
            if (operation.equals("insert")) {
                pq.add(Integer.parseInt(input[1]));
            } else if (operation.equals("getMin")) {
                if(pq.isEmpty()){
                    bw.write("Empty");
                }else{
                    bw.write(String.valueOf(pq.peek()));
                }
                bw.write("\n");
            } else {
                if(!pq.isEmpty()){
                    pq.poll();
                }
            }
            T--;
        }

        br.close();
        bw.close();
    }
}