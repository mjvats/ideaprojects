import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ObtainTheSum {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            String[] input = br.readLine().trim().split(" ");
            long N = Long.parseLong(input[0]);
            long S = Long.parseLong(input[1]);
            bw.write(sum(N, S)+"");
            bw.write("\n");
            T--;
        }
        br.close();
        bw.close();
    }

    public static long sum(long N, long S){

        long netSum = (N *(N+1))/2;
        long low = 1;
        long high = N;

        while(low <= high){
            long mid = (low+high)/2;
            long left = ((mid-1)*mid)/2;
            long right = netSum-(left+mid);
            long currSum = (left+right);
            if( currSum == S){
                return mid;
            }else if(currSum < S){
                high = mid-1;
            }else{
                low = mid+1;
            }
        }
        return -1;
    }

}
