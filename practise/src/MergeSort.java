public class MergeSort {

     static public void main(String[] args) {
        int[] input = {-30, 15, 20, 10, -10 };;
        mergeSort(input, 0, input.length-1);
        for(int i = 0 ; i < input.length ; i++){
            System.out.print(input[i]+" ");
        }
    }

    static public void mergeSort(int arr[], int start, int end){
        if(start == end){
            return;
        }
        else{
            int mid = (start+end)/2;
            mergeSort(arr, start, mid);
            mergeSort(arr, mid+1, end);
            merge(arr, start, mid, end);
        }
    }

    static void merge(int arr[], int start, int mid, int end){
        int[] left = new int[mid-start+1];
        int[] right = new int[end-mid];

        int k = 0;
        for(int i = start ; i <= mid ; i++)
            left[k++] = arr[i];
        k = 0;
        for(int i = mid+1 ; i <= end ; i++)
            right[k++] = arr[i];

        k = start;
        int i = 0, j = 0;
        while(i < left.length && j < right.length){
            if(left[i] <= right[j]){
                arr[k] = left[i];
                i++;
            }
            else{
                arr[k] = right[j];
                j++;
            }
            k++;
        }

        while(i < left.length){
            arr[k] = left[i];
            i++;k++;
        }
        while(j < right.length) {
            arr[k] = right[j];
            j++;
            k++;
        }
    }
}
