import java.util.ArrayList;

public class MergeSortedList {

    public static void main(String[] args) {
        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(-4);
        list1.add(-3);
        list1.add(0);
        ArrayList<Integer> list2 = new ArrayList<>();
        list2.add(2);

        merge(list1, list2);
        for (int val : list1) {
            System.out.println(val);
        }

    }

    public static void merge(ArrayList<Integer> a, ArrayList<Integer> b) {

        int i = 0, j = 0;
        while (i < a.size() && j < b.size()) {
            if (a.get(i) < b.get(j)) {
                i++;
            } else {
                a.add(i, b.get(j));
                j++;
            }
        }

        while (j < b.size()) {
            a.add(i, b.get(j));
            j++;
        }

    }
}
