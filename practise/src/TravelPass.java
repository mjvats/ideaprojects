import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class TravelPass {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while(T > 0){
            String[] line1 = br.readLine().trim().split(" ");
            int N = Integer.parseInt(line1[0]);
            int A = Integer.parseInt(line1[1]);
            int B = Integer.parseInt(line1[2]);

            String[] line2 = br.readLine().trim().split("");
            int[] arr = new int[N];
            for(int i = 0 ; i < N ; i++) arr[i] = Integer.parseInt(line2[i]);

            int result = func(arr, A, B);
            bw.write(result+"");
            bw.write("\n");
            T--;
        }

        br.close();
        bw.close();
    }

    public static int func(int[] arr, int A, int B){
        int time = 0;
        for(int val : arr){
            if(val == 1) time += B;
            else time += A;
        }
        return time;
    }
}
