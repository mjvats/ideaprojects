public class MinSubArraySum {

    public static void main(String args[]){
        int[] input = {1,2,3,4,5};
        System.out.println(new MinSubArraySum().minSubArrayLen(11, input));
    }

    public int minSubArrayLen(int target, int[] nums) {
        if(!validateInput(target, nums)){
            return 0;
        }

        int sum = 0;
        int length = 0;

        for(int i = -1, j = -1 ; i < nums.length && i >= j;){
            if(sum < target && i < nums.length-1){
                sum += nums[++i];
            }
            else if((sum > target || sum == target) && j < i){
                int temp = (i-j);
                length = (length == 0 || temp < length) ? temp : length;
                sum -= nums[++j];
            }
            else{
                break;
            }
        }
        return length;
    }

    private boolean validateInput(int target, int[] nums){

        if(target < 1 || target > 1000000000 || nums.length < 1 || nums.length > 100000){
            return false;
        }
        return true;
    }

}
