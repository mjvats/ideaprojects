public class StringReverse {

    public static void main(String args[]){
        char c1 = 'a';
        char c2 = 'b';
        int c = c1^c2;
        c1 = (char)(c^c1);
        c2 = (char)(c^c2);

        System.out.println("c1 = " + c1);
        System.out.println("c2 = " + c2);
    }
}
