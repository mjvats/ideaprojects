public class CheckForSpace {

    public static void main(String[] args) {
        String s1 = "AEPY2giL";
        System.out.println(new CheckForSpace().reverseWords(s1));
    }

    public String reverseWords(String s) {
        if(!validateInput(s)){
            return "";
        }

        String[] strArr = s.split(" ");
        boolean isWord = false;
        String result = "";
        for(int i = strArr.length-1 ; i >= 0 ; i--){
            if(!strArr[i].isEmpty() && strArr[i] != " "){
                isWord = true;
                result += strArr[i] + " ";
                //System.out.println(result);
            }
        }
        if(!isWord)
            return "";
        return result.trim();
    }

    private boolean validateInput(String s){
        if(s.length() < 1 || s.length() > 10000){
            return false;
        }
        char[] strArr = s.toCharArray();
        for(int i = 0 ; i < strArr.length ; i++){
            if(strArr[i] > 47 && strArr[i] < 58){
                return true;
            }
            else if(strArr[i] != 32 && (strArr[i] < 65 || strArr[i] > 122 || (strArr[i] > 90 && strArr[i] < 97))){
                return false;
            }
        }
        return true;
    }

}
