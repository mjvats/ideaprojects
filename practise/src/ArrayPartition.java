public class ArrayPartition {

    public static void main(String args[]){
        int[] numbers = {2,7,11,15};
        int target = 17;

        int[] result = twoSum(numbers, target);
        System.out.println(result[0]+" "+result[1]);
    }
    public static int[] twoSum(int[] numbers, int target) {
        if(!validateInput(numbers, target))
            return new int[0];

        int[] result = new int[2];
        for(int i = 0 ; i < numbers.length ; i++){
            int temp = target - numbers[i];
            int startIndex = 0;
            int endIndex = numbers.length - 1;

            while(startIndex <= endIndex){
                int midIndex = (startIndex + endIndex)/2;
                if(temp == numbers[midIndex]){
                    result[0] = i+1;
                    result[1] = midIndex+1;
                    return result;
                }
                else if (temp > numbers[midIndex]){
                    startIndex = midIndex+1;
                }
                else{
                    endIndex = midIndex-1;
                }
            }

        }

        return result;
    }

    private static boolean validateInput(int[] numbers, int target){
        if(numbers.length < 1 || numbers.length > 30000 || target  < -1000 || target > 1000){
            return false;
        }

        //Below code will check if input array is sorted throughout in increasing order or not.
        for(int i = 0 ; i < numbers.length-1 ; i++){
            if(numbers[i+1] < numbers[i])
                return false;
        }

        return true;
    }

}
