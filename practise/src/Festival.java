import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;

public class Festival {

    static Map<Integer, PriorityQueue<Integer>> map;

    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        int count = 1;
        while (count <= T) {
            map = new HashMap<>();
            String[] line1 = br.readLine().trim().split(" ");
            int D = Integer.parseInt(line1[0]);
            int N = Integer.parseInt(line1[1]);
            int K = Integer.parseInt(line1[2]);

            while (N > 0) {
                String[] input = br.readLine().trim().split(" ");
                int h = Integer.parseInt(input[0]);
                int s = Integer.parseInt(input[1]);
                int e = Integer.parseInt(input[2]);
                for (int i = s; i <= e; i++) {
                    if (map.containsKey(i)) {
                        PriorityQueue<Integer> pq = map.get(i);
                        pq.add(h);
                    } else {
                        PriorityQueue<Integer> pq = new PriorityQueue<>((o1, o2) -> o2 - o1);
                        pq.add(h);
                        map.put(i, pq);
                    }
                }
                N--;
            }

            long sum = 0L;
            for (Entry<Integer, PriorityQueue<Integer>> entry : map.entrySet()) {
                PriorityQueue<Integer> pq = entry.getValue();
                if (!pq.isEmpty()) {
                    long tempSum = 0L;
                    for (int i = 0; !pq.isEmpty() && i < K; i++) {
                        tempSum += pq.poll();
                    }
                    sum = Math.max(sum, tempSum);
                }
            }

            bw.write("Case #" + count + ": " + sum);
            bw.write("\n");
            count++;
        }

        br.close();
        bw.close();
    }

}

