import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class FirstRepeatingElement {

    public static void main(String[] args) {

        List<Integer> list = Arrays.asList(6, 10, 5, 4, 9, 120);
        int result = new FirstRepeatingElement().solve(list);
        System.out.println(result);
    }

    public int solve(List<Integer> A) {

        HashMap<Integer, Integer> map = new HashMap<>();
        for(int val : A)
        {
            if(map.containsKey(val)) map.replace(val, map.get(val)+1);
            else map.put(val, 1);
        }

        for(int val : A) if(map.containsKey(val) && map.get(val) > 1) return val;
        return -1;
    }

}
