import java.util.Arrays;
import java.util.List;

public class SubsetSumIb {

    public static void main(String[] args)
    {
        List<Integer> A = Arrays.asList(68, 35, 1, 70, 25, 79, 59, 63, 65, 6, 46);
        int B = 282;
        int result = solve(A, B);
        System.out.println(result);
    }

    public static int solve(List<Integer> A, int B)
    {

        int[][] M = new int[A.size()+1][B+1];
        for(int i = 0 ; i <= A.size() ; i++)
        {
            M[i][0] = 1;
        }

        for(int i = 1 ; i <= A.size() ; i++)
        {
            for(int j = 1; j <= B ; j++)
            {
                if(A.get(i-1) == j)
                {
                    M[i][j] = 1;
                }
                else if(A.get(i-1) > j)
                {
                    M[i][j] = M[i-1][j];
                }
                else
                {
                    M[i][j] = Integer.max(M[i-1][j], M[i-1][j-A.get(i-1)]);
                }

                if(j==B && M[i][j] == 1)
                {
                    return 1;
                }
            }
        }
        return 0;
    }

}
