import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;

public class Solution {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());
        while (T > 0) {
            BinarySearchTree bst = new BinarySearchTree();
            int n = Integer.parseInt(br.readLine());
            int[] a = new int[n];

            String[] input = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(input[i]);
            }
            for (int i = 0; i < n; i++) {
                bst.add(a[i]);
            }
            bst.levelOrderBottomUp(bw);
            bw.write("\n");
            bw.write("\n");
            T--;
        }
        br.close();
        bw.close();
    }
}

class Node {

    int data;
    Node left;
    Node right;

    Node(int data) {
        this.data = data;
        this.left = null;
        this.right = null;
    }
}

class ListNode<E> {

    Object data;
    ListNode<E> next;
    ListNode<E> prev;

    ListNode(Object data) {
        this.data = data;
        this.next = null;
        this.prev = null;
    }
}

class StackDll<E> {

    private ListNode<E> root;
    private ListNode<E> last;

    public StackDll() {
        this.root = null;
        this.last = null;
    }

    public void push(E data) {
        if (isEmpty()) {
            this.root = new ListNode<E>(data);
            this.last = root;
        } else {
            ListNode<E> node = new ListNode<E>(data);
            node.prev = this.last;
            this.last.next = node;
            this.last = node;
        }
    }

    public E pop() {
        if (!isEmpty() && this.last != null) {
            E val = (E) this.last.data;
            if (this.last == this.root) {
                this.root = null;
            }
            this.last = this.last.prev;
            if (this.last != null) {
                this.last.next = null;
            }
            return val;
        }
        return null;
    }

    public E peek() {
        if (!isEmpty() && this.last != null) {
            return (E) this.last.data;
        }
        return null;
    }

    public boolean isEmpty() {
        return this.last == this.root && this.last == null;
    }

}

class BinarySearchTree {

    private Node root;

    BinarySearchTree() {
        this.setRoot(null);
    }

    public Node getRoot() {
        return this.root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public Node add(int data) {
        if (this.root == null) {
            this.root = new Node(data);
            return this.root;
        }
        return add(this.root, data);
    }

    private Node add(Node root, int data) {
        if (root == null) {
            root = new Node(data);
            return root;
        } else {
            if (data < root.data) {
                root.left = add(root.left, data);
            } else {
                root.right = add(root.right, data);
            }
            return root;
        }
    }

    public void levelOrderBottomUp(BufferedWriter bw) throws IOException {
        if (this.root != null) {
            levelOrderBottomUp(this.root, bw);
        }
    }

    public void levelOrderBottomUp(Node root, BufferedWriter bw) throws IOException {
        if (root == null) {
            return;
        }
        Queue<Node> q = new LinkedList<>();
        StackDll<Node> s = new StackDll<Node>();
        q.add(root);
        q.add(null);
        while (!q.isEmpty()) {
            Node node = q.poll();
            if (node != null) {
                if (node.right != null) {
                    q.add(node.right);
                }
                if (node.left != null) {
                    q.add(node.left);
                }
            } else {
                if (q.isEmpty()) {
                    break;
                } else {
                    q.add(null);
                }

            }
            s.push(node);
        }

        while (!s.isEmpty()) {
            Node node = s.pop();
            if (node == null) {
                bw.write("\n");
            } else {
                bw.write(node.data + " ");
            }
        }
    }
}