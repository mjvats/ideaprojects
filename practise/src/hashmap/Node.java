package hashmap;

public class Node {

    int val;
    Node left;
    Node right;
    int count;

    Node(int val){
        this.val = val;
        this.left = null;
        this.right = null;
        this.count = 1;
    }

}
