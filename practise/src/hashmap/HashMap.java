package hashmap;

public class HashMap {

    Node[] hashtable;
    int size;
    HashMap(int size) {
        hashtable = new Node[size];
        this.size = 0;
    }

    void insert(int x)
    {
        int index = hashCode(x);
        Node newNode = new Node(x);
        if (hashtable[index] == null)
        {
            hashtable[index] = newNode;
        } else {
            Node existingNode = hashtable[index];
            while (existingNode != null)
            {
                if (newNode.val == existingNode.val)
                {
                    existingNode.count++;
                    return;
                }
                else if (newNode.val < existingNode.val)
                {
                    existingNode = existingNode.left;
                }
                else
                {
                    existingNode = existingNode.right;
                }
            }
        }
    }

    void delete(int x) {

    }

    boolean search(int x) {
        int index = hashCode(x);
        Node nodeAtIndex = hashtable[index];
        while (nodeAtIndex != null){
            if(nodeAtIndex.val == x)
            {
                return true;
            }
            else if (x < nodeAtIndex.val)
            {
                nodeAtIndex = nodeAtIndex.left;
            }
            else
            {
                nodeAtIndex = nodeAtIndex.right;
            }
        }
        return false;
    }

    int size() {
        for(int i = 0 ; i < hashtable.length ; i++)
        {
            if(hashtable[i] != null)
            {
                Node root = hashtable[i];
                getSize(root);
            }
        }
        return size;
    }

    private int hashCode(int x) {
        return x % (hashtable.length);
    }

    private void getSize(Node root)
    {
        if(root == null)
        {
            return;
        }
        size++;
        getSize(root.left);
        getSize(root.right);
    }
}