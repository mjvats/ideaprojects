package hashmap;

public class HashMapDemo {

    public static void main(String[] args) {
        HashMap hashMap = new HashMap(100);
        hashMap.insert(5);
        hashMap.insert(805);
        hashMap.insert(205);
        hashMap.insert(505);
        hashMap.insert(605);
        hashMap.insert(1205);
        hashMap.insert(6);
        hashMap.insert(806);
        hashMap.insert(206);

        System.out.println(hashMap.size());
        System.out.println(hashMap.search(1205));
    }

}
