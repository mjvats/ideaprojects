import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ClosestPower {

    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.parseInt(br.readLine());

        while(T > 0){

            long A = Long.parseLong(br.readLine());
            long result = getClosestPower(A);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;

        }

        br.close();
        bw.close();
    }

    private static long getClosestPower(long N)
    {
        long i = N-1;
        long j = N+1;

        while(!isPerfectPower(i))
        {
            i--;
        }
        long leftDiff = N-i;

        while(!isPerfectPower(j))
        {
            j++;
        }
        long rightDiff = j-N;

        if(leftDiff < rightDiff)
        {
            return i;
        }
        else if(rightDiff < leftDiff)
        {
            return j;
        }
        else
        {
            return Long.min(i, j);
        }
    }

    private static boolean isPerfectPower(long a) {

        long start = 2, end = (long) Math.sqrt(a);

        while (start <= end)
        {
            long mid = (start + end)/2;
            long prod = mid;
            while (prod*mid <= a)
            {
                prod = prod*mid;
            }
            if(prod == a)
            {
                return true;
            }
            else if(prod < a)
            {
                start = mid+1;
            }
            else
            {
                end = mid-1;
            }
        }
        return false;
    }

}
