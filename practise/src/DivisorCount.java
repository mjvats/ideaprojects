import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class DivisorCount {

    public static void main(String[] args) throws IOException {
        OutputStreamWriter osw = new OutputStreamWriter(System.out);
        BufferedWriter bw = new BufferedWriter(osw);

        long T = 1L;
        while (T > 0) {
            bw.write(String.valueOf(divisorscount(9)));
            bw.write("\n");
            T--;
        }

        bw.close();
    }

    public static int divisorscount(int N)
    {
        int count = 0;
        for(int i = 1 ; i <= (int)(Math.sqrt(N)) ; i++)
        {
            if(N % i == 0)
            {
                if(N / i == i)
                {
                    count = count + 1;
                }
                else
                {
                    count = count+2;
                }
            }
        }
        return count;
    }
}
