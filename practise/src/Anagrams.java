import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Anagrams {

    public static void main(String[] args) {

        List<String> A = Arrays.asList("abbbaabbbabbbbabababbbbbbbaabaaabbaaababbabbabbaababbbaaabbabaabbaabbabbbbbababbbababbbbaabababba", "abaaabbbabaaabbbbabaabbabaaaababbbbabbbaaaabaababbbbaaaabbbaaaabaabbaaabbaabaaabbabbaaaababbabbaa", "babbabbaaabbbbabaaaabaabaabbbabaabaaabbbbbbabbabababbbabaabaabbaabaabaabbaabbbabaabbbabaaaabbbbab", "bbbabaaabaaaaabaabaaaaaaabbabaaaabbababbabbabbaabbabaaabaabbbabbaabaabaabaaaabbabbabaaababbaababb", "abbbbbbbbbbbbabaabbbbabababaabaabbbababbabbabaaaabaabbabbaaabbaaaabbaabbbbbaaaabaaaaababababaabab", "aabbbbaaabbaabbbbabbbbbaabbababbbbababbbabaabbbbbbababaaaabbbabaabbbbabbbababbbaaabbabaaaabaaaaba", "abbaaababbbabbbbabababbbababbbaaaaabbbbbbaaaabbaaabbbbbbabbabbabbaabbbbaabaabbababbbaabbbaababbaa", "aabaaabaaaaaabbbbaabbabaaaabbaababaaabbabbaaaaababaaabaabbbabbababaabababbaabaababbaabbabbbaaabbb");
        ArrayList<ArrayList<Integer>> result = countAnagrams(A);
        for (ArrayList<Integer> list : result) {
            for (Integer val : list) {
                System.out.print(val + ",");
            }
            System.out.print("||");
        }
    }

    public static ArrayList<ArrayList<Integer>> countAnagrams(final List<String> A) {
        ArrayList<ArrayList<Integer>> resultList = new ArrayList<>();
        HashMap<String, ArrayList<Integer>> map = new HashMap<>();
        for (int i = 0; i < A.size(); i++) {
            char[] charArr = A.get(i).toCharArray();
            ArrayList<Character> charList = new ArrayList<>();
            for (char c : charArr) {
                charList.add(c);
            }
            Collections.sort(charList);
            String sortedString = charList.toString();
            ArrayList<Integer> tempList = new ArrayList<>();
            if (map.containsKey(sortedString)) {
                tempList = new ArrayList<>(map.get(sortedString));
            }
            tempList.add(i+1);
            map.put(sortedString, tempList);
        }
        for(Map.Entry<String, ArrayList<Integer>> entry : map.entrySet())
        {
            resultList.add(entry.getValue());
        }
        return resultList;
    }

}
