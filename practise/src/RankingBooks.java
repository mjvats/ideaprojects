import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class RankingBooks {
    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.valueOf(br.readLine());

        while(T > 0){

            int N = Integer.valueOf(br.readLine());
            int[] A = new int[N];
            String[] line1 = br.readLine().trim().split(" ");
            for(int i = 0 ; i < N; i++){
                A[i] = Integer.valueOf(line1[i]);
            }

            int M = Integer.valueOf(br.readLine());
            int[] B = new int[M];
            String[] line2 = br.readLine().trim().split(" ");
            for(int i = 0 ; i < M; i++){
                B[i] = Integer.valueOf(line2[i]);
            }

            ArrayList<Integer> result = func(A, B);
            for(int val : result)
            {
                bw.write(String.valueOf(val)+" ");
            }
            bw.write("\n");
            T--;

        }

        br.close();
        bw.close();
    }

    public static ArrayList<Integer> func(int[] A, int[] B)
    {
        ArrayList<Integer> result = new ArrayList<>();
        ArrayList<Integer> listA = new ArrayList<>();
        int i = 0;
        for( ; i < A.length-1 ; i++)
        {
            if(A[i] == A[i+1])
            {
                continue;
            }
            listA.add(A[i]);
        }
        listA.add(A[i]);

        i = 0;
        for( ; i < B.length ; i++)
        {
            int val = B[i];
            int mid = (0+ listA.size()-1)/2;
            if(listA.get(mid) == val)
            {
                result.add(mid+1);
            }
            else if(listA.get(mid) > val)
            {
                int j = mid+1;
                for( ; j < listA.size() ; j++)
                {
                    if(listA.get(j) <= val)
                    {
                        break;
                    }
                }
                result.add(j+1);
            }
            else
            {
                int j = mid-1;
                for( ; j >= 0 ; j--)
                {
                    if(listA.get(j) > val)
                    {
                        break;
                    }
                    else if(listA.get(j) == val)
                    {
                        j--;
                        break;
                    }
                }
                result.add(j+2);
            }
        }
        return result;
    }
}
