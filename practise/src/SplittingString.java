import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;

public class SplittingString {

    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        OutputStreamWriter osw = new OutputStreamWriter(System.out);

        BufferedReader br = new BufferedReader(isr);
        BufferedWriter bw = new BufferedWriter(osw);

        int T = Integer.valueOf(br.readLine());

        while(T > 0){

            String input = br.readLine();
            char[] charArr = input.toCharArray();
            int result = perfectString(charArr, 0, charArr.length-1);
            bw.write(String.valueOf(result));
            bw.write("\n");
            T--;

        }

        br.close();
        bw.close();

    }

    public static int perfectString(char[] charArr, int start, int end)
    {
        HashMap map = new HashMap();
        if(start == end)
        {
            return 0;
        }

        boolean allcharsame = true;
        for(int i = start; i < end; i++)
        {
            if(charArr[i] != charArr[i+1])
            {
                allcharsame = false;
                break;
            }
        }
        if(allcharsame)
        {
            return 0;
        }

        if((end-start+1) % 2 != 0)
        {
            return -1;
        }

        int mid = (start + end)/2;
        int a = perfectString(charArr, start, mid);
        int b = perfectString(charArr, mid+1, end);

        if(a == -1 && b == -1)
        {
            return -1;
        }
        else if(b == -1)
        {
            return 1+a;
        }
        else if(a == -1)
        {
            return 1+b;
        }
        else
        {
            return Integer.min(1+a, 1+b);
        }
    }

}
