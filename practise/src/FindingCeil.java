public class FindingCeil {

    static int[] A;

    static public void mergeSort(int arr[], int start, int end) {
        if (start == end) {
            return;
        } else {
            int mid = (start + end) / 2;
            mergeSort(arr, start, mid);
            mergeSort(arr, mid + 1, end);
            merge(arr, start, mid, end);
        }
    }

    static void merge(int arr[], int start, int mid, int end) {
        int[] left = new int[mid - start + 1];
        int[] right = new int[end - mid];

        int k = 0;
        for (int i = start; i <= mid; i++) {
            left[k++] = arr[i];
        }
        k = 0;
        for (int i = mid + 1; i <= end; i++) {
            right[k++] = arr[i];
        }

        k = start;
        int i = 0, j = 0;
        while (i < left.length && j < right.length) {
            if (left[i] <= right[j]) {
                arr[k] = left[i];
                i++;
            } else {
                arr[k] = right[j];
                j++;
            }
            k++;
        }

        while (i < left.length) {
            arr[k] = left[i];
            i++;
            k++;
        }
        while (j < right.length) {
            arr[k] = right[j];
            j++;
            k++;
        }
    }

    public static void main(String[] args) {
        int[] arr = {-6, 10, -1, 20, 15, 5};
        mergeSort(arr, 0, 5);
        A = arr;
        System.out.println(findingCeil(-1));
        System.out.println(findingCeil(10));
        System.out.println(findingCeil(13));
        System.out.println(findingCeil(25));
        System.out.println(findingCeil(-10));
    }

    public static int findingCeil(int a) {
        int mid = ((A.length - 1) / 2);
        if (a > A[mid]) {
            for (int i = mid + 1; i < A.length; i++) {
                if (A[i] >= a) {
                    return A[i];
                }
            }
            return Integer.MAX_VALUE;
        } else if (a < A[mid]) {
            for (int i = mid - 1; i >= 0; i--) {
                if (A[i] <= a) {
                    if(A[i] < a){
                        return A[i+1];
                    }
                    else{
                        return A[i];
                    }
                }
            }
            return A[0];
        } else {
            return A[mid];
        }
    }
}
